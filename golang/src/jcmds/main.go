//----------------------------------------------------------------------
// main.go
//----------------------------------------------------------------------
// Copyright 2016-2018 Johan Holmberg.
//----------------------------------------------------------------------
// This file is part of "jcmds".
//
// "jcmds" is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// "jcmds" is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with "jcmds".  If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------

package main

import (
	"bufio"
	"crypto/md5"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"os/signal"
	"path"
	"runtime/pprof"
	"strings"

	"libjcons"
)

type Chdir2 struct {
	dir string
	cwd string
}

var cons_cache_by_dir = make(map[Chdir2]map[[16]byte]*libjcons.Cons)

var default_digest = [16]byte{100}

var OptCpuProfile bool
var OptNjobs int

//----------------------------------------------------------------------

type FileArr []string

var OptFile FileArr

func (self *FileArr) String() string {
	return fmt.Sprint(*self)
}
func (self *FileArr) Set(value string) error {
	*self = append(*self, value)
	return nil
}

//----------------------------------------------------------------------

func ParseArguments() {
	flag.BoolVar(&OptCpuProfile, "cpu-profile", false, "CPU profiling")

	flag.BoolVar(&libjcons.OptLogging, "logging", false, "logging for debugging")
	flag.BoolVar(&libjcons.OptPrintTree, "print-tree", false, "print directory tree for debugging")
	flag.BoolVar(&libjcons.OptAcceptExistingTarget, "accept-existing-target", false, "make updating an existing target a nop")

	flag.BoolVar(&libjcons.OptRemove, "r", false, "remove targets")
	flag.BoolVar(&libjcons.OptRemove, "remove", false, "remove targets")

	flag.Var(&OptFile, "f", "file with commands")
	flag.Var(&OptFile, "file", "file with commands")

	flag.BoolVar(&libjcons.OptListTargets, "p", false, "list known targets")
	flag.BoolVar(&libjcons.OptListTargets, "list-targets", false, "list known targets")

	flag.BoolVar(&libjcons.OptQuiet, "q", false, "be more quiet")
	flag.BoolVar(&libjcons.OptQuiet, "quiet", false, "be more quiet")
	flag.IntVar(&OptNjobs, "parallel", 1, "build in parallel")
	flag.BoolVar(&libjcons.KeepGoing, "k", false, "continue after errors")

	flag.BoolVar(&libjcons.OptDebug, "debug", false, "debug mode")
	flag.BoolVar(&libjcons.OptVerbose, "verbose", false, "be more verbose")
	flag.BoolVar(&libjcons.OptAlwaysMake, "always-make", false, "always build targets")
	flag.BoolVar(&libjcons.OptAlwaysMake, "B", false, "always build targets")
	flag.Parse()

	if libjcons.OptVerbose {
		fmt.Printf("This is jcons_go\n")
		fmt.Printf("    OptNjobs = %d\n", OptNjobs)
	}
}

//----------------------------------------------------------------------

var args []string
var g_incs = []string{}

func argument(cmdline, prefix string, value *string) bool {
	if strings.HasPrefix(cmdline, prefix) {
		*value = cmdline[len(prefix):]
		return true
	}
	return false
}

func split_string(str string, parts *[]string) {
	var i, j int
	for j = len(str); j > 0 && str[j-1] == ' '; j -= 1 {
	}

	i = 0
	for {
		for i < j && str[i] == ' ' {
			i += 1
		}
		if i >= j {
			break
		}
		var k int
		for k = i; k < j && str[k] != ' '; k += 1 {
		}
		*parts = append(*parts, str[i:k])
		i = k
	}

}

func incs_to_cons(
	incs []string,
	cons_cache map[[16]byte]*libjcons.Cons) *libjcons.Cons {

	md := md5.New()
	delim := "::"
	for _, inc := range incs {
		md.Write([]byte(inc))
		md.Write([]byte(delim))
	}
	var digest [16]byte
	copy(digest[:], md.Sum(nil))

	env1 := cons_cache[digest]
	if env1 == nil {
		incs_copy := make([]string, len(incs))
		copy(incs_copy, incs)
		env1 = libjcons.NewCons(incs_copy)
		cons_cache[digest] = env1
	}
	return env1
}

func ReadCommands(fh *os.File, dir string) {
	libjcons.PushDir(dir)
	defer libjcons.PopDir()

	var value string
	var cachedir string

	var infiles []string
	var infile string
	var outfile string
	var chdir string

	input := []string{}
	output := []string{}
	incdir := []string{}
	depend := []string{}
	exe_depend := []string{}

	reader := bufio.NewReader(fh)
	for {
		bytes, err := reader.ReadBytes('\n')
		if err == io.EOF {
			break
		} else if err != nil {
			fmt.Printf("ERROR: %s\n", err)
			os.Exit(1)
		}
		cmdline := string(bytes)
		cmdline = strings.TrimRight(cmdline, "\r\n")
		if libjcons.OptDebug {
			fmt.Println("######## ", cmdline)
		}
		used_cmdline := cmdline

		if false {
			args = strings.Fields(cmdline)
		} else {
			args = args[:0]
			split_string(cmdline, &args)
		}

		if len(args) == 0 {
			// skip
			continue
		}
		if cmdline[0] == '#' {
			if argument(cmdline, "# INPUT: ", &value) {
				input = append(input, value)
				continue
			}
			if argument(cmdline, "# OUTPUT: ", &value) {
				output = append(output, value)
				continue
			}
			if argument(cmdline, "# INCDIR: ", &value) {
				incdir = append(incdir, value)
				continue
			}
			if argument(cmdline, "# CHDIR: ", &value) {
				chdir = value
				continue
			}
			if argument(cmdline, "# DEPEND: ", &value) {
				depend = append(depend, value)
				continue
			}
			if argument(cmdline, "# EXE_DEPEND: ", &value) {
				exe_depend = append(exe_depend, value)
				continue
			}
			if argument(cmdline, "# CACHEDIR: ", &value) {
				cachedir = value
				continue
			}
		}

		var cwd = "."
		if len(args) > 3 && args[0] == "cd" && args[2] == "&&" {
			cwd = args[1]
			args = args[3:]
		} else if chdir != "" {
			cwd = chdir
			used_cmdline = "cd " + chdir + " && " + cmdline
		}
		if dir != "." {
			used_cmdline = "cd " + dir + " && " + used_cmdline
		}
		libjcons.PushDir(cwd)

		k := Chdir2{dir, cwd}
		cons_cache, ok := cons_cache_by_dir[k]
		if !ok {
			cons_cache = make(map[[16]byte]*libjcons.Cons)
			cons_cache_by_dir[k] = cons_cache
		}
		env, ok := cons_cache[default_digest]
		if !ok {
			env = libjcons.NewCons([]string{})
			cons_cache[default_digest] = env
		}

		var cmd *libjcons.Cmd

		if len(input) == 1 && len(output) == 1 && len(incdir) > 0 {
			digest_cmdline := used_cmdline
			env1 := incs_to_cons(incdir, cons_cache)
			cmd = env1.CommandCpp(output[0], input[0], used_cmdline, digest_cmdline)
			input = input[:0]
			output = output[:0]
			incdir = incdir[:0]

		} else if len(args) == 3 && args[0] == "cp" {
			cmd = env.Command(args[2], args[1], used_cmdline)

		} else if len(input) > 0 && len(output) > 0 {
			cmd = env.CommandArr(output, input, used_cmdline)
			input = input[:0]
			output = output[:0]

		} else if len(input) > 0 && len(depend) > 0 && cmdline == ":" {
			for _, tgt := range depend {
				for _, src := range input {
					env.Depends(tgt, src)
				}
			}
			input = input[:0]
			depend = depend[:0]

		} else if len(input) > 0 && len(exe_depend) > 0 && cmdline == ":" {
			for _, tgt := range exe_depend {
				for _, src := range input {
					env.ExeDepends(tgt, src)
				}
			}
			input = input[:0]
			exe_depend = exe_depend[:0]

		} else if compiler() && opt_c() && src_arg(&infile) && opt_o(&outfile) {
			digest_cmdline := args[0]
			rest := args[1:]
			g_incs = g_incs[:0]
			for len(rest) > 0 {
				if rest[0] == "-I" && len(rest) >= 2 {
					g_incs = append(g_incs, rest[1])
					rest = rest[2:]
				} else if strings.HasPrefix(rest[0], "-I") {
					g_incs = append(g_incs, rest[0][2:])
					rest = rest[1:]
				} else {
					digest_cmdline += " " + rest[0]
					rest = rest[1:]
				}
			}
			env1 := incs_to_cons(g_incs, cons_cache)

			//fmt.Printf("#env = %d\n", len(cons_cache))
			cmd = env1.CommandCpp(outfile, infile, used_cmdline, digest_cmdline)

		} else if compiler() && !opt_c() && objs(&infiles) &&
			(archives(&infiles, false) || true) && opt_o(&outfile) {

			outfiles := []string{outfile}
			cmd = env.CommandArr(outfiles, infiles, used_cmdline)

		} else if path.Base(args[0]) == "ar" && ar_option(args[1]) && len(args) >= 3 {
			outfiles := []string{args[2]}
			infiles = args[3:]
			cmd = env.CommandArr(outfiles, infiles, used_cmdline)

		} else if has_input(&infile) && has_output(&outfile) {
			cmd = env.Command(outfile, infile, used_cmdline)

		} else {
			log.Fatal("unknown command: " + cmdline)
		}

		if cmd != nil {
			if cachedir != "" {
				cmd.SetCachedir(cachedir)
				cachedir = ""
			}
		}
		libjcons.PopDir()
		chdir = ""
	}
}

var compilers = map[string]bool{"gcc": true, "g++": true, "c++": true, "cc": true}

var compiler_suffixes []string
var compiler_prefixes []string

func init() {
	for k := range compilers {
		compiler_prefixes = append(compiler_prefixes, k+"-")
		compiler_suffixes = append(compiler_suffixes, "-"+k)
	}
}

func compiler() bool {
	name := path.Base(args[0])
	for _, prefix := range compiler_prefixes {
		if strings.HasPrefix(name, prefix) {
			return true
		}
	}
	for _, suffix := range compiler_suffixes {
		if strings.HasSuffix(name, suffix) {
			return true
		}
	}
	return compilers[name]
}

func ar_option(arg string) bool {
	return arg == "rc" || arg == "cq" || arg == "qc"
}

func opt_c() bool {
	flag := false
	for _, arg := range args {
		if arg == "-c" {
			flag = true
			break
		}
	}
	return flag
}

var known_exts = map[string]bool{".c": true, ".cpp": true, ".cc": true, ".cxx": true, ".C": true}

func src_arg(file *string) bool {
	nfound := 0
	for _, arg := range args {
		ext := path.Ext(arg)
		if known_exts[ext] {
			*file = arg
			nfound += 1
		}
	}
	return nfound == 1
}

func opt_o(file *string) bool {
	nfound := 0
	for i, arg := range args {
		if arg == "-o" && i < len(args)-1 {
			*file = args[i+1]
			nfound += 1
		} else if strings.HasPrefix(arg, "-o") && len(arg) > 2 {
			*file = arg[2:]
			nfound += 1
		}
	}
	return nfound == 1
}

func objs(files *[]string) bool {
	*files = nil
	for _, arg := range args {
		if path.Ext(arg) == ".o" {
			*files = append(*files, arg)
		}
	}
	return len(*files) >= 1
}

func archives(files *[]string, clear bool) bool {
	if clear {
		*files = nil
	}
	for _, arg := range args {
		if path.Ext(arg) == ".a" {
			*files = append(*files, arg)
		}
	}
	return len(*files) >= 1
}

func has_input(file *string) bool {
	count := 0
	for i, arg := range args {
		if arg == "<" && i < len(args)-1 {
			*file = args[i+1]
			count += 1
		}
	}
	return count == 1
}

func has_output(file *string) bool {
	count := 0
	for i, arg := range args {
		if arg == ">" && i < len(args)-1 {
			*file = args[i+1]
			count += 1
		}
	}
	return count == 1
}

//----------------------------------------------------------------------

func setup_signals() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		<-c
		libjcons.GotSigint = true
	}()
}

//----------------------------------------------------------------------

func main() {
	ParseArguments()
	libjcons.Initialize()
	setup_signals()

	if OptCpuProfile {
		f, err := os.Create("my_cpu.prof")
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}

	if len(OptFile) == 0 {
		ReadCommands(os.Stdin, ".")
	} else {
		for _, fname := range OptFile {
			fh, err := os.Open(fname)
			if err != nil {
				log.Fatal(err)
			}
			defer fh.Close()
			ReadCommands(fh, path.Dir(fname))
		}

	}

	if libjcons.OptVerbose {
		libjcons.TopPrintTree()
	}

	targets := flag.Args()
	if len(targets) == 0 {
		targets = []string{"."}
	}
	engine := libjcons.NewEngine(OptNjobs)
	nerrors := engine.UpdateTopFiles(targets)

	if libjcons.GotSigint {
		fmt.Printf("jcons: *** got interrupt, terminating\n")
		os.Exit(1)
	}
	if nerrors > 0 {
		os.Exit(1)
	}
}

//----------------------------------------------------------------------
// process_windows.go
//----------------------------------------------------------------------
// Copyright 2016-2018 Johan Holmberg.
//----------------------------------------------------------------------
// This file is part of "jcmds".
//
// "jcmds" is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// "jcmds" is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with "jcmds".  If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------

// +build windows

package libjcons

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

func processStart(done chan Done, pid int, cmdline string, noop bool) {
	if noop {
		done <- Done{pid, nil}
	} else {
		cmd := exec.Command("cmd", "/c", cmdline)
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		err := cmd.Run()
		done <- Done{pid, err}
	}
}

func splitPath(path string) (string, []string) {
	if path == "" {
		panic("empty path")
	}
	path = filepath.ToSlash(path)
	if len(path) == 3 && path[1] == ':' && path[2] == '/' {
		return path[:3], []string{}
	}
	res := strings.Split(path, "/")
	for i := 1; i < len(res)-1; i += 1 {
		if len(res[i]) == 0 {
			res = append(res[:i], res[i+1:]...)
			i -= 1
		}
	}
	if len(path) >= 3 && path[1] == ':' && path[2] == '/' {
		return path[:3], res[1:]
	} else {
		return "", res
	}
}

func Printf(format string, args ...interface{}) {
	fmt.Printf(format, args...)
	fmt.Printf("\r\n")
}

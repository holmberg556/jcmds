//----------------------------------------------------------------------
// cons.go
//----------------------------------------------------------------------
// Copyright 2016-2018 Johan Holmberg.
//----------------------------------------------------------------------
// This file is part of "jcmds".
//
// "jcmds" is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// "jcmds" is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with "jcmds".  If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------

package libjcons

import (
	"log"
	"os"
)

//----------------------------------------------------------------------

func Initialize() {
	InitializeFilesystem()
	conscriptDirStack = make([]*Dir, 0, 10)
	conscriptDir = Cwd
}

//----------------------------------------------------------------------

var conscriptDirStack []*Dir
var conscriptDir *Dir

func PushDir(path string) {
	conscriptDirStack = append(conscriptDirStack, conscriptDir)
	conscriptDir = conscriptDir.findDir(path)
}

func PopDir() {
	n := len(conscriptDirStack)
	conscriptDir = conscriptDirStack[n-1]
	conscriptDirStack = conscriptDirStack[:n-1]
}

//----------------------------------------------------------------------

type CppPathMap struct {
	incs          []string
	cppPathUnique []*Dir
	fileToRfile   map[string]*File
	fileToResult  map[*File]Digest
	byDir         map[*Dir]map[Include]*File
}

func NewCppPathMap(cwd *Dir, incs []string) *CppPathMap {
	var ds []*Dir
	for _, inc := range incs {
		ds = append(ds, cwd.findDir(inc))
	}
	return &CppPathMap{
		incs:          incs,
		cppPathUnique: ds,
		fileToRfile:   make(map[string]*File),
		fileToResult:  make(map[*File]Digest),
	}
}

func (cppPathMap *CppPathMap) putResult(file *File, result Digest) {
	cppPathMap.fileToResult[file] = result
}

func (cppPathMap *CppPathMap) getResult(file *File) Digest {
	result, ok := cppPathMap.fileToResult[file]
	if !ok {
		log.Fatalf("Internal error: %s\n", file.Path())
	}
	return result
}

func (cppPathMap *CppPathMap) hasResult(file *File) bool {
	_, ok := cppPathMap.fileToResult[file]
	return ok
}

func (cppPathMap *CppPathMap) findNodeIncludes(node *File) []*File {
	d := node.Parent()
	var incs []*File

	if cppPathMap.byDir == nil {
		cppPathMap.byDir = make(map[*Dir]map[Include]*File)
	}
	byDir, ok := cppPathMap.byDir[d]
	if !ok {
		byDir = make(map[Include]*File)
		cppPathMap.byDir[d] = byDir
	}

	for _, include := range node.dbIncludes() {
		inc, ok := byDir[include]
		if !ok {
			if include.Quotes {
				inc = d.lookupOrFsFile(include.File)
			}
			if inc == nil {
				inc = cppPathMap.findInclude(include.File)
			}
			byDir[include] = inc
		}
		if inc != nil && inc != node {
			if OptLogging {
				Printf("INCFOUND: %s - %s",
					node.Path(), inc.Path())
			}
			incs = append(incs, inc)
		}
	}
	return incs
}

func (cppPathMap *CppPathMap) findInclude(file string) *File {
	f, ok := cppPathMap.fileToRfile[file]
	if ok {
		return f
	}

	// calculate new value and cache it
	for _, d := range cppPathMap.cppPathUnique {
		f = d.lookupOrFsFile(file)
		if f != nil {
			break
		}
	}
	cppPathMap.fileToRfile[file] = f
	return f
}

//----------------------------------------------------------------------

type Cons struct {
	cwd        *Dir
	incs       []string
	cppPathMap *CppPathMap
}

func NewCons(incs []string) *Cons {
	return &Cons{
		cwd:  conscriptDir,
		incs: incs,
	}
}

func (cons *Cons) cppPath() *CppPathMap {
	if cons.cppPathMap == nil {
		cons.cppPathMap = NewCppPathMap(cons.cwd, cons.incs)
	}
	return cons.cppPathMap
}

func connectSrc(s *File, cmd *Cmd) {
	cmd.srcs = append(cmd.srcs, s)
}

func connectTgt(t *File, cmd *Cmd) {
	if t.cmd != nil {
		Printf("jcons: error: same target twice: '%s'",
			t.Path())
		os.Exit(1)
	}
	cmd.tgts = append(cmd.tgts, t)
	t.cmd = cmd

	if t.extraDeps != nil {
		if cmd.extraDeps != nil {
			// merge sets
			cmd.extraDeps = append(cmd.extraDeps, t.extraDeps...)
			t.extraDeps = nil
		} else {
			// move set
			cmd.extraDeps = t.extraDeps
			t.extraDeps = nil
		}
	}
}

func (cons *Cons) Command(tgt string, src string, command string) *Cmd {
	t := conscriptDir.FindFile(tgt)
	s := conscriptDir.FindFile(src)
	cmd := &Cmd{cons: cons}
	connectSrc(s, cmd)
	connectTgt(t, cmd)
	cmd.cmdline = command
	return cmd
}

func (cons *Cons) CommandCpp(tgt string, src string, command string, digest_command string) *Cmd {
	t := conscriptDir.FindFile(tgt)
	s := conscriptDir.FindFile(src)
	cmd := &Cmd{cons: cons}
	connectSrc(s, cmd)
	connectTgt(t, cmd)
	cmd.cmdline = command
	cmd.digestCmdline = digest_command
	cmd.usesCpp = true
	return cmd
}

func (cons *Cons) CommandArr(tgts []string, srcs []string, command string) *Cmd {
	cmd := &Cmd{cons: cons}
	for _, tgt := range tgts {
		t := conscriptDir.FindFile(tgt)
		connectTgt(t, cmd)
	}
	for _, src := range srcs {
		s := conscriptDir.FindFile(src)
		connectSrc(s, cmd)
	}
	cmd.cmdline = command
	return cmd
}

func (cons *Cons) Depends(tgt string, src string) {
	t := conscriptDir.FindFile(tgt)
	s := conscriptDir.FindFile(src)
	if t.cmd != nil {
		t.cmd.extraDeps = append(t.cmd.extraDeps, s)
	} else {
		t.extraDeps = append(t.extraDeps, s)
	}
}

func (cons *Cons) ExeDepends(tgt string, src string) {
	t := conscriptDir.FindFile(tgt)
	s := conscriptDir.FindFile(src)
	t.exeDeps = append(t.exeDeps, s)
}

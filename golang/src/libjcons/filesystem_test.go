package libjcons

import (
	"os"
	"path"
	"testing"
)

func TestCwd(t *testing.T) {
	cwd, _ := os.Getwd()
	InitializeFilesystem()

	t.Log("Current dir has path .")
	path1 := Cwd.Path()
	expected := "."
	if path1 != expected {
		t.Errorf("Cwd.Path() != %v : %v\n", expected, path1)
	}

	t.Log("Parent dir has path full path")
	path1 = Cwd.Parent().Path()
	expected = path.Dir(cwd)
	if path1 != expected {
		t.Errorf("Cwd.Parent().Path() != %v : %v\n", expected, path1)
	}

	t.Log("Subdir has relative path")
	path1 = Cwd.findDir("sub1").Path()
	expected = "sub1"
	if path1 != expected {
		t.Errorf("Subdir != %v : %v\n", expected, path1)
	}
}

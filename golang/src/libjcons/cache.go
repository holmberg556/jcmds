//----------------------------------------------------------------------
// cache.go
//----------------------------------------------------------------------
// Copyright 2016-2018 Johan Holmberg.
//----------------------------------------------------------------------
// This file is part of "jcmds".
//
// "jcmds" is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// "jcmds" is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with "jcmds".  If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------

package libjcons

import (
	"fmt"
	"os"
)

type Cache struct {
	top *Dir
}

func NewCache(d *Dir, dpath string) *Cache {
	return &Cache{
		top: d.findDir(dpath),
	}
}

func (cache *Cache) get(dep_sig Digest, f *File) bool {
	dep_sig_str := fmt.Sprintf("%x", dep_sig[:])
	prefix := dep_sig_str[0:1]
	cache_d := cache.top.findDir(prefix)
	err := os.MkdirAll(cache_d.Path(), 0755)
	if err != nil {
		return false
	}

	cache_f := cache_d.FindFile(dep_sig_str)
	if !cache_f.fileExist() {
		return false
	}
	if cache_f.dbDepSig() != dep_sig {
		_ = os.Remove(cache_f.Path())
		return false
	}
	err = os.Link(cache_f.Path(), f.Path())
	if err != nil {
		_ = os.Remove(f.Path())
		return false
	}
	f.ST_updated(dep_sig)
	Printf("Updated from cache: %s", f.Path())
	return true
}

func (cache *Cache) put(dep_sig Digest, f *File) {
	dep_sig_str := fmt.Sprintf("%x", dep_sig[:])
	prefix := dep_sig_str[0:1]
	cache_d := cache.top.findDir(prefix)
	err := os.MkdirAll(cache_d.Path(), 0755)
	if err != nil {
		return
	}

	cache_f := cache_d.FindFile(dep_sig_str)
	if !cache_f.fileExist() {
		// TODO: copy OR link ???
		err = os.Link(f.Path(), cache_f.Path())
		if err != nil {
			return
		}
		cache_f.ST_updated(dep_sig)
	}
}

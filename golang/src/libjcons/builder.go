//----------------------------------------------------------------------
// builder.go
//----------------------------------------------------------------------
// Copyright 2016-2018 Johan Holmberg.
//----------------------------------------------------------------------
// This file is part of "jcmds".
//
// "jcmds" is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// "jcmds" is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with "jcmds".  If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------

package libjcons

import (
	"crypto/md5"
	"fmt"
	"os"
	"regexp"
	"runtime"
	"sort"
	"strings"
)

type State int

const (
	State_RAW State = iota
	State_COOKING
	State_DONE
)

var sActiveFuns = make(map[FUN]bool)

var processCount = 0

//----------------------------------------------------------------------

func reportError(errmsg string, arg string) {
	Printf("jcons: error: %s '%s'", errmsg, arg)
	theEngine.nerrors++
}

//----------------------------------------------------------------------

type IncludesTree struct {
	deps         map[*File][]*File
	invertedDeps map[*File][]*File
	finished     []*File
	visited      map[*File]bool
}

func (tree *IncludesTree) genInvertedDeps() {
	for k, vs := range tree.deps {
		for _, v := range vs {
			tree.invertedDeps[v] = append(tree.invertedDeps[v], k)
		}
	}
}

func (tree *IncludesTree) calculateFinished() {
	for k, vs := range tree.deps {
		k.color = WHITE
		for _, v := range vs {
			v.color = WHITE
		}
	}
	for k, _ := range tree.deps {
		if k.color == WHITE {
			tree.calculateFinished_DFS(k)
		}
	}
}

func (tree *IncludesTree) calculateFinished_DFS(f *File) {
	f.color = GREY
	if p, ok := tree.deps[f]; ok {
		for _, f2 := range p {
			if f2.color == WHITE {
				tree.calculateFinished_DFS(f2)
			}
		}
	}
	f.color = BLACK
	tree.finished = append(tree.finished, f)
}

func (tree *IncludesTree) sccs_prepare() {
	for _, f := range tree.finished {
		f.color = WHITE
	}
}

func (tree *IncludesTree) sccs_DFS(f *File, groupElements *[]*File) {
	f.color = GREY
	*groupElements = append(*groupElements, f)
	if p, ok := tree.invertedDeps[f]; ok {
		for _, f2 := range p {
			if f2.color == WHITE {
				tree.sccs_DFS(f2, groupElements)
			}
		}
	}
	f.color = BLACK
}

//----------------------------------------------------------------------

var mmCreatedFunArr []FUN
var mmCurrentFun FUN

type FUN interface {
	fmt.Stringer

	setStatusOk(flag bool)
	getStatusOk() bool

	setCaller(fun FUN)
	getCaller() FUN

	isFinished() bool
	semSet(count int)
	releaseSemaphores()
	getFinishAfterCmd() bool
	setFinishAfterCmd()
	semRelease() bool
	semAcquire()
	callNextMethod()

	initialize()
}

type FUN_base struct {
	caller         FUN
	next           func()
	statusOk       bool
	semCount       int
	finishAfterCmd bool
}

func (fun *FUN_base) initialize() {
	fun.statusOk = true

	// connect caller and callee
	fun.caller = mmCurrentFun
	if fun.caller != nil {
		fun.caller.semAcquire()
	}
}

func initialize(fun FUN) {
	fun.initialize()
	// collect all created funs
	mmCreatedFunArr = append(mmCreatedFunArr, fun)
	sActiveFuns[fun] = true
}

func (fun *FUN_base) setStatusOk(flag bool) {
	fun.statusOk = flag
}

func (fun *FUN_base) getStatusOk() bool {
	return fun.statusOk
}

func (fun *FUN_base) setCaller(caller FUN) {
	fun.caller = caller
}

func (fun *FUN_base) getCaller() FUN {
	return fun.caller
}

func (fun *FUN_base) isFinished() bool {
	return fun.next == nil
}

func (fun *FUN_base) callNextMethod() {
	mmCreatedFunArr = mmCreatedFunArr[:0] // reset for this call
	for fun.semCount == 1 && fun.next != nil {
		fun.next()
	}
}

func callNextMethod(fun FUN) {
	mmCurrentFun = fun
	fun.callNextMethod()
	if fun.isFinished() {
		delete(sActiveFuns, fun)
	}
}

func (fun *FUN_base) semSet(count int) {
	fun.semCount = count
}

func (fun *FUN_base) releaseSemaphores() {
	// empty
}

func (fun *FUN_base) getFinishAfterCmd() bool {
	return fun.finishAfterCmd
}

func (fun *FUN_base) setFinishAfterCmd() {
	fun.finishAfterCmd = true
}

func (fun *FUN_base) semAcquire() {
	fun.semCount += 1
}

func (fun *FUN_base) semRelease() bool {
	fun.semCount -= 1
	if fun.semCount == 0 {
		return true
	} else if fun.semCount < 0 {
		panic("sem_count < 0")
	} else {
		return false
	}
}

func semRelease(fun FUN) {
	if fun.semRelease() {
		theEngine.setRunnable(fun)
	}
}

//======================================================================
// FUN_get_includes_tree
//
// Collect the include tree of a file.
// While traversing the tree, "update" files that can be built.
// Each node is processed in parallel, so files that take a long time
// to generate, will not stall the collecting in other parts of the
// include tree.
//

type FUN_get_includes_tree struct {
	FUN_base
	level        int
	cppPathMap   *CppPathMap
	node         *File
	includesTree *IncludesTree
}

func MAKE_get_includes_tree(cppPathMap *CppPathMap, node *File,
	includesTree *IncludesTree, level int) {
	if OptLogging {
		Printf("LOG: FUN_get_includes_tree CREATE %s level=%d", node.Path(), level)
	}
	trace(1, node)
	fun := &FUN_get_includes_tree{
		cppPathMap:   cppPathMap,
		node:         node,
		includesTree: includesTree,
		level:        level,
	}
	initialize(fun)
	fun.next = fun.STATE_start
}

func (fun *FUN_get_includes_tree) String() string {
	return fmt.Sprintf("FUN_get_includes_tree[%q]", fun.node)
}

func (fun *FUN_get_includes_tree) STATE_start() {
	trace(0, fun)
	fun.maybe_FUN_update_tgt(fun.node)
	fun.next = fun.STATE_node_updated
}

func (fun *FUN_get_includes_tree) STATE_node_updated() {
	trace(0, fun)
	if !fun.node.ST_ok_p() {
		// stop after failure. Caller will check for this.
		fun.next = nil
		fun.statusOk = false
		return
	}

	for _, inc := range fun.cppPathMap.findNodeIncludes(fun.node) {
		fun.includesTree.deps[fun.node] =
			append(fun.includesTree.deps[fun.node], inc)
		if _, ok := fun.includesTree.visited[inc]; !ok {
			fun.includesTree.visited[inc] = true
			if !fun.cppPathMap.hasResult(inc) {
				MAKE_get_includes_tree(fun.cppPathMap,
					inc, fun.includesTree, fun.level+1)
			}
		}
	}
	fun.next = fun.STATE_finish
}

//----------------------------------------------------------------------

func (fun *FUN_get_includes_tree) STATE_finish() {
	trace(0, fun)
	if !fun.statusOk {
		fun.next = nil
		fun.statusOk = false
		return
	}
	fun.next = nil
}

//======================================================================
// FUN_includes_md5
//
// Calculate MD5 of include tree

type FUN_includes_md5 struct {
	FUN_base
	cppPathMap   *CppPathMap
	node         *File
	includesTree IncludesTree
}

func MAKE_includes_md5(cppPathMap *CppPathMap, node *File) {
	if OptLogging {
		Printf("LOG: FUN_includes_md5 CREATE %s", node.Path())
	}
	trace(1, node)
	fun := &FUN_includes_md5{
		cppPathMap: cppPathMap,
		node:       node,
	}
	initialize(fun)
	fun.includesTree.visited = make(map[*File]bool)
	fun.includesTree.deps = make(map[*File][]*File)
	fun.includesTree.invertedDeps = make(map[*File][]*File)
	fun.next = fun.STATE_start
}

func (fun *FUN_includes_md5) String() string {
	return fmt.Sprintf("FUN_includes_md5[%q]", fun.node)
}

func (fun *FUN_includes_md5) STATE_start() {
	trace(0, fun)
	fun.includesTree.visited[fun.node] = true
	if !fun.cppPathMap.hasResult(fun.node) {
		MAKE_get_includes_tree(fun.cppPathMap, fun.node, &fun.includesTree, 0)
	}
	fun.next = fun.STATE_finish
}

//----------------------------------------------------------------------

func (fun *FUN_includes_md5) STATE_finish() {
	trace(0, fun)
	if !fun.statusOk {
		fun.next = nil
		return
	}

	fun.includesTree.calculateFinished()
	fun.includesTree.genInvertedDeps()

	if len(fun.includesTree.finished) == 0 {
		fun.includesTree.finished =
			append(fun.includesTree.finished, fun.node)
	}

	fun.includesTree.sccs_prepare()
	reverseFiles(fun.includesTree.finished)

	// Find strongly connected components. The second part of the algorithm
	// visiting the nodes in the order given by the "finishing times" from
	// the previous part.

	var groupElements []*File
	var groupOffset1 []int
	var groupOffset2 []int
	for _, it := range fun.includesTree.finished {
		if it.color == WHITE {
			x1 := len(groupElements)
			groupOffset1 = append(groupOffset1, x1)
			fun.includesTree.sccs_DFS(it, &groupElements)
			x2 := len(groupElements)
			groupOffset2 = append(groupOffset2, x2)
		}
	}

	// Calculate MD5 for each of the "nodes". The value depend on the
	// recursively included files, so the order of traversal of
	// group_offset1/group_offset2 is significant.

	for i := len(groupOffset1) - 1; i >= 0; i-- {
		j1 := groupOffset1[i]
		j2 := groupOffset2[i]
		if j2 == j1+1 {
			// *one* file without cycles
			node := groupElements[j1]

			if !fun.cppPathMap.hasResult(node) {
				// no cached value
				fun.cppPathMap.putResult(
					node, fun.nodeCalcMd5(node))
			}
		} else {
			// a cycle with more than one file
			cycle := groupElements[j1:j2]
			digest := fun.nodeCalcMd5Cycle(cycle)
			for _, it := range cycle {
				fun.cppPathMap.putResult(it, digest)
			}
		}
	}
	fun.next = nil
}

func reverseFiles(arr []*File) {
	for i, j := 0, len(arr)-1; i < j; i, j = i+1, j-1 {
		arr[i], arr[j] = arr[j], arr[i]
	}
}

//-----------------------------------

type ByDigest []Digest

func (a ByDigest) Len() int      { return len(a) }
func (a ByDigest) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a ByDigest) Less(i, j int) bool {
	for k := range a[i] {
		if a[i][k] < a[j][k] {
			return true
		} else if a[i][k] > a[j][k] {
			return false
		}
	}
	return false
}

func (fun *FUN_includes_md5) nodeCalcMd5(node *File) Digest {
	deps := fun.includesTree.deps[node]
	if len(deps) == 0 {
		// no dependencies ==> use content
		return node.dbContentSig()
	}

	digests := make([]Digest, 0, len(deps)+1)
	digests = append(digests, node.dbContentSig())
	for _, node2 := range deps {
		digests = append(digests, fun.cppPathMap.getResult(node2))
	}
	sort.Sort(ByDigest(digests))
	digest := calcDigest(digests)
	return digest
}

//-----------------------------------

func (fun *FUN_includes_md5) nodeCalcMd5Cycle(cycle []*File) Digest {
	// set temporarily to make rest of code easier
	for _, it := range cycle {
		fun.cppPathMap.putResult(it, Digest_tmp)
	}

	// collect common digest for all files in cycle
	digests := make([]Digest, 0, 2*len(cycle))
	for _, it := range cycle {
		digests = append(digests, md5calcString(it.Path()))
		digests = append(digests, fun.nodeCalcMd5(it))
	}
	sort.Sort(ByDigest(digests))
	digest := calcDigest(digests)
	return digest
}

//----------------------------------------------------------------------

type FUN_update_tgt struct {
	FUN_base
	tgt            *File
	info           []*File
	nextWaitingSem *FUN_update_tgt
}

func (fun *FUN_base) maybe_FUN_update_tgt(node *File) {
	if node.buildOk == Status_UNKNOWN {
		MAKE_update_tgt(node)
	} else {
		if node.buildOk == Status_ERROR {
			fun.statusOk = false
		}
	}
}

func MAKE_update_tgt(tgt *File) {
	if OptLogging {
		Printf("LOG: FUN_update_tgt CREATE %s", tgt.Path())
	}
	trace(1, tgt)
	fun := &FUN_update_tgt{tgt: tgt}
	initialize(fun)
	fun.next = fun.STATE_start
}

func (fun *FUN_update_tgt) String() string {
	return fmt.Sprintf("FUN_update_tgt[%q]", fun.tgt)
}

//----------------------------------------------------------------------

func (fun *FUN_update_tgt) STATE_start() {
	trace(0, fun)
	if fun.tgt.IsSource() {
		if fun.tgt.fileExist() {
			if fun.tgt.TopTarget {
				Printf("jcons: already up-to-date: '%s' (source file)",
					fun.tgt.Path())
			}
			fun.tgt.ST_source()
			fun.STATE_finish()

		} else {
			reportError("don't know how to build", fun.tgt.Path())
			fun.tgt.ST_propagateError()
			fun.STATE_finish()
		}
	} else {
		// 'tgt' not source
		cmd := fun.tgt.cmd
		if cmd.state == State_RAW {
			cmd.state = State_COOKING
			for _, src := range cmd.srcs {
				fun.maybe_FUN_update_tgt(src)
			}
			for _, extra_dep := range cmd.extraDeps {
				fun.maybe_FUN_update_tgt(extra_dep)
			}
			fun.next = fun.STATE_srcs_updated

		} else if cmd.state == State_COOKING {
			fun.semAcquire()

			// add 'this' to 'waiting sem' list of tgt
			fun.nextWaitingSem = fun.tgt.waitingSem
			fun.tgt.waitingSem = fun

			fun.next = fun.STATE_updated_by_other

		} else if cmd.state == State_DONE {
			fun.next = nil
			fun.statusOk = fun.tgt.ST_ok_p()
		}
	}
}

func (fun *FUN_update_tgt) STATE_updated_by_other() {
	trace(0, fun)
	fun.next = nil
	fun.statusOk = fun.tgt.ST_ok_p()
	return
}

func (fun *FUN_update_tgt) STATE_srcs_updated() {
	trace(0, fun)
	cmd := fun.tgt.cmd
	if !fun.statusOk {
		cmd.ST_propagateError()
		fun.STATE_finish()
		return
	}
	if cmd.usesCpp {
		cpp_path_map := cmd.cons.cppPath()
		for _, src := range cmd.srcs {
			incs := cpp_path_map.findNodeIncludes(src)
			for _, inc := range incs {
				if !cpp_path_map.hasResult(inc) {
					MAKE_includes_md5(cpp_path_map, inc)
				}
				fun.info = append(fun.info, inc)
			}
		}
	}

	f := cmd.findProgram()
	if f != nil {
		fun.maybe_FUN_update_tgt(f)
		for _, dep := range f.exeDeps {
			fun.maybe_FUN_update_tgt(dep)
		}
	}
	fun.next = fun.STATE_maybe_execute_cmd
}

//----------------------------------------------------------------------

func (fun *FUN_update_tgt) STATE_maybe_execute_cmd() {
	trace(0, fun)
	cmd := fun.tgt.cmd
	if !fun.statusOk {
		cmd.ST_propagateError()
		fun.STATE_finish()
		return
	}

	if cmd.usesCpp {
		h := md5.New()
		cpp_path_map := cmd.cons.cppPath()
		for _, inc := range fun.info {
			res := cpp_path_map.getResult(inc)
			addDigest(h, res)
		}
		cmd.includesDigest = digest(h)
	}

	for tgt_nr, tgt := range cmd.tgts {
		tgt.wantedDigest = node_new_dep_sig(tgt, tgt_nr)
	}
	any_need_update := false
	for _, tgt := range cmd.tgts {
		if need_update_p(tgt) {
			any_need_update = true
		}
	}
	if any_need_update {
		for _, tgt := range cmd.tgts {
			if !OptAcceptExistingTarget {
				// OK to fail removing file
				_ = os.Remove(tgt.Path())
			}
		}
		for _, tgt := range cmd.tgts {
			if err := os.MkdirAll(tgt.parent.Path(), 0755); err != nil {
				reportError("expected directory", fun.tgt.parent.Path())
				cmd.ST_propagateError()
				fun.STATE_finish()
				return
			}
		}
		build_cache := cmd.buildCache()
		if build_cache != nil {
			all_ok := true
			for _, tgt := range cmd.tgts {
				all_ok = all_ok && build_cache.get(tgt.wantedDigest, tgt)
			}
			if all_ok {
				// TODO: do anything more ???
				fun.STATE_finish()
				return
			}
		}
		noop := (OptAcceptExistingTarget && cmd.allTgtsExist())
		cmdline := cmd.getCmdline2()

		if OptQuiet {
			processCount++
			Printf("[%d] %s",
				processCount, cmd.get_targets_str())
			// TODO: stdout.flush()
		} else {
			Printf("%s", cmdline)
			// TODO: stdout.flush()
		}

		theEngine.executeCmd(fun, cmdline, noop, cmd.setExitStatus)
		fun.next = fun.STATE_cmd_executed
	} else {
		// ! tgts_need_update
		build_cache := cmd.buildCache()
		for _, tgt := range cmd.tgts {
			tgt.ST_propagate_ok()
			if build_cache != nil && OptCacheForce {
				build_cache.put(tgt.dbDepSig(), tgt)
			}
			if tgt.topTarget {
				Printf("jcons: already up-to-date: '%s'",
					tgt.Path())
			}
		}
		fun.STATE_finish()
	}
}

//----------------------------------------------------------------------

func (fun *FUN_update_tgt) STATE_cmd_executed() {
	trace(0, fun)
	cmd := fun.tgt.cmd
	if cmd.exitStatus != nil {
		reportError("error building", fun.tgt.Path())
		cmd.ST_propagateError()
		fun.STATE_finish()
		return
	}

	var missing_names []string
	for _, tgt := range cmd.tgts {
		if !tgt.fileExist_FORCED() {
			missing_names = append(missing_names, tgt.Path())
		}
	}
	if len(missing_names) > 0 {
		reportError("tgts not created", strings.Join(missing_names, " "))
		cmd.ST_propagateError()
		fun.STATE_finish()
		return
	}

	// update tgt sigs
	build_cache := cmd.buildCache()
	for _, tgt := range cmd.tgts {
		dep_sig := tgt.wantedDigest
		tgt.ST_updated(dep_sig)
		if build_cache != nil {
			build_cache.put(dep_sig, tgt)
		}
	}
	fun.STATE_finish()
}

//----------------------------------------------------------------------

func (fun *FUN_update_tgt) STATE_finish() {
	trace(0, fun)
	fun.releaseSemaphores()
	fun.next = nil
	fun.statusOk = fun.tgt.ST_ok_p()
}

func (fun *FUN_update_tgt) releaseSemaphores() {
	cmd := fun.tgt.cmd
	if cmd != nil {
		if cmd.state == State_COOKING {
			for _, tgt := range cmd.tgts {
				waiting_fun := tgt.waitingSem
				for waiting_fun != nil {
					semRelease(waiting_fun)
					waiting_fun = waiting_fun.nextWaitingSem
				}
				tgt.waitingSem = nil
			}
		}
		cmd.state = State_DONE
	}
}

func need_update_p(tgt *File) bool {
	if OptAlwaysMake {
		return true
	}
	if !tgt.fileExist() {
		return true
	}
	return tgt.dbDepSig() != tgt.wantedDigest
}

//-----------------------------------
// Helper method to 'node_new_dep_sig'.
// Returns the part of the digest all targets of a command have in common.

func cmd_new_dep_sig(cmd *Cmd) Digest {
	// TODO: only do this once (when there are several targets)
	// TODO: maybe move 'md5' or whole method to 'Cmd' class.
	h := md5.New()
	for _, src := range cmd.srcs {
		addDigest(h, src.dbContentSig())
		addString(h, src.Path())
	}
	if len(cmd.extraDeps) > 0 {
		digests := make([]Digest, 0, len(cmd.extraDeps))
		for _, dep := range cmd.extraDeps {
			digests = append(digests, dep.dbContentSig())
		}
		sort.Sort(ByDigest(digests))
		for _, digest := range digests {
			addDigest(h, digest)
		}
	}
	if cmd.usesCpp {
		addDigest(h, cmd.includesDigest)
	}
	cmd.appendSig(h)
	return digest(h)
}

//-----------------------------------
// The digest a target file should have to be considered up-to-date.

func node_new_dep_sig(tgt *File, tgt_nr int) Digest {
	dep_sig := cmd_new_dep_sig(tgt.cmd)
	h := md5.New()
	addDigest(h, dep_sig)
	addInt(h, tgt_nr)
	addString(h, tgt.name)
	return digest(h)
}

//======================================================================
// FUN_top_level

type FUN_top_level struct {
	FUN_base
	tgts []*File
}

func (fun *FUN_top_level) String() string {
	return fmt.Sprintf("FUN_top_level[%q]", fun.tgts)
}

func MAKE_top_level(tgts []*File) FUN {
	trace(1, tgts)
	fun := &FUN_top_level{tgts: tgts}
	initialize(fun)
	fun.next = fun.STATE_start
	return fun
}

func (fun *FUN_top_level) STATE_start() {
	trace(0, fun)
	for _, tgt := range fun.tgts {
		fun.maybe_FUN_update_tgt(tgt)
	}
	fun.next = fun.STATE_finish
}

func (fun *FUN_top_level) STATE_finish() {
	trace(0, fun)
	fun.next = nil
}

//----------------------------------------------------------------------

func trace(level int, x interface{}) {
	if OptDebug {
		var name string
		var arg string

		pc, _, _, ok := runtime.Caller(1)
		if ok {
			function := runtime.FuncForPC(pc)
			name = strings.Replace(function.Name(), "libjcons.", "", 1)
		} else {
			name = "<UNKNOWN_FUN>"
		}
		fun, ok := x.(FUN)
		if ok {
			show := fun.String()
			re := regexp.MustCompile(`\[(.*)\]`)
			m := re.FindStringSubmatch(show)
			if m == nil {
				arg = "???"
			} else {
				arg = m[1]
			}
		} else {
			arg = fmt.Sprintf("%v", x)
		}
		n := 40 + level*8
		Printf("%*s--- %s: %s", n, "", name, arg)
	}
}

//----------------------------------------------------------------------
// cmd.go
//----------------------------------------------------------------------
// Copyright 2016-2018 Johan Holmberg.
//----------------------------------------------------------------------
// This file is part of "jcmds".
//
// "jcmds" is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// "jcmds" is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with "jcmds".  If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------

package libjcons

import (
	"hash"
	"os"
	"path/filepath"
	"strings"
)

//----------------------------------------------------------------------

var fileByCmdname = map[string]*File{}
var pathDirs = filepath.SplitList(os.Getenv("PATH"))

var sCache *Cache

//----------------------------------------------------------------------

type Cmd struct {
	cons           *Cons
	digestCmdline  string
	cmdline        string
	srcs           []*File
	tgts           []*File
	extraDeps      []*File
	state          State
	usesCpp        bool
	includesDigest Digest
	cache          *Cache
	exitStatus     error
}

// pass on 'invalid' call to all targets
func (cmd *Cmd) ST_invalidError() {
	for _, tgt := range cmd.tgts {
		tgt.ST_invalidError()
	}
}

// pass on 'propagate' call to all targets
func (cmd *Cmd) ST_propagateError() {
	for _, tgt := range cmd.tgts {
		tgt.ST_propagateError()
	}
}

func (cmd *Cmd) findProgram() *File {
	args := strings.Fields(cmd.cmdline)
	cmdname := args[0]
	f, ok := fileByCmdname[cmdname]
	if !ok {
		if !strings.Contains(cmdname, "/") {
			// simple name
			for _, dir := range pathDirs {
				f = Cwd.findDir(dir).lookupOrFsFile(cmdname)
				if f != nil {
					fileByCmdname[cmdname] = f
					return f
				}
			}
		} else {
			// relative path
			f = Cwd.lookupOrFsFile(cmdname)
			if f != nil {
				fileByCmdname[cmdname] = f
				return f
			}
		}
		fileByCmdname[cmdname] = nil
		return nil
	} else {
		return f
	}
}

func (cmd *Cmd) buildCache() *Cache {
	if cmd.cache == nil {
		return sCache
	} else {
		return cmd.cache
	}
}

func (cmd *Cmd) allTgtsExist() bool {
	for _, tgt := range cmd.tgts {
		if !tgt.fileExist() {
			return false
		}
	}
	return true
}

func (cmd *Cmd) getCmdline1() string {
	if cmd.digestCmdline == "" {
		return cmd.cmdline
	} else {
		return cmd.digestCmdline
	}
}

func (cmd *Cmd) getCmdline2() string {
	return cmd.cmdline
}

func (cmd *Cmd) get_targets_str() string {
	// TODO: handle quoting of paths
	var res string
	for i, tgt := range cmd.tgts {
		if i > 0 {
			res += " "
		}
		res += tgt.Path()
	}
	return res
}

func (cmd *Cmd) appendSig(h hash.Hash) {
	addString(h, cmd.getCmdline1())

	f := cmd.findProgram()
	if f != nil {
		addString(h, f.Path())
		addDigest(h, f.dbContentSig())

		for _, dep := range f.exeDeps {
			addString(h, dep.Path())
			addDigest(h, dep.dbContentSig())
		}
	}
}

func (cmd *Cmd) setExitStatus(err error) {
	cmd.exitStatus = err
}

func (cmd *Cmd) SetCachedir(cachedir string) {
	d := Cwd
	if conscriptDir != nil {
		d = conscriptDir
	}
	cmd.cache = NewCache(d, cachedir)
}

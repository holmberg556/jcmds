//----------------------------------------------------------------------
// filesystem.go
//----------------------------------------------------------------------
// Copyright 2016-2018 Johan Holmberg.
//----------------------------------------------------------------------
// This file is part of "jcmds".
//
// "jcmds" is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// "jcmds" is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with "jcmds".  If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------

package libjcons

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"regexp"
	"sort"
	"strings"
)

type JConsTime int64

type Status int

const (
	Status_UNKNOWN Status = iota
	Status_OK
	Status_ERROR
)

type Color int

const (
	WHITE Color = iota
	GREY
	BLACK
)

//----------------------------------------------------------------------
// Information saved for each file

type Include struct {
	Quotes bool
	File   string
}

type ConsignEntry struct {
	Mtime      JConsTime
	ContentSig Digest
	DepSig     Digest
	Includes   []Include
}

//----------------------------------------------------------------------
// All ConsignEntry's for a directory

type Consign struct {
	Entries map[string]*ConsignEntry
	Dirty   bool
}

func NewConsign() *Consign {
	return &Consign{
		Entries: make(map[string]*ConsignEntry),
	}
}

func (consign *Consign) get(name string) *ConsignEntry {
	entry, ok := consign.Entries[name]
	if !ok {
		entry = &ConsignEntry{}
		consign.Entries[name] = entry
	}
	return entry
}

func consignReadFile(fname string) *Consign {
	st, err := os.Stat(fname)
	isFile := (err == nil) && st.Mode().IsRegular()
	if isFile {
		consign := pickleLoad(fname)
		consign.Dirty = false
		return consign
	} else {
		return NewConsign()
	}
}

func (consign *Consign) writeFile(now JConsTime, fname string) {
	for _, v := range consign.Entries {
		if v.Mtime == now {
			// don't trust recent 'mtime' (within current "delta")
			v.Mtime = 0
		}
	}
	consign.pickleDump(fname)
}

func pickleLoad(fname string) *Consign {
	f, err := os.Open(fname)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	enc := json.NewDecoder(f)
	var consign *Consign
	err = enc.Decode(&consign)
	if err != nil {
		log.Fatalf("Decode error: %s", err)
	}
	return consign
}

func (consign *Consign) pickleDump(fname string) {
	f, err := os.Create(fname)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	enc := json.NewEncoder(f)
	err = enc.Encode(&consign)
	if err != nil {
		log.Fatalf("Encode error: %s", err)
	}
}

// Set dirty flag and propagate upwards in tree.
//
func (entry *Entry) setDirty() {
	if entry.dirty {
		return
	}
	entry.dirty = true
	for dir := entry.parent; dir != nil && !dir.dirty; dir = dir.parent {
		dir.dirty = true
	}
}

//----------------------------------------------------------------------

var Cwd *Dir
var sTops map[string]*Dir
var sDirArr []*Dir

const dotJconsDir = ".jcons/golang"

// Initialize filesystem module.
// The function can be called several times, and will then "reset"
// the state. This can be useful for testing the code.
func InitializeFilesystem() {
	sTops = make(map[string]*Dir)
	sDirArr = []*Dir{}

	cwd, _ := os.Getwd()

	root, _ := splitPath(cwd)
	if root == "" {
		panic("internal error")
	}
	top := MakeDir(root, nil)
	sTops[root] = top

	Cwd = top.findDir(cwd) // 'cwd' absolute, 'top' not used
	Cwd.cachedPath = "."

	err := os.MkdirAll(dotJconsDir, 0755)
	if err != nil {
		log.Fatal("MkdirAll", dotJconsDir)
	}
}

// Flush information to .consign files.
// Should be called before the process is terminated.
func Terminate() {
	now := currTimeFilesystem()
	for _, d := range sDirArr {
		if d.cachedConsign.Dirty {
			d.cachedConsign.writeFile(now, d.consignPath())
		}
	}
}

func currTimeFilesystem() JConsTime {
	fname := dotJconsDir + "/.timestamp"
	err := ioutil.WriteFile(fname, []byte("a timestamp file\n"), 0644)
	if err != nil {
		log.Fatal(err)
	}
	return getFileMtime(fname)
}

//----------------------------------------------------------------------

type EntryInterface interface {
	Path() string
	Parent() *Dir
	PrintTree(level int)
}

//----------------------------------------------------------------------
// Entry

type Entry struct {
	parent     *Dir
	name       string
	cachedPath string
	dirty      bool
}

func (entry *Entry) Parent() *Dir {
	return entry.parent
}

func (entry *Entry) Path() string {
	if len(entry.cachedPath) != 0 {
		return entry.cachedPath
	}
	if entry.parent == nil {
		return "/"
	}
	path := entry.parent.Path()
	if path == "." {
		path = entry.name
	} else {
		if path[len(path)-1] != '/' {
			path += "/"
		}
		path += entry.name
	}
	entry.cachedPath = path
	return path
}

//----------------------------------------------------------------------
// Dir

type Dir struct {
	Entry
	cache_dir_p   bool
	local_dir_p   bool
	cachedConsign *Consign
	entries       map[string]EntryInterface
	fsEntries     map[string]bool
	tgtdir        *Dir
}

func MakeDir(name string, parent *Dir) *Dir {
	dir := &Dir{}
	dir.name = name
	dir.parent = parent

	// TODO: verify that parent !is nil always is true,
	// and if so remove the tests below.
	dir.cache_dir_p = (parent != nil && parent.cache_dir_p)
	dir.local_dir_p = (parent != nil && parent.local_dir_p)

	dir.entries = make(map[string]EntryInterface)
	dir.entries["."] = dir
	if parent == nil {
		dir.entries[".."] = dir
	} else {
		dir.entries[".."] = parent
	}
	return dir
}

func (dir *Dir) String() string {
	return "Dir[" + dir.Path() + "]"
}

func (dir *Dir) consignPath() string {
	if dir.cache_dir_p {
		return dir.Path() + "/.jconsign"
	} else {
		name := dir.Path()
		//
		// Transform a path with directory delimiters ('/')
		// to a filename:
		//
		//   - use '=' as quote character
		//   - use '+' instead of '/'
		//
		name = strings.Replace(name, "=", "==", -1)
		name = strings.Replace(name, "+", "=+", -1)
		name = strings.Replace(name, "/", "+", -1)

		return dotJconsDir + "/__" + name
	}
}

func (dir *Dir) Consign() *Consign {
	if dir.cachedConsign == nil {
		dir.cachedConsign = consignReadFile(dir.consignPath())
		sDirArr = append(sDirArr, dir)
	}
	return dir.cachedConsign
}

func popName(parts []string) ([]string, string) {
	n := len(parts)
	return parts[:n-1], parts[n-1]
}

//----------------------------------------------------------------------

func (dir *Dir) FindFile(path string) *File {
	root, parts := splitPath(path)
	parts, name := popName(parts)
	d := dir._FindDir(root, parts)

	var f *File
	e, ok := d.entries[name]
	if !ok {
		f = MakeFile(name, d)
		d.entries[name] = f
	} else {
		f = e.(*File)
	}
	if OptPrintTree {
		Printf("-------------------- FindFile(%#v)", path)
		TopPrintTree()
	}
	return f
}

//----------------------------------------------------------------------

func (dir *Dir) startDir(root string) *Dir {
	var d *Dir = nil
	if root == "" {
		d = dir
	} else {
		existing_d, ok := sTops[root]
		if !ok {
			d = MakeDir(root, nil)
			sTops[root] = d
		} else {
			d = existing_d
		}
	}
	return d
}

func (dir *Dir) findDir(path string) *Dir {
	root, parts := splitPath(path)
	d := dir._FindDir(root, parts)
	if OptPrintTree {
		Printf("-------------------- findDir(%#v)", path)
		TopPrintTree()
	}
	return d
}

func (dir *Dir) _FindDir(root string, path []string) *Dir {
	d := dir.startDir(root)
	for _, name := range path {
		if name == "#" {
			d = Cwd
		} else {
			var d2 *Dir
			e2, ok := d.entries[name]
			if !ok {
				d2 = MakeDir(name, d)
				d.entries[name] = d2
			} else {
				d2 = e2.(*Dir)
			}
			d = d2
		}
	}
	return d
}

//----------------------------------------------------------------------

func (dir *Dir) lookupOrFsDir(path string) *Dir {
	root, parts := splitPath(path)
	return dir._lookupOrFsDir(root, parts)
}

func (dir *Dir) _lookupOrFsDir(root string, parts []string) *Dir {
	d := dir.startDir(root)
	for _, name := range parts {
		p, ok := d.entries[name]
		if !ok {
			if !d.possibleEntry(name) {
				return nil
			}
			fspath := d.Path() + "/" + name
			entry, err := os.Stat(fspath)
			if err == nil {
				if entry.Mode().IsDir() {
					d2 := MakeDir(name, d)
					d.entries[name] = d2
					d = d2
				} else {
					f := MakeFile(name, d)
					d.entries[name] = f
					f.mtime = JConsTime(entry.ModTime().Unix())
					f.mtimeSet = true
					f.existSet = true
					f.exist = true
					panic("not a dir")
				}
			} else {
				d.entries[name] = nil
				return nil
			}
		} else if p == nil {
			return nil
		} else {
			d = p.(*Dir)
			if d == nil {
				panic("not a dir")
			}
		}
	}
	return d
}

//----------------------------------------------------------------------
// TODO: what to do if 'path' is a full filepath?

func (dir *Dir) possibleEntry(name string) bool {
	if dir.fsEntries == nil {
		dir.fsEntries = make(map[string]bool)
		f, err := os.Open(dir.Path())
		if err != nil {
			return false
		}
		defer f.Close()
		names, err := f.Readdirnames(0)
		for _, name := range names {
			dir.fsEntries[name] = true
		}
	}
	if dir.fsEntries[name] {
		return true
	} else {
		dir.entries[name] = nil
		return false
	}
}

func (dir *Dir) lookupOrFsFile(path string) *File {
	if OptLogging {
		Printf("lookupOrFsFile: %s", path)
	}
	root, parts := splitPath(path)
	parts, name := popName(parts)
	d := dir._lookupOrFsDir(root, parts)
	if d == nil {
		return nil
	}

	var f *File
	p, ok := d.entries[name]
	if !ok {
		if !d.possibleEntry(name) {
			return nil
		}
		fspath := d.Path() + "/" + name
		entry, err := os.Stat(fspath)
		//Printf("Stat %s", fspath)
		if err == nil {
			if !entry.Mode().IsDir() {
				f = MakeFile(name, d)
				d.entries[name] = f
				f.mtime = JConsTime(entry.ModTime().Unix())
				f.mtimeSet = true
				f.existSet = true
				f.exist = true
			} else {
				d2 := MakeDir(name, d)
				d.entries[name] = d2
				panic("not a file")
			}
		} else {
			d.entries[name] = nil
			return nil
		}
	} else if p == nil {
		return nil
	} else {
		f = p.(*File)
		if f == nil {
			panic("not a file")
		}
	}
	return f
}

//----------------------------------------------------------------------

func (dir *Dir) lookupEntry(path string) EntryInterface {
	root, parts := splitPath(path)
	d := dir.startDir(root)
	for _, name := range parts[0 : len(parts)-1] {
		e := d.entries[name]
		if e == nil {
			return nil
		}
		var ok bool
		d, ok = e.(*Dir)
		if !ok {
			log.Fatal("expected Dir", name)
		}
	}
	return d.entries[parts[len(parts)-1]]
}

//----------------------------------------------------------------------

func (dir *Dir) appendFilesUnder(tgts []*File) []*File {
	var ks []string
	for k := range dir.entries {
		ks = append(ks, k)
	}
	sort.Strings(ks)
	for _, k := range ks {
		v := dir.entries[k]
		if k == "." || k == ".." {
			continue
		}
		if d, ok := v.(*Dir); ok {
			tgts = d.appendFilesUnder(tgts)
			continue
		}
		if f, ok := v.(*File); ok {
			if f.cmd != nil {
				tgts = append(tgts, f)
			}
			continue
		}
		log.Fatal("jcons: error: internal error")
	}
	return tgts
}

//----------------------------------------------------------------------
// File

type File struct {
	Entry
	mtime      JConsTime
	mtimeSet   bool
	exist      bool
	existSet   bool
	buildOk    Status
	cmd        *Cmd
	TopTarget  bool
	waitingSem *FUN_update_tgt
	exeDeps    []*File
	extraDeps  []*File

	wantedDigest Digest
	topTarget    bool
	color        Color
}

func MakeFile(name string, parent *Dir) *File {
	return &File{
		Entry: Entry{
			name:   name,
			parent: parent,
		},
	}
}

func (file *File) String() string {
	return "File[" + file.Path() + "]"
}

func (file *File) consignGetEntry() *ConsignEntry {
	return file.parent.Consign().get(file.name)
}

func (file *File) consignRemoveEntry() {
	delete(file.parent.Consign().Entries, file.name)
}

func (file *File) consignSetDirty() {
	file.parent.Consign().Dirty = true
}

//--------------------------------------------------
// Methods for managing the .consign information ...

// Return the "raw" content_sig value.
// It will always exist when this function is called.

func (file *File) dbContentSig() Digest {
	return file.consignGetEntry().ContentSig
}

// Return dep_sig if it is valid.
// Called when we consider rebuilding the file.

func (file *File) dbDepSig() Digest {
	entry := file.consignGetEntry()
	if entry.DepSig.isInvalid() {
		return InvalidDigest
	}

	mtime := file.fileMtime()
	if mtime == entry.Mtime && OptTrustMtime {
		return entry.DepSig
	}
	contentSig := md5calcFile(file.Path())
	if contentSig == entry.ContentSig {
		entry.Mtime = mtime
		file.consignSetDirty()
		return entry.DepSig
	}
	entry.Includes = nil
	file.consignSetDirty()
	return Digest_undefined
}

// The file exists and is a "source".
// Make sure it has an accurate .consign entry.

func (file *File) ST_source() {
	entry := file.consignGetEntry()
	mtime := file.fileMtime()
	if !(mtime == entry.Mtime && OptTrustMtime) {
		entry.Mtime = mtime
		entry.ContentSig = md5calcFile(file.Path())
		entry.Includes = nil
		file.consignSetDirty()
	}
	if entry.DepSig != entry.ContentSig {
		entry.DepSig = entry.ContentSig
		file.consignSetDirty()
	}
	file.buildOk = Status_OK
}

// The file is invalid: the file does not exist, or a build step failed.
// Forget about the file.

func (file *File) ST_invalidError() {
	file.setDirty()
	file.consignRemoveEntry()
	file.consignSetDirty()
	file.buildOk = Status_ERROR
}

// Propagate the fact that there has been an error "upstream".
// No change of .consign info.

func (file *File) ST_propagateError() {
	file.setDirty()
	file.buildOk = Status_ERROR
}

// The file is already up-to-date.
// No change of .consign info.

func (file *File) ST_propagate_ok() {
	file.buildOk = Status_OK
}

// The file has been updated by running a command.
// The new 'dep_sig' is stored, and 'mtime' and 'content_sig' are updated.

func (file *File) ST_updated(depSig Digest) {
	file.setDirty()
	entry := file.consignGetEntry()
	file.consignSetDirty()
	entry.Mtime = file.fileMtime_FORCED()
	entry.ContentSig = md5calcFile(file.Path())
	entry.DepSig = depSig

	entry.Includes = nil

	file.buildOk = Status_OK
}

// Tell if file has "ok" build status.

func (file *File) ST_ok_p() bool {
	return file.buildOk == Status_OK
}

//----------------------------------------------------------------------

func (file *File) fileExist() bool {
	if !file.existSet {
		file.exist, file.mtime = fileExistAndMtime(file.Path())
		file.existSet = true
		file.mtimeSet = file.exist
	}
	return file.exist
}

//----------------------------------------------------------------------

func (file *File) fileExist_FORCED() bool {
	// like 'file_exist', but with forced call
	file.exist, file.mtime = fileExistAndMtime(file.Path())
	file.existSet = true
	file.mtimeSet = file.exist

	return file.exist
}

//----------------------------------------------------------------------

func (file *File) fileMtime() JConsTime {
	if !file.mtimeSet {
		file.mtime = getFileMtime(file.Path())
		file.mtimeSet = true
	}
	return file.mtime
}

//----------------------------------------------------------------------

func (file *File) fileMtime_FORCED() JConsTime {
	// like 'file_mtime', but with forced call
	file.mtime = getFileMtime(file.Path())
	file.mtimeSet = true

	return file.mtime
}

//----------------------------------------------------------------------
// Called when file exists and has current info in .consign.
// We may either get value from .consign or compute it now.

func (file *File) dbIncludes() []Include {
	entry := file.consignGetEntry()
	if entry.Includes == nil {
		entry.Includes = file.calcIncludes()
		file.consignSetDirty()
	}
	return entry.Includes
}

//----------------------------------------------------------------------

var include_re = regexp.MustCompile(`^\s*#\s*include\s+([<"])(.*?)[>"]`)
var end_re = regexp.MustCompile(`^#end\b`)

func (file *File) calcIncludes() []Include {
	f, err := os.Open(file.Path())
	if err != nil {
		panic("Open: " + file.Path())
	}
	defer f.Close()

	var includes []Include
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		if strings.Index(line, "#") == -1 {
			continue
		}
		m := include_re.FindStringSubmatch(line)
		if len(m) > 0 {
			quotes := (m[1] == "\"")
			filename := m[2]
			includes = append(includes, Include{quotes, filename})
		} else if end_re.FindStringSubmatch(line) != nil {
			break
		}
	}
	return includes
}

func (file *File) IsSource() bool {
	return file.cmd == nil
}

//----------------------------------------------------------------------

func (dir *Dir) PrintTree(level int) {
	for i := 0; i < level; i++ {
		fmt.Print("    ")
	}
	Printf("- %#v", dir.name)
	for k, v := range dir.entries {
		if k == "." {
			continue
		}
		if k == ".." {
			continue
		}
		if v != nil {
			v.PrintTree(level + 1)
		}
	}
}

func (file *File) PrintTree(level int) {
	for i := 0; i < level; i++ {
		fmt.Print("    ")
	}
	Printf("+ %#v", file.name)
}

func TopPrintTree() {
	for k, v := range sTops {
		Printf("==== top = %#v ====", k)
		v.PrintTree(0)
	}
}

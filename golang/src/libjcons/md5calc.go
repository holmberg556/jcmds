//----------------------------------------------------------------------
// md5calc.go
//----------------------------------------------------------------------
// Copyright 2016-2018 Johan Holmberg.
//----------------------------------------------------------------------
// This file is part of "jcmds".
//
// "jcmds" is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// "jcmds" is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with "jcmds".  If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------

package libjcons

import (
	"crypto/md5"
	"encoding/binary"
	"hash"
	"io"
	"log"
	"os"
)

type Digest [16]byte

var Digest_undefined = Digest{1} // TODO
var InvalidDigest = Digest{2}    // TODO
var Digest_tmp = Digest{3}       // TODO

func (digest *Digest) isInvalid() bool {
	return *digest == InvalidDigest
}

func calcDigest(digests []Digest) Digest {
	md := md5.New()
	for _, digest := range digests {
		md.Write(digest[:])
	}
	var res Digest
	copy(res[:], md.Sum(nil))
	return res
}

func md5calcString(arg string) Digest {
	md := md5.New()
	md.Write([]byte(arg))
	var res Digest
	copy(res[:], md.Sum(nil))
	return res
}

// Calculate MD5 of file content
//
func md5calcFile(path string) Digest {
	h := md5.New()
	f, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	if _, err := io.Copy(h, f); err != nil {
		log.Fatal(err)
	}
	var res Digest
	copy(res[:], h.Sum(nil))
	return res
}

func digest(h hash.Hash) Digest {
	var res Digest
	copy(res[:], h.Sum(nil))
	return res
}

func addInt(h hash.Hash, arg int) {
	var buf [8]byte
	binary.PutVarint(buf[:], int64(arg))
	h.Write(buf[:])
}

func addString(h hash.Hash, arg string) {
	h.Write([]byte(arg))
}

func addDigest(h hash.Hash, arg Digest) {
	h.Write(arg[:])
}

//----------------------------------------------------------------------
// engine.go
//----------------------------------------------------------------------
// Copyright 2016-2018 Johan Holmberg.
//----------------------------------------------------------------------
// This file is part of "jcmds".
//
// "jcmds" is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// "jcmds" is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with "jcmds".  If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------

package libjcons

import (
	"os"
	"os/exec"
	"sort"
	"strings"
	"syscall"
)

//----------------------------------------------------------------------

var GotSigint = false
var KeepGoing = false
var latestPid = 100

//----------------------------------------------------------------------

func (engine *Engine) processWait(blocking bool) (pid int, err error) {
	if blocking {
		done := <-engine.done
		err = done.err
		pid = done.pid
	} else {
		select {
		case done := <-engine.done:
			err = done.err
			pid = done.pid
		default:
			pid = 0
		}
	}
	return
}

type Done struct {
	pid int
	err error
}

type ExecuteContext struct {
	fun           FUN
	setExitStatus func(error)
	noop          bool
}

type Engine struct {
	njobs     int
	nerrors   int
	ready     []FUN
	executing map[int]ExecuteContext
	done      chan Done
}

var theEngine *Engine

func NewEngine(njobs int) *Engine {
	theEngine = &Engine{
		njobs:     njobs,
		executing: make(map[int]ExecuteContext),
		done:      make(chan Done),
	}
	return theEngine
}

func (engine *Engine) setRunnable(fun FUN) {
	engine.ready = append(engine.ready, fun)
}

func (engine *Engine) executeCmd(fun FUN, cmdline string, noop bool, setExitStatus func(error)) {
	latestPid += 1
	go processStart(engine.done, latestPid, cmdline, noop)
	engine.executing[latestPid] = ExecuteContext{fun, setExitStatus, noop}
	fun.semAcquire()
}

func (engine *Engine) shouldTerminate() bool {
	return GotSigint || engine.nerrors > 0 && !KeepGoing
}

func (engine *Engine) funCallNext(fun FUN) {
	fun.semSet(1)
	callNextMethod(fun)
	if fun.isFinished() {
		engine.returnToCaller(fun)
	} else {
		engine.queueNewFuns()
		semRelease(fun)
	}
}

func (engine *Engine) returnToCaller(fun FUN) {
	caller := fun.getCaller()
	if caller != nil {
		if !fun.getStatusOk() {
			caller.setStatusOk(false)
		}
		semRelease(caller)
		fun.setCaller(nil) // TODO: funkar == nil here?
	}
}

func (engine *Engine) queueNewFuns() {
	arr := mmCreatedFunArr
	for i, j := 0, len(arr)-1; i < j; i, j = i+1, j-1 {
		arr[i], arr[j] = arr[j], arr[i]
	}
	engine.ready = append(engine.ready, arr...)
}

//----------------------------------------------------------------------

func (engine *Engine) Run() int {
	terminateWarningGiven := false
	progress := true

	for len(engine.executing) > 0 || len(engine.ready) > 0 {
		if len(engine.executing) > 0 {
			if engine.shouldTerminate() && !terminateWarningGiven {
				terminateWarningGiven = true
				Printf("jcons: *** waiting for commands to finish ...")
			}
			blocking := (!progress || len(engine.executing) >= engine.njobs)
			pid, err := engine.processWait(blocking)
			if pid != 0 {
				context, ok := engine.executing[pid]
				if !ok {
					panic("internal error")
				}
				context.setExitStatus(err)
				delete(engine.executing, pid)
				semRelease(context.fun)
				if engine.shouldTerminate() {
					context.fun.setFinishAfterCmd()
				}
				if err != nil {
					maybeReportInterrupted(context.fun, err)
				}
			}
			// always fall through
		}
		progress = false
		if len(engine.ready) > 0 {
			progress = true
			n := len(engine.ready)
			fun := engine.ready[n-1]
			engine.ready = engine.ready[:n-1]
			if engine.shouldTerminate() && !fun.getFinishAfterCmd() {
				// unwind call stack
				fun.releaseSemaphores()
				engine.returnToCaller(fun)
			} else {
				// normal case
				engine.funCallNext(fun)
			}
		}
	}
	return engine.nerrors
}

func maybeReportInterrupted(fun FUN, err error) {
	if exit_error, ok := err.(*exec.ExitError); ok {
		wait_status := exit_error.Sys().(syscall.WaitStatus)
		if wait_status.Signaled() {
			update_fun := fun.(*FUN_update_tgt)
			if wait_status.Signal() == syscall.SIGINT {
				Printf("jcons: *** [%s] interrupted",
					update_fun.tgt.Path())
			} else {
				Printf("jcons: *** [%s] interrupted by %d",
					update_fun.tgt.Path(),
					wait_status.Signal())
			}
		}
	}
}

//----------------------------------------------------------------------

func lookup_targets(targets []string, entries *[]EntryInterface, tgts *[]*File) int {
	var errors = 0
	for _, target := range targets {
		tgt := Cwd.lookupEntry(target)
		if tgt == nil {
			Printf("jcons: error: don't know how to build '%s'",
				target)
			errors += 1
			continue
		}
		if f, ok := tgt.(*File); ok {
			*tgts = append(*tgts, f)
			f.topTarget = true
			*entries = append(*entries, tgt)
			continue
		}
		if d, ok := tgt.(*Dir); ok {
			*tgts = d.appendFilesUnder(*tgts)
			*entries = append(*entries, tgt)
			continue
		}
		Printf("jcons: error: internal error")
		os.Exit(1)
	}
	return errors
}

//----------------------------------------------------------------------

func (engine *Engine) UpdateTopFiles(targets []string) int {
	var tgts []*File
	var entries []EntryInterface
	errors := lookup_targets(targets, &entries, &tgts)
	if errors > 0 {
		return 1
	}

	if OptListTargets {
		for _, tgt := range tgts {
			Printf("%s", tgt.Path())
		}
		return 0
	}
	if OptRemove {
		for _, tgt := range tgts {
			if tgt.fileExist_FORCED() {
				_ = os.Remove(tgt.Path())
				Printf("Removed %s", tgt.Path())
				tgt.ST_invalidError()
			}
		}
		for _, entry := range entries {
			if d, ok := entry.(*Dir); ok {
				if !d.dirty {
					Printf("jcons: already removed: %s",
						d.Path())
				}
			}
		}
		return 0
	}

	engine.ready = []FUN{MAKE_top_level(tgts)}
	nerrors := engine.Run()
	defer Terminate()

	var paths []string
	for fun := range sActiveFuns {
		caller := fun.getCaller()
		if caller != nil {
			update_fun, ok := caller.(*FUN_update_tgt)
			if ok {
				paths = append(paths, update_fun.tgt.Path())
			}
		}
	}
	if len(paths) > 0 {
		sort.Strings(paths)
		Printf("jcons: error: circular dependency for '%s'",
			strings.Join(paths, ","))
		return 1
	}

	for _, entry := range entries {
		if d, ok := entry.(*Dir); ok {
			if !d.dirty {
				Printf("jcons: up-to-date: %s",
					d.Path())
			}
		}
	}

	return nerrors
}

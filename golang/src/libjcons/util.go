//----------------------------------------------------------------------
// util.go
//----------------------------------------------------------------------
// Copyright 2016-2018 Johan Holmberg.
//----------------------------------------------------------------------
// This file is part of "jcmds".
//
// "jcmds" is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// "jcmds" is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with "jcmds".  If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------

package libjcons

import (
	"log"
	"os"
)

// Return (exist, mtime) for a filename
//
func fileExistAndMtime(path string) (exist bool, mtime JConsTime) {
	st, err := os.Stat(path)
	if err != nil {
		return
	}
	mtime = JConsTime(st.ModTime().Unix())
	exist = true
	return
}

// Return mtime for file
//
func getFileMtime(path string) JConsTime {
	st, err := os.Stat(path)
	if err != nil {
		log.Fatal(err)
	}
	return JConsTime(st.ModTime().Unix())
}

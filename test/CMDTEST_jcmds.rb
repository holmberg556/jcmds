
JCONS_CMDS = File.expand_path(ENV['JCMDS_TO_TEST'])

class TC_jcons_cmds < Cmdtest::Testcase

  def setup
    prepend_local_path "."
    ignore_file ".jcons/"
  end

  #----------------------------------------------------------------------

  def test_00_simple_cp
    create_file "cmds.txt", [
      "cp aaa.txt bbb.txt",
    ]

    cmd "#{JCONS_CMDS} bbb.txt < cmds.txt" do
      comment "nonexisting source"
      stdout_equal /don.t know how to build 'aaa.txt'/
      exit_nonzero
    end

    #--------------------
    create_file "aaa.txt", "123456\n"

    cmd "#{JCONS_CMDS} bbb.txt < cmds.txt" do
      comment "existing source"
      stdout_equal [
        "cp aaa.txt bbb.txt",
      ]
      created_files "bbb.txt"
    end

    #--------------------
    cmd "#{JCONS_CMDS} bbb.txt < cmds.txt" do
      comment "up to date"
      stdout_equal [
        "jcons: already up-to-date: 'bbb.txt'",
      ]
    end

    #--------------------
    create_file "aaa.txt", "abcdef\n"

    cmd "#{JCONS_CMDS} bbb.txt < cmds.txt" do
      comment "changed source"
      stdout_equal [
        "cp aaa.txt bbb.txt",
      ]
      written_files "bbb.txt"
    end

    #--------------------
    cmd "#{JCONS_CMDS} bbb.txt < cmds.txt" do
      comment "up to date"
      stdout_equal [
        "jcons: already up-to-date: 'bbb.txt'",
      ]
    end

    #--------------------
    create_file "bbb.txt", "garbage\n"

    cmd "#{JCONS_CMDS} bbb.txt < cmds.txt" do
      comment "changed target"
      stdout_equal [
        "cp aaa.txt bbb.txt",
      ]
      written_files "bbb.txt"
    end

    #--------------------
    cmd "#{JCONS_CMDS} bbb.txt < cmds.txt" do
      comment "up to date"
      stdout_equal [
        "jcons: already up-to-date: 'bbb.txt'",
      ]
    end

    #--------------------
    remove_file "bbb.txt"

    cmd "#{JCONS_CMDS} bbb.txt < cmds.txt" do
      comment "removed target"
      stdout_equal [
        "cp aaa.txt bbb.txt",
      ]
      written_files "bbb.txt"
    end

    #--------------------
    cmd "#{JCONS_CMDS} bbb.txt < cmds.txt" do
      comment "up to date"
      stdout_equal [
        "jcons: already up-to-date: 'bbb.txt'",
      ]
    end

    #--------------------
    remove_file_tree ".jcons"

    cmd "#{JCONS_CMDS} bbb.txt < cmds.txt" do
      comment "removed .jcons/ directory"
      stdout_equal [
        "cp aaa.txt bbb.txt",
      ]
      written_files "bbb.txt"
    end

  end

  #----------------------------------------------------------------------

  def test_ar_simple
    create_file "cmds.txt", [
      "gcc -c mod1.c -o mod1.o",
      "gcc -c mod2.c -o mod2.o",
      "gcc -c prog1.c -o prog1.o",
      "gcc -c prog2.c -o prog2.o",
      "gcc -o prog1 prog1.o mods.a",
      "ar rc mods.a mod1.o mod2.o",
      "gcc -o prog2 prog2.o mods.a",
    ]
    create_file "mod1.c", "int mod1_i = 11;\n"
    create_file "mod2.c", "int mod2_i = 22;\n"
    create_file "prog1.c", [
      "extern int mod1_i;",
      "extern int mod2_i;",
      "int main() { return 1 + mod1_i + mod2_i; }",
    ]
    create_file "prog2.c", [
      "extern int mod1_i;",
      "extern int mod2_i;",
      "int main() { return 2 + mod1_i + mod2_i; }",
    ]

    cmd "#{JCONS_CMDS} < cmds.txt" do
      comment "build all"
      stdout_equal [
        "gcc -c mod1.c -o mod1.o",
        "gcc -c mod2.c -o mod2.o",
        "ar rc mods.a mod1.o mod2.o",
        "gcc -c prog1.c -o prog1.o",
        "gcc -o prog1 prog1.o mods.a",
        "gcc -c prog2.c -o prog2.o",
        "gcc -o prog2 prog2.o mods.a",
      ]
      written_files "mod1.o", "mod2.o", "mods.a", "prog1", "prog1.o", "prog2", "prog2.o"
    end

    #--------------------
    cmd "./prog1" do
      comment "return 34"
      exit_status 34
    end

    #--------------------
    cmd "./prog2" do
      comment "return 35"
      exit_status 35
    end

    #--------------------
    cmd "#{JCONS_CMDS} < cmds.txt" do
      comment "up to date"
      stdout_equal [
        "jcons: up-to-date: .",
      ]
    end

    #--------------------
    create_file "mod2.c", "int mod2_i = 44;\n"

    cmd "#{JCONS_CMDS} < cmds.txt" do
      comment "after mod2.c change"
      stdout_equal [
        "gcc -c mod2.c -o mod2.o",
        "ar rc mods.a mod1.o mod2.o",
        "gcc -o prog1 prog1.o mods.a",
        "gcc -o prog2 prog2.o mods.a",
      ]
      written_files "mod2.o", "mods.a", "prog1", "prog2"
    end

  end

  #----------------------------------------------------------------------

  def test_gcc_I
    create_file "cmds.txt", [
      "gcc -c -Idir1 hello.c -o hello1.o",
      "gcc hello1.o -o hello1",
      "gcc -c -Idir2 hello.c -o hello2.o",
      "gcc hello2.o -o hello2",
    ]

    create_file "dir1/greeting.h", [
      "#define GREETING \"hello...\"",
    ]

    create_file "dir2/greeting.h", [
      "#define GREETING \"goodbye...\"",
    ]

    create_file "hello.c", [
      "#include <stdio.h>",
      "#include <greeting.h>",
      "int main() {",
      "  printf(\"%s\\n\", GREETING);",
      "  return 0;",
      "}",
    ]

    cmd "#{JCONS_CMDS} < cmds.txt hello1 hello2" do
      comment "build hello1 & hello2"
      stdout_equal [
        "gcc -c -Idir1 hello.c -o hello1.o",
        "gcc hello1.o -o hello1",
        "gcc -c -Idir2 hello.c -o hello2.o",
        "gcc hello2.o -o hello2",
      ]
      written_files "hello1.o", "hello1", "hello2.o", "hello2"
    end

    cmd "./hello1" do
      comment "run hello1"
      stdout_equal [
        "hello...",
      ]
    end

    cmd "./hello2" do
      comment "run hello2"
      stdout_equal [
        "goodbye...",
      ]
    end

    #---

    create_file "dir2/greeting.h", [
      "#define GREETING \"hello goodbye...\"",
    ]

    cmd "#{JCONS_CMDS} < cmds.txt hello1 hello2" do
      comment "build - after change in dir2"
      stdout_equal [
        "jcons: already up-to-date: 'hello1'",
        "gcc -c -Idir2 hello.c -o hello2.o",
        "gcc hello2.o -o hello2",
      ]
      written_files "hello2.o", "hello2"
    end

    cmd "./hello1" do
      comment "run hello1"
      stdout_equal [
        "hello...",
      ]
    end

    cmd "./hello2" do
      comment "run hello2"
      stdout_equal [
        "hello goodbye...",
      ]
    end

  end

  #----------

  def test_with_cd
    create_file "cmds.txt", [
      "cd subdir && cp aaa.txt bbb.txt",
    ]

    create_file "subdir/aaa.txt", "123\n"

    cmd "#{JCONS_CMDS} < cmds.txt" do
      comment "cp in subdir"
      stdout_equal [
        "cd subdir && cp aaa.txt bbb.txt",
      ]
      created_files "subdir/bbb.txt"
    end

    cmd "#{JCONS_CMDS} < cmds.txt" do
      comment "cp in subdir -- up to date"
      stdout_equal [
        "jcons: up-to-date: .",
      ]
    end

    #----
    create_file "cmds.txt", [
      "cd subdir && cp aaa.txt bbb.txt",
      "cp subdir/bbb.txt ccc.txt",
    ]

    create_file "subdir/aaa.txt", "123456789\n"

    cmd "#{JCONS_CMDS} < cmds.txt" do
      comment "cp in subdir + normal cp"
      stdout_equal [
        "cd subdir && cp aaa.txt bbb.txt",
        "cp subdir/bbb.txt ccc.txt",
      ]
      written_files "subdir/bbb.txt", "ccc.txt"
    end

    cmd "#{JCONS_CMDS} < cmds.txt" do
      comment "cp in subdir + normal cp - up to date"
      stdout_equal [
        "jcons: up-to-date: .",
      ]
    end

    create_file "subdir/aaa.txt", "ABC\n"

    cmd "#{JCONS_CMDS} < cmds.txt" do
      comment "cp in subdir + normal cp - changed input"
      stdout_equal [
        "cd subdir && cp aaa.txt bbb.txt",
        "cp subdir/bbb.txt ccc.txt",
      ]
      written_files "subdir/bbb.txt", "ccc.txt"
    end

  end

  #----------

  def test_simple_gcc
    create_file "cmds.txt", [
      "gcc -o foo_b foo_a.o",
      "gcc -c foo.c -o foo_a.o",
    ]

    create_file "foo.c", [
      "#include <stdio.h>",
      "int main() { printf(\"hello\\n\"); return 0; }",
    ]

    cmd "#{JCONS_CMDS} < cmds.txt" do
      comment "build simple gcc program"
      stdout_equal [
        "gcc -c foo.c -o foo_a.o",
        "gcc -o foo_b foo_a.o",
      ]
      created_files "foo_a.o", "foo_b"
    end

    cmd "./foo_b" do
      comment "run program"
      stdout_equal [
        "hello",
      ]
    end

  end

  #----------

  def test_simple_2_cp
    create_file "cmds.txt", [
      "cp aaa.txt bbb.txt",
      "cp bbb.txt ccc.txt",
    ]

    #---
    create_file "aaa.txt", "123456\n"

    cmd "#{JCONS_CMDS} < cmds.txt" do
      comment "existing source"
      stdout_equal [
        "cp aaa.txt bbb.txt",
        "cp bbb.txt ccc.txt",
      ]
      created_files "bbb.txt", "ccc.txt"
    end

    #---
    cmd "#{JCONS_CMDS} < cmds.txt" do
      comment "up to date"
      stdout_equal [
        "jcons: up-to-date: .",
      ]
    end

    #---
    create_file "aaa.txt", "abcdef\n"

    cmd "#{JCONS_CMDS} < cmds.txt" do
      comment "changed source"
      stdout_equal [
        "cp aaa.txt bbb.txt",
        "cp bbb.txt ccc.txt",
      ]
      written_files "bbb.txt", "ccc.txt"
    end

    #---
    create_file "bbb.txt", "ABCDEF\n"

    cmd "#{JCONS_CMDS} < cmds.txt" do
      comment "changed intermediate file"
      stdout_equal [
        "cp aaa.txt bbb.txt",
      ]
      written_files "bbb.txt"
    end

  end

  #----------

  def test_CHDIR_INPUT_OUTPUT
    create_file "cmds.txt", [
      "# CHDIR: garbage",
      "# INPUT: aaa.txt",
      "# OUTPUT: bbb.txt",
      "cd subdir && cat -n < aaa.txt > bbb.txt",
    ]

    #---
    create_file "subdir/aaa.txt", "123456\n"

    cmd "#{JCONS_CMDS} < cmds.txt" do
      comment "cd DIR overrides CHDIR"
      stdout_equal [
        "cd subdir && cat -n < aaa.txt > bbb.txt",
      ]
      created_files "subdir/bbb.txt"
    end

    #---
    create_file "cmds.txt", [
      "# CHDIR: subdir",
      "# INPUT: aaa.txt",
      "# OUTPUT: bbb.txt",
      "cat -n < aaa.txt > bbb.txt",
    ]

    cmd "#{JCONS_CMDS} -B < cmds.txt" do
      comment "CHDIR works"
      stdout_equal [
        "cd subdir && cat -n < aaa.txt > bbb.txt",
      ]
      written_files "subdir/bbb.txt"
    end

    #---
    create_file "cmds.txt", [
      "# INPUT: aaa.txt",
      "# OUTPUT: bbb.txt",
      "cd subdir && cat -n < aaa.txt > bbb.txt",
    ]

    cmd "#{JCONS_CMDS} -B < cmds.txt" do
      comment "cd DIR works"
      stdout_equal [
        "cd subdir && cat -n < aaa.txt > bbb.txt",
      ]
      written_files "subdir/bbb.txt"
    end
end

  #----------

  def test_two_option_f
    create_file "dir1/aaa.txt", [
      "dir1 1",
      "dir1 2",
      "dir1 3",
    ]
    create_file "dir1/cmds.txt", [
      "cat -n < aaa.txt > bbb.txt",
    ]

    create_file "dir2/ccc.txt", [
      "dir2 1",
      "dir2 2",
    ]
    create_file "dir2/subdir/eee.txt", [
      "dir2/subdir 1",
      "dir2 subdir 2",
    ]
    create_file "dir2/subdir/ggg.txt", [
      "dir2/subdir 1",
    ]
    create_file "dir2/cmds.txt", [
      "cat -n < ccc.txt > ddd.txt",
      "cd subdir && cat -n < eee.txt > fff.txt",
      "# CHDIR: subdir",
      "cat -n < ggg.txt > hhh.txt",
    ]

    #---
    cmd "#{JCONS_CMDS} -f dir1/cmds.txt -f dir2/cmds.txt" do
      comment "two -f options"
      stdout_equal [
        "cd dir1 && cat -n < aaa.txt > bbb.txt",
        "cd dir2 && cat -n < ccc.txt > ddd.txt",
        "cd dir2 && cd subdir && cat -n < eee.txt > fff.txt",
        "cd dir2 && cd subdir && cat -n < ggg.txt > hhh.txt",
      ]
      created_files [
        "dir1/bbb.txt", "dir2/ddd.txt",
        "dir2/subdir/fff.txt", "dir2/subdir/hhh.txt",
      ]
    end
  end

  #----------

  def test_simple_INCDIR
    create_file "cmds.txt", [
      "# INPUT: aaa.txt",
      "# OUTPUT: bbb.txt",
      "# INCDIR: inc1",
      "cat -n < aaa.txt > bbb.txt",
    ]

    #---
    create_file "aaa.txt", [
      '123',
      '#include "aaa.inc"',
      '456',
    ]

    cmd "#{JCONS_CMDS} bbb.txt < cmds.txt" do
      comment "no existing include"
      stdout_equal [
        "cat -n < aaa.txt > bbb.txt",
      ]
      created_files "bbb.txt"
    end

    #---
    cmd "#{JCONS_CMDS} bbb.txt < cmds.txt" do
      comment "up to date"
      stdout_equal [
        "jcons: already up-to-date: 'bbb.txt'",
      ]
    end

    #---
    create_file "inc1/aaa.inc", [
      '789',
    ]

    cmd "#{JCONS_CMDS} bbb.txt < cmds.txt" do
      comment "with existing include"
      stdout_equal [
        "cat -n < aaa.txt > bbb.txt",
      ]
      written_files "bbb.txt"
    end

  end

  #----------

  def test_simple_INPUT_OUTPUT
    create_file "cmds.txt", [
      "# INPUT: aaa.txt",
      "# OUTPUT: bbb.txt",
      "cat -n < aaa.txt > bbb.txt",
      "# INPUT: bbb.txt",
      "# OUTPUT: ccc.txt",
      "cat -n < bbb.txt > ccc.txt",
    ]

    #---
    create_file "aaa.txt", "123456\n"

    cmd "#{JCONS_CMDS} ccc.txt < cmds.txt" do
      comment "existing source"
      stdout_equal [
        "cat -n < aaa.txt > bbb.txt",
        "cat -n < bbb.txt > ccc.txt",
      ]
      created_files "bbb.txt", "ccc.txt"
    end

    #---
    cmd "#{JCONS_CMDS} ccc.txt < cmds.txt" do
      comment "up to date"
      stdout_equal [
        "jcons: already up-to-date: 'ccc.txt'",
      ]
    end

    #---
    create_file "aaa.txt", "abcdef\n"

    cmd "#{JCONS_CMDS} ccc.txt < cmds.txt" do
      comment "changed source"
      stdout_equal [
        "cat -n < aaa.txt > bbb.txt",
        "cat -n < bbb.txt > ccc.txt",
      ]
      written_files "bbb.txt", "ccc.txt"
    end

    #---
    create_file "bbb.txt", "ABCDEF\n"

    cmd "#{JCONS_CMDS} ccc.txt < cmds.txt" do
      comment "changed intermediate file"
      stdout_equal [
        "cat -n < aaa.txt > bbb.txt",
        "jcons: already up-to-date: 'ccc.txt'",
      ]
      written_files "bbb.txt"
    end

  end
end

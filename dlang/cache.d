//----------------------------------------------------------------------
// cache.d
//----------------------------------------------------------------------
// Copyright 2002-2017 Johan Holmberg.
//----------------------------------------------------------------------
// This file is part of "jcmds".
//
// "jcmds" is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// "jcmds" is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with "jcmds".  If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------

import core.stdc.stdlib;
import std.conv;
import std.stdio;
import std.string;

import filesystem;
import os;
import md5calc;

class Cache {

    Rdir m_cache_dir;

    this(Rdir d, string cache_dir) {
        m_cache_dir = d.find_dir(cache_dir);
    }

    void toString(scope void delegate(const(char)[]) sink) {
        sink("Cache[");
        sink(m_cache_dir.path());
        sink("]");
    }

    //----------------------------------------------------------------------

    void put(in Rdigest dep_sig, Rfile file) {
        string dep_sig_str = to!string(dep_sig);
        Rdigest xxx = dep_sig;
        string prefix = dep_sig_str[0..1]; // TODO: 1 eller 2 ???
        Rdir cache_dir = m_cache_dir.find_dir(prefix);
        bool ok = cache_dir.mkdir_p();
        if (!ok) return;

        Rfile cache_file = cache_dir.find_file(dep_sig_str);
        if (! cache_file.file_exist()) {
            ok = link_or_copy(file.path(), cache_file.path());
            if (!ok) return;
            cache_file.st_updated(dep_sig);
        }
    }

    //----------------------------------------------------------------------

    bool get(in Rdigest dep_sig, Rfile file) {
        string dep_sig_str = to!string(dep_sig);
        string prefix = dep_sig_str[0..1]; // TODO: 1 eller 2 ???
        Rdir cache_dir = m_cache_dir.find_dir(prefix);
        bool ok = cache_dir.mkdir_p();
        if (!ok) return false;

        Rfile cache_file = cache_dir.find_file(dep_sig_str);
        if (! cache_file.file_exist()) {
            return false;
        }
        if (cache_file.db_dep_sig() != dep_sig) {
            os.rm_f(cache_file.path());
            return false;
        }
        ok = link_or_copy(cache_file.path(), file.path());
        if (!ok) {
            os.rm_f(file.path());
            return false;
        }
        file.st_updated(dep_sig);
        writeln("Updated from cache: ", file.path());
        return true;
    }

    //----------------------------------------------------------------------

}

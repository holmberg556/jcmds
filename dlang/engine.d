//----------------------------------------------------------------------
// engine.d
//----------------------------------------------------------------------
// Copyright 2008-2017 Johan Holmberg.
//----------------------------------------------------------------------
// This file is part of "jcmds".
//
// "jcmds" is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// "jcmds" is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with "jcmds".  If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------

import std.conv;
import std.process;
import std.stdio;

import builder;
import cmd;
import cons;
import filesystem;
import global;
import process;

int g_nerrors = 0;

void add_error() {
    g_nerrors += 1;
}

void show_cmds() {
    foreach (cmd; Cmd.s_all_cmds) {
        writeln("----------------------------------------------------------------------");
        writeln("cmd: ", cmd.m_cmdline);
        foreach (src; cmd.srcs) {
            writeln("    src: ", src.path());
        }
        foreach (tgt; cmd.tgts) {
            writeln("    tgt: ", tgt.path());
        }
    }
}

struct ExecuteContext {
    BaseFun fun;
    void delegate(int status) set_exitstatus;
    bool noop;
}

class QueueRun {
    BaseFun[] arr;
    size_t n;

    bool empty() {
        return n == 0;
    }
    BaseFun pop() {
        n -= 1;
        auto res = arr[n];
        arr[n] = null;
        return res;
    }
    void push(BaseFun f) {
        if (n < arr.length) {
            arr[n] = f;
        }
        else {
            arr ~= f;
        }
        n += 1;
    }

    override string toString() {
        return "QueueRun[" ~ to!string(arr) ~ "]";
    }
}


class Engine {
    static bool got_sigint;
    static int njobs;

    static void delegate(BaseFun fun) set_runnable;
    static void delegate(BaseFun fun, string cmdline, bool noop_command, void delegate(int status) set_exitstatus) execute_cmd;

    QueueRun m_ready;
    ExecuteContext[int] m_executing;

    this(Rfile[] tgts) {
        m_ready = new QueueRun();
        m_ready.push(new FUN_top_level(tgts));

        set_runnable = &m_ready.push;
        execute_cmd = &_execute_cmd;
    }

    void _execute_cmd(BaseFun fun, string cmdline, bool noop_command, void delegate(int status) set_exitstatus) {
        int pid = process.start( cmdline, noop_command );
        m_executing[pid] = ExecuteContext(fun, set_exitstatus);
        fun.sem_acquire();
    }

    bool should_terminate_p() {
        return got_sigint || g_nerrors > 0 && !opt_keep_going;
    }

    void fun_call_next(BaseFun fun) {
        fun.sem_set(1);
        fun.call_next_method();
        if (fun.finished_p()) {
            return_to_caller(fun);
        }
        else {
            _queue_new_funs();
            fun.sem_release();
        }
    }

    void return_to_caller(BaseFun fun) {
        if (fun.m_caller !is null) {
            if (! fun.status_ok) fun.m_caller.status_ok = false;
            fun.m_caller.sem_release();
            fun.m_caller = null;
        }
    }

    void _queue_new_funs() {
        foreach_reverse (fun; BaseFun.mm_created_fun_arr) {
            m_ready.push(fun);
        }
    }

    void run() {
        bool terminate_warning_given = false;
        bool progress = true;
        while (m_executing.length > 0 || ! m_ready.empty()) {
            if (m_executing.length > 0) {
                if (should_terminate_p() && ! terminate_warning_given) {
                    terminate_warning_given = true;
                    writeln("jcons: *** waiting for commands to finish ...");
                }
                bool blocking =
                    (! progress || m_executing.length >= Engine.njobs);
                int pid;
                int exitstatus;
                process.wait(blocking, pid, exitstatus);
                if (pid != 0) {
                    auto it = (pid in m_executing);
                    if (it is null) {
                        throw new Exception("internal error: unknown pid");
                    }
                    (*it).set_exitstatus(exitstatus);
                    BaseFun sem_fun = (*it).fun;
                    m_executing.remove(pid);
                    sem_fun.sem_release();
                    if (should_terminate_p()) {
                        sem_fun.m_finish_after_cmd = true;
                    }
                    if (exitstatus & 127) {
                        sem_fun.signalled_command(exitstatus);
                    }
                }
                // always fall through
            }
            progress = false;
            if (! m_ready.empty()) {
                progress = true;
                BaseFun fun = m_ready.pop();
                if (should_terminate_p() && ! fun.m_finish_after_cmd) {
                    // unwind call stack
                    fun.release_semaphores();
                    return_to_caller(fun);
                }
                else {
                    // normal case
                    fun_call_next(fun);
                }
            }
        }
    }

}

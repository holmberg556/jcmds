//----------------------------------------------------------------------
// cmd.d
//----------------------------------------------------------------------
// Copyright 2008-2017 Johan Holmberg.
//----------------------------------------------------------------------
// This file is part of "jcmds".
//
// "jcmds" is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// "jcmds" is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with "jcmds".  If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------

import std.algorithm;
import std.stdio;

import cache;
import filesystem;
import cons;
import global;
import md5calc;

enum State { RAW, COOKING, DONE,  };

class Cmd {
    Rfile[] srcs;
    Rfile[] tgts;
    Cons m_cons;
    State m_state;

    Rfile[] extra_deps;
    string m_digest_cmdline;
    string m_cmdline;
    bool uses_cpp;
    bool m_uses_libpath;
    int m_exitstatus;

    Rdigest m_includes_digest;

    Rfile m_cmdname_file;
    bool m_cmdname_file_set;
    Cache m_build_cache;

    static Cmd[] s_all_cmds;

    this(Cons cons) {
        this.m_cons = cons;
        s_all_cmds ~= this;
    }

    void set_exitstatus(int status) {
        m_exitstatus = status;
    }

    //----------------------------------------------------------------------

    void append_sig(ref Rmd5 md5) {
        string local_cmdline = get_cmdline1();
        md5.append(local_cmdline);

        Rfile f = find_program();
        if (f !is null) {
            md5.append( f.path() );
            md5.append( f.db_content_sig() );

            foreach (dep; f.m_exe_deps) {
                md5.append( dep.path() );
                md5.append( dep.db_content_sig() );
            }
        }
    }

    //----------------------------------------------------------------------

    string get_targets_str() {
        // TODO: handle quoting of paths
        string res;
        foreach (i, tgt; tgts) {
            if (i > 0) res ~= " ";
            res ~= tgt.path();
        }
        return res;
    }

    //----------------------------------------------------------------------

    string get_cmdline1() {
        if (m_digest_cmdline is null) {
            return m_cmdline;
        }
        else {
            return m_digest_cmdline;
        }
    }

    string get_cmdline2() {
        return m_cmdline;
    }

    //----------------------------------------------------------------------

    Rfile find_program() {
        if (! m_cmdname_file_set) {
            string local_cmdline = get_cmdline2();
            auto t = local_cmdline.findSplitBefore(" ");
            string cmdname = t[0];

            m_cmdname_file = m_cons.program_path().find_program(cmdname);
            m_cmdname_file_set = true;
        }
        return  m_cmdname_file;
    }
    //----------------------------------------------------------------------


    bool all_tgts_exist() {
        foreach (tgt; tgts) {
            if (! tgt.file_exist()) {
                return false;
            }
        }
        return true;
    }

    //----------------------------------------------------------------------
    // pass on 'invalid' call to all targets

    void st_invalid_error()
    {
        foreach (tgt; tgts) {
            tgt.st_invalid_error();
        }
    }

    //----------------------------------------------------------------------
    // pass on 'propagate' call to all targets

    void st_propagate_error()
    {
        foreach (tgt; tgts) {
            tgt.st_propagate_error();
        }
    }

    Cache build_cache() {
        return m_build_cache !is null ? m_build_cache : g_build_cache;
    }

    void set_cachedir(string cachedir) {
        Rdir d = s_conscript_dir !is null ? s_conscript_dir : s_curr_dir;
        m_build_cache = new Cache(d, cachedir);
    }
}

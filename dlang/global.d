//----------------------------------------------------------------------
// global.d
//----------------------------------------------------------------------
// Copyright 2008-2017 Johan Holmberg.
//----------------------------------------------------------------------
// This file is part of "jcmds".
//
// "jcmds" is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// "jcmds" is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with "jcmds".  If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------

import cache;

Cache g_build_cache;

bool opt_accept_existing_target;
bool opt_build_all;
bool opt_cache_force;
bool opt_keep_going;
bool opt_perl_cons;
bool opt_quiet;
bool opt_remove;
bool opt_trust_mtime;

int opt_njobs;

string opt_cachedir;

//----------------------------------------------------------------------
// os.d
//----------------------------------------------------------------------
// Copyright 2008-2017 Johan Holmberg.
//----------------------------------------------------------------------
// This file is part of "jcmds".
//
// "jcmds" is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// "jcmds" is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with "jcmds".  If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------

import std.file;
import std.string;
import core.sys.posix.unistd;


void mkdir_p(string path) {
    assert(false);
}

void rm_f(string path) {
    if (exists(path)) {
        remove(path);
    }
}

bool is_file(string path) {
    return exists(path) && isFile(path);
}

bool is_windows() {
    return false;
}

bool link_or_copy(string src, string tgt) {
    bool ok;
    static if (is_windows()) {
        ok = copy(src, tgt);
    }
    else {
        int err = link(toStringz(src), toStringz(tgt));
        ok = (err == 0);
    }
    return ok;
}

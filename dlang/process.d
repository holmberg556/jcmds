//----------------------------------------------------------------------
// process.d
//----------------------------------------------------------------------
// Copyright 2008-2017 Johan Holmberg.
//----------------------------------------------------------------------
// This file is part of "jcmds".
//
// "jcmds" is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// "jcmds" is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with "jcmds".  If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------

import std.stdio;
import std.string;
import std.process;
import core.stdc.stdlib;
import core.sys.posix.unistd;
import core.sys.posix.sys.wait;

//----------------------------------------------------------------------

int start(string cmdline, bool noop) {
    int pid = fork();
    if (pid == -1) {
        perror("fork");
        exit(1);
    }
    else if (pid == 0) {
        if (noop) {
            _exit(0);
        }
        else {
            string[] argv = ["sh", "-c", cmdline];
            execvp("sh", argv);
            _exit(126);
        }
    }
    else {
        return pid;
    }
    assert(0);
}

//----------------------------------------------------------------------

void wait(bool blocking, out int pid, out int status) {
    int wait_arg = blocking ? 0 : WNOHANG;
    pid = waitpid(0, &status, wait_arg);
    if (pid == -1) {
        perror("waitpid");
        exit(1);
    }
}

//----------------------------------------------------------------------

debug(testmain) {
    void main() {
        start("echo 11 ; sleep 5; echo 22", false);
        start("echo aa ; sleep 4; echo bb", false);
        start("echo xx ; sleep 6; echo yy", false);
        while (true) {
            int pid;
            int status;
            wait(true, pid, status);
            writeln("wait ", pid, ", ", status);
        }
    }
}

//----------------------------------------------------------------------

// TODO: compare 'waitpid' usage with AUP
// TODO: simple command lines --> split + no shell
// TODO: use "ruby -S" for *.rb ?
// TODO: windows port

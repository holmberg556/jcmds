//----------------------------------------------------------------------
// builder.d
//----------------------------------------------------------------------
// Copyright 2008-2017 Johan Holmberg.
//----------------------------------------------------------------------
// This file is part of "jcmds".
//
// "jcmds" is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// "jcmds" is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with "jcmds".  If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------

import std.algorithm;
import core.stdc.signal;
import core.stdc.stdlib;
import std.conv;
import std.digest.md;
import std.file;
import std.stdio;

import cache;
import cmd;
import cons;
import filesystem;
import global;
import os;
import md5calc;
import engine;

struct IncludesTree {
    Rfile[][Rfile] deps;
    Rfile[][Rfile] inverted_deps;
    Rfile[] finished;
    bool[Rfile] visited;

    void gen_inverted_deps() {
        foreach (k,vs; deps) {
            foreach (v; vs) {
                inverted_deps[v] ~= k;
            }
        }
    }

    void calculate_finished() {
        foreach (k,vs; deps) {
            k.color = Color.WHITE;
            foreach (v; vs) {
                v.color = Color.WHITE;
            }
        }
        foreach (k,vs; deps) {
            if (k.color == Color.WHITE) {
                _calculate_finished_dfs(k);
            }
        }
    }

    void _calculate_finished_dfs(Rfile f) {
        f.color = Color.GREY;
        if (auto p = f in deps) {
            foreach (f2; *p) {
                if (f2.color == Color.WHITE) {
                    _calculate_finished_dfs(f2);
                }
            }
        }
        f.color = Color.BLACK;
        finished ~= f;
    }

    void sccs_prepare() {
        foreach (f; finished) {
            f.color = Color.WHITE;
        }
    }

    void sccs_dfs(Rfile f, ref Rfile[] group_elements) {
      f.color = Color.GREY;
      group_elements ~= f;
      if (auto p = f in inverted_deps) {
          foreach (f2; *p) {
              if (f2.color == Color.WHITE) {
                  sccs_dfs(f2, group_elements);
              }
          }
      }
      f.color = Color.BLACK;
    }
}

//======================================================================
// Base class

bool[BaseFun] s_active_funs;
bool opt_log_states;

class BaseFun {
    bool m_finish_after_cmd;
    BaseFun m_caller;
    bool status_ok;
    void delegate() next;
    BaseFun m_next_waiting_sem;   // TODO: better name ???
    int m_sem_count;

    this() {
        status_ok = true;

        // collect all created funs
        mm_created_fun_arr ~= this;

        // connect caller and callee
        m_caller = mm_current_fun;
        if (m_caller !is null) {
            m_caller.sem_acquire();
        }
        s_active_funs[this] = true;
    }

    void logg(string method) {
        if (opt_log_states) {
            writeln("                                       ********** ",
                    method.find('.')[1..$], "[", name(), "]");
        }
    }

    void maybe_FUN_update_tgt(Rfile node) {
        if (node.build_ok == Status.UNKNOWN) {
            new FUN_update_tgt(node);
        }
        else {
            if (node.build_ok == Status.ERROR) this.status_ok = false;
        }
    }

    override string toString() {
        string klass = typeid(this).toString().find('.')[1..$];
        return klass ~ "[" ~ name() ~ "]";
    }
    string name()       { return "???"; }

    void execute_cmd(string cmdline, bool noop, void delegate(int status) set_exitstatus) {
        Engine.execute_cmd(this, cmdline, noop, set_exitstatus);
    }

    void sem_set(int count) {
        m_sem_count = count;
    }

    void sem_acquire() {
        m_sem_count += 1;
    }

    void sem_release() {
        m_sem_count -= 1;
        assert(m_sem_count >= 0);
        if (m_sem_count == 0) {
            Engine.set_runnable(this);
        }
    }

    void release_semaphores() {}
    void signalled_command(int exitstatus) {}

    bool finished_p() {
      return next is null;
    }

    static BaseFun mm_current_fun;
    static BaseFun[] mm_created_fun_arr;

    void call_next_method() {
        mm_current_fun = this;
        mm_created_fun_arr = mm_created_fun_arr[0..0]; // reset for this call
        while (m_sem_count == 1 && next !is null) {
            next();
        }
        if (next is null) {
            s_active_funs.remove(this);
        }
    }
}

//======================================================================
// FUN_get_includes_tree
//
// Collect the include tree of a file.
// While traversing the tree, "update" files that can be built.
// Each node is processed in parallel, so files that take a long time
// to generate, will not stall the collecting in other parts of the
// include tree.
//

class FUN_get_includes_tree : BaseFun {
    int m_level;
    CppPathMap cpp_path_map;
    Rfile node;
    IncludesTree * m_includes_tree;

    this(CppPathMap cpp_path_map, Rfile node, IncludesTree * includes_tree, int level) {
        debug (logging) {
            writeln("LOG: FUN_get_includes_tree CREATE ", node.path(), " level=", level);
        }
        this.m_level = level;
        this.cpp_path_map = cpp_path_map;
        this.node = node;
        this.m_includes_tree = includes_tree;
        next = &STATE_start;
    }

    override string name() {
        string res;
        res ~= "FUN_get_includes_tree[";
        res ~= node.name;
        res ~= "]";
        return res;
    }

    void STATE_start() {
        debug (state) logg(__FUNCTION__);
        maybe_FUN_update_tgt(node);
        next = &STATE_node_updated;
    }

    void STATE_node_updated() {
        debug (state) logg(__FUNCTION__);
        if (! node.st_ok_p()) {
            // stop after failure. Caller will check for this.
            status_ok = false;
            next = null;
            return;
        }

        foreach (inc; cpp_path_map.find_node_includes(node)) {
            m_includes_tree.deps[node] ~= inc;
            if (inc !in m_includes_tree.visited) {
                m_includes_tree.visited[inc] = true;
                if (! cpp_path_map.has_result(inc)) {
                    new FUN_get_includes_tree(cpp_path_map, inc, m_includes_tree, m_level+1);
                }
            }
        }
        next = &STATE_finish;
    }

    //----------------------------------------------------------------------

    void STATE_finish() {
        debug (state) logg(__FUNCTION__);
        next = null;
    }
}

//======================================================================
// FUN_includes_md5
//
// Calculate MD5 of include tree

class FUN_includes_md5 : BaseFun {
    CppPathMap cpp_path_map;
    Rfile node;
    IncludesTree m_includes_tree;

    this(CppPathMap cpp_path_map, Rfile node) {
        debug (logging) {
            writeln("LOG: FUN_includes_md5 CREATE ", node.path());
        }
        this.cpp_path_map = cpp_path_map;
        this.node = node;
        next = &STATE_start;
    }

    override string name() { return "...???..."; }

    //----------------------------------------------------------------------

    void STATE_start() {
        debug (state) logg(__FUNCTION__);
        m_includes_tree.visited = null;

        if (node !in m_includes_tree.visited) {
            m_includes_tree.visited[node] = true;
            if (! cpp_path_map.has_result(node)) {
                new FUN_get_includes_tree(cpp_path_map, node, &m_includes_tree, 0);
            }
        }
        next = &STATE_finish;
    }

    //----------------------------------------------------------------------

    void STATE_finish() {
        debug (state) logg(__FUNCTION__);
        if (! status_ok) {
            next = null;
            return;
        }

        m_includes_tree.calculate_finished();
        m_includes_tree.gen_inverted_deps();

        if (m_includes_tree.finished.length == 0) {
            m_includes_tree.finished ~= node;
        }

        m_includes_tree.sccs_prepare();
        m_includes_tree.finished.reverse();

        // Find strongly connected components. The second part of the algorithm
        // visiting the nodes in the order given by the "finishing times" from
        // the previous part.

        Rfile[] group_elements;
        ulong[] group_offset1;
        ulong[] group_offset2;
        foreach (it; m_includes_tree.finished) {
            if (it.color == Color.WHITE) {
                ulong x1 = group_elements.length;
                group_offset1 ~= x1;
                m_includes_tree.sccs_dfs(it, group_elements);
                ulong x2 = group_elements.length;
                group_offset2 ~= x2;
            }
        }

        // Calculate MD5 for each of the "nodes". The value depend on the
        // recursively included files, so the order of traversal of
        // group_offset1/group_offset2 is significant.

        foreach_reverse (i; 0..group_offset1.length) {
            ulong j1 = group_offset1[i];
            ulong j2 = group_offset2[i];
            if (j2 == j1 + 1) {
                // *one* file without cycles
                Rfile node = group_elements[j1];

                if (! cpp_path_map.has_result(node)) {
                    // no cached value
                    cpp_path_map.put_result(node, node_calc_md5(node));
                }
            }
            else {
                // a cycle with more than one file
                Rfile[] cycle = group_elements[j1..j2];
                Rdigest digest = node_calc_md5_cycle(cycle);
                foreach (it; cycle) {
                    cpp_path_map.put_result(it, digest);
                }
            }
        }
        next = null;
    }

    //-----------------------------------

    Rdigest node_calc_md5(Rfile node)
    {
        auto p = (node in m_includes_tree.deps);
        if (p is null || (*p).length == 0) {
            // no dependencies ==> use content
            Rdigest digest = node.db_content_sig();
            return digest;
        }

        Rdigest[] digests;
        digests ~= node.db_content_sig();
        foreach (node2; *p) {
            digests ~= cpp_path_map.get_result(node2);
        }
        digests.sort(); // TODO: needed in D?
        Rdigest digest = calc_digest(digests);
        return digest;
    }

    //-----------------------------------

    Rdigest node_calc_md5_cycle(Rfile[] cycle) {
        // set temporarily to make rest of code easier
        Rdigest tmp_digest = Rmd5.digest("temporary-md5-signature");
        foreach (it; cycle) {
            cpp_path_map.put_result(it, tmp_digest);
        }

        // collect common digest for all files in cycle
        Rdigest[] digests;
        foreach (it; cycle) {
            digests ~= Rmd5.digest(it.path());
            digests ~= node_calc_md5(it);
        }
        digests.sort();
        Rdigest digest = calc_digest(digests);
        return digest;
    }

}


//======================================================================
// FUN_update_tgt

class FUN_update_tgt : BaseFun {
    Rfile tgt;
    Rfile[] info;

    this(Rfile tgt) {
        debug (logging) {
            writeln("LOG: FUN_update_tgt CREATE ", tgt.path());
        }
        this.tgt = tgt;
        next = &STATE_start;
    }

    override string name() {
        return tgt.path();
    }

    //----------------------------------------------------------------------

    void STATE_start() {
        debug (state) logg(__FUNCTION__);
        if (tgt.is_source()) {
            if (tgt.file_exist()) {
                if (tgt.m_top_target) {
                    writeln("jcons: already up-to-date: '", tgt.path(), "' (source file)");
                }
                tgt.st_source();
                STATE_finish();
            }
            else {
                report_error("don't know how to build", tgt.path());
                tgt.st_propagate_error();
                STATE_finish();
            }
        }
        else {
            // 'tgt' not source
            Cmd cmd = tgt.cmd;
            if (cmd.m_state == State.RAW) {
                cmd.m_state = State.COOKING;
                foreach (src; cmd.srcs) {
                    maybe_FUN_update_tgt(src);
                }
                foreach (extra_dep; cmd.extra_deps) {
                    maybe_FUN_update_tgt(extra_dep);
                }
                if (cmd.m_uses_libpath) {
                    foreach (fs_lib; cmd.m_cons.libpath_map().fs_libs()) {
                        maybe_FUN_update_tgt(fs_lib);
                    }
                }
                next = &STATE_srcs_updated;
            }
            else if (cmd.m_state == State.COOKING) {
                this.sem_acquire();

                // add 'this' to 'waiting sem' list of tgt
                this.m_next_waiting_sem = tgt.m_waiting_sem;
                tgt.m_waiting_sem = this;

                next = &STATE_updated_by_other;
            }
            else if (cmd.m_state == State.DONE) {
                status_ok = tgt.st_ok_p();
                next = null;
            }
        }
    }

    //----------------------------------------------------------------------

    void STATE_updated_by_other() {
        debug (state) logg(__FUNCTION__);
        status_ok = tgt.st_ok_p();
        next = null;
    }

    //----------------------------------------------------------------------

    void STATE_srcs_updated() {
        debug (state) logg(__FUNCTION__);
        Cmd cmd = tgt.cmd;
        if (! status_ok) {
            cmd.st_propagate_error();
            STATE_finish();
            return;
        }
        if (cmd.uses_cpp) {
            CppPathMap cpp_path_map = cmd.m_cons.cpp_path();
            foreach (src; cmd.srcs) {
                Rfile[] incs = cpp_path_map.find_node_includes(src);
                foreach (inc; incs) {
                    if (! cpp_path_map.has_result(inc)) {
                        new FUN_includes_md5(cpp_path_map, inc);
                    }
                    info ~= inc;
                }
            }
        }

        Rfile f = cmd.find_program();
        if (f !is null) {
            maybe_FUN_update_tgt(f);
            if (f.m_exe_deps !is null) {
                auto deps = f.m_exe_deps;
                foreach (dep; deps) {
                    maybe_FUN_update_tgt(dep);
                }
            }
        }
        next = &STATE_maybe_execute_cmd;
    }

    //----------------------------------------------------------------------
    // TODO: rename to a better name

    void STATE_maybe_execute_cmd() {
        debug (state) logg(__FUNCTION__);
        Cmd cmd = tgt.cmd;

        if (! status_ok) {
            cmd.st_propagate_error();
            STATE_finish();
            return;
        }

        if (cmd.uses_cpp) {
            Rmd5 md5;
            CppPathMap cpp_path_map = cmd.m_cons.cpp_path();
            foreach (inc; info) {
                md5.append( cpp_path_map.get_result(inc) );
            }
            cmd.m_includes_digest = md5.digest();
        }

        foreach (tgt_nr, tgt; cmd.tgts) {
            tgt.wanted_digest = node_new_dep_sig(tgt, to!int(tgt_nr));
        }
        if (any!need_update_p(cmd.tgts)) {
            foreach (tgt; cmd.tgts) {
                if (! opt_accept_existing_target) {
                    // OK to fail removing file
                    os.rm_f(tgt.path());
                }
            }
            foreach (tgt; cmd.tgts) {
                try {
                    mkdirRecurse(tgt.parent().path());
                }
                catch (FileException) {
                    report_error("expected directory", tgt.parent().path());
                    cmd.st_propagate_error();
                    STATE_finish();
                    return;
                }
            }
            Cache build_cache = cmd.build_cache();
            if (build_cache !is null) {
                bool all_ok = true;
                foreach (tgt; cmd.tgts) {
                    all_ok = all_ok && build_cache.get(tgt.wanted_digest, tgt);
                }
                if (all_ok) {
                    // TODO: do anything more ???
                    STATE_finish();
                    return;
                }
            }

            bool noop = (opt_accept_existing_target && cmd.all_tgts_exist());
            string cmdline = cmd.get_cmdline2();

            if (opt_quiet) {
                writeln("Build ", cmd.get_targets_str());
                stdout.flush();
            }
            else {
                writeln(cmdline);
                stdout.flush();
            }

            execute_cmd(cmdline, noop, &cmd.set_exitstatus);
            next = &STATE_cmd_executed;
        }
        else {
            // ! tgts_need_update
            foreach (tgt; cmd.tgts) {
                tgt.st_propagate_ok();
                Cache build_cache = cmd.build_cache();
                if (build_cache !is null && opt_cache_force) {
                    build_cache.put(tgt.db_dep_sig(), tgt);
                }
                if (tgt.m_top_target) {
                    writeln("jcons: already up-to-date: '", tgt.path(), "'");
                }
            }
            STATE_finish();
        }
    }

    //----------------------------------------------------------------------

    void STATE_cmd_executed() {
        debug (state) logg(__FUNCTION__);
        Cmd cmd = tgt.cmd;
        if (cmd.m_exitstatus != 0) {
            report_error("error building", tgt.path());
            cmd.st_propagate_error();
            STATE_finish();
            return;
        }

        string missing_names = "";
        foreach (tgt; cmd.tgts) {
            if (! tgt.file_exist_FORCED()) {
                if (missing_names != "") missing_names ~= " ";
                missing_names ~= tgt.path();
            }
        }
        if (missing_names != "") {
            report_error("tgts not created", missing_names);
            cmd.st_propagate_error();
            STATE_finish();
            return;
        }

        // update tgt sigs
        foreach (tgt; cmd.tgts) {
            Rdigest dep_sig = tgt.wanted_digest;
            tgt.st_updated(dep_sig);
            Cache build_cache = cmd.build_cache();
            if (build_cache !is null) {
                build_cache.put(dep_sig, tgt);
            }
        }
        STATE_finish();
    }

    //----------------------------------------------------------------------

    void STATE_finish() {
        debug (state) logg(__FUNCTION__);
        release_semaphores();
        status_ok = tgt.st_ok_p();
        next = null;
    }

    //----------------------------------------------------------------------

    override void signalled_command(int exitstatus) {
        if ((exitstatus & 127) == SIGINT) {
            writeln("jcons: *** [", tgt.path(), "] interrupted");
        }
        else {
            writeln("jcons: *** [", tgt.path(), "] interrupted by ", exitstatus);
        }
    }

    //-----------------------------------
    // Release other FUN_update_tgt objects also looking at this node.

    override void release_semaphores() {
        Cmd cmd = tgt.cmd;
        if (cmd !is null) {
            if (cmd.m_state == State.COOKING) {
                foreach (tgt; tgt.cmd.tgts) {
                    BaseFun waiting_fun = tgt.m_waiting_sem;
                    while (waiting_fun !is null) {
                        waiting_fun.sem_release();
                        waiting_fun = waiting_fun.m_next_waiting_sem;
                    }
                    tgt.m_waiting_sem = null;
                }
            }
            cmd.m_state = State.DONE;
        }
    }

    //-----------------------------------
    // An error that may halt 'rcons', unless the -k option was given.

    void report_error(string err, string arg) {
        writeln("jcons: error: ", err, " '", arg, "'");
        engine.add_error();
    }

    //-----------------------------------
    // Tell if a target file needs to be updated.

    static bool need_update_p(Rfile tgt) {
        if (opt_build_all) {
            return true;
        }
        if (! tgt.file_exist()) {
            return true;
        }
        if (tgt.db_dep_sig() == tgt.wanted_digest) {
            return false;
        }
        else {
            return true;
        }
    }

    //-----------------------------------
    // Helper method to 'node_new_dep_sig'.
    // Returns the part of the digest all targets of a command have in common.

    static Rdigest cmd_new_dep_sig(Cmd cmd)
    {
        // TODO: only do this once (when there are several targets)
        // TODO: maybe move 'md5' or whole method to 'Cmd' class.
        Rmd5 md5;
        foreach (src; cmd.srcs) {
            md5.append(src.db_content_sig());
            md5.append(src.path());
        }
        if (cmd.extra_deps !is null) {
            Rdigest[] digests;
            foreach (dep; cmd.extra_deps) {
                digests ~= dep.db_content_sig();
            }
            sort(digests);
            foreach (digest; digests) {
                md5.append(digest);
            }
        }
        if (cmd.uses_cpp) {
            md5.append(cmd.m_includes_digest);
        }

        // if (cmd.m_uses_libpath) {
        //     file_arr_t & fs_libs = cmd.m_cons.libpath_map().fs_libs();
        //     foreach (auto & it : fs_libs) {
        //         md5.append(it.db_content_sig());
        //     }
        // }

        cmd.append_sig(md5);
        return md5.digest();
    }

    //-----------------------------------
    // The digest a target file should have to be considered up-to-date.

    static Rdigest node_new_dep_sig(Rfile tgt, int tgt_nr) {
        Rdigest dep_sig = cmd_new_dep_sig(tgt.cmd);
        Rmd5 md5;
        md5.append(dep_sig);
        md5.append(tgt_nr);
        md5.append(tgt.name);
        return md5.digest();
    }

}

//======================================================================
// FUN_top_level

class FUN_top_level : BaseFun {
    Rfile[] tgts;

    this(Rfile[] tgts) {
        this.tgts = tgts;
        next = &STATE_start;
    }

    override string name() { return "TOP_LEVEL"; }

    void STATE_start() {
        debug (state) logg(__FUNCTION__);
        foreach (tgt; tgts) {
            maybe_FUN_update_tgt(tgt);
        }
        next = &STATE_finish;
    }

    void STATE_finish() {
        debug (state) logg(__FUNCTION__);
        next = null;
    }
}

//======================================================================
// END
//======================================================================

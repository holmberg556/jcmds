//----------------------------------------------------------------------
// jcmds.d
//----------------------------------------------------------------------
// Copyright 2008-2017 Johan Holmberg.
//----------------------------------------------------------------------
// This file is part of "jcmds".
//
// "jcmds" is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// "jcmds" is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with "jcmds".  If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------

import std.algorithm;
import core.stdc.stdlib;
import core.stdc.signal;
import std.getopt;
import std.path;
import std.stdio;
import std.string;

import argumentparser;
import builder;
import cmd;
import cons;
import filesystem;
import global;
import os;
import engine;
import setup;

bool opt_version;
bool opt_verbose;
bool opt_list_targets;
bool opt_list_commands;
string[] opt_args;
string[] cmds_files;

string g_CHDIR;
string[] g_INPUT;
string[] g_OUTPUT;
string[] g_DEPEND;
string[] g_EXE_DEPEND;
string g_CACHEDIR;

//----------------------------------------------------------------------

class ConsCache {
    Cons[string] mIncsConsMap;

    Cons get(string[] incs) {
        string idx = join(incs, " ");
        auto p = (idx in mIncsConsMap);
        if (p is null) {
            auto cons = new Cons(); // TODO: change
            cons.set_cpp_path(incs);
            mIncsConsMap[idx] = cons;
            return cons;
        }
        else {
            return *p;
        }
    }
}

//----------------------------------------------------------------------

struct CmdReader {
    string[] mArgs;
    string mProg;

    this(string prog, string[] args) {
        mArgs = args;
        mProg = baseName(prog);
    }

    void process(string cmdline, ref ConsCache cons_cache, string subdir) {
        string[] infiles;
        string infile;
        string outfile;
        string[] incs;

        Cmd cmd;

        string used_cmdline = (subdir == "." ?
                               cmdline :
                               "cd " ~ subdir ~ " && " ~ cmdline);

        //--------------------
        if (g_INPUT.length > 0 && g_OUTPUT.length > 0) {
            auto cons = cons_cache.get(incs);
            cmd = cons.Command(g_OUTPUT, g_INPUT, used_cmdline, false);
        }

        //--------------------
        else if (g_INPUT.length > 0 && g_EXE_DEPEND.length > 0 && cmdline == ":") {
            auto cons = cons_cache.get(incs);
            foreach (tgt; g_EXE_DEPEND) {
                foreach (src; g_INPUT) {
                    cons.ExeDepends(tgt, src);
                }
            }
        }

        //--------------------
        else if (g_INPUT.length > 0 && g_DEPEND.length > 0 && cmdline == ":") {
            auto cons = cons_cache.get(incs);
            foreach (tgt; g_DEPEND) {
                foreach (src; g_INPUT) {
                    cons.Depends(tgt, src);
                }
            }
        }

        //--------------------
        else if ((is_cmd("gcc") || is_cmd("g++") || is_cmd("c++")) && opt_c() && src(infile) && opt_o(outfile)) {
            string[] args = mArgs;
            string digest_cmdline = mProg;
            while (args.length > 0) {
                if (args[0] == "-I" && args.length >= 2) {
                    incs ~= args[1];
                    args = args[2..$];
                }
                else if (startsWith(args[0], "-I")) {
                    incs ~= args[0][2..$];
                    args = args[1..$];
                }
                else {
                    digest_cmdline ~= " " ~ args[0];
                    args = args[1..$];
                }
            }

            auto cons = cons_cache.get(incs);
            cmd = cons.Command(outfile, infile, used_cmdline, digest_cmdline, true);
        }

        //--------------------
        else if ((is_cmd("gcc") || is_cmd("g++") || is_cmd("c++")) && ! opt_c() &&
            objs(infiles) &&
            (archives(infiles, false) || true) &&
            opt_o(outfile)) {

            auto cons = new Cons();
            cmd = cons.Command(outfile, infiles, used_cmdline);
        }

        //--------------------
        else if (is_cmd("ar") && ar_option(mArgs[0]) && nargs() >= 3) {
            outfile = mArgs[1];
            infiles ~= mArgs[2..$];

            auto cons = new Cons();
            cmd = cons.Command(outfile, infiles, used_cmdline);
        }

        //--------------------
        else if (is_cmd("cp") && nargs() == 2 && ! is_option(mArgs[0])) {
            infile  = mArgs[0];
            outfile = mArgs[1];

            auto cons = new Cons();
            cmd = cons.Command(outfile, infile, used_cmdline);
        }

        //--------------------
        else if (has_input(infile) && has_output(outfile)) {
            auto cons = new Cons();
            cmd = cons.Command(outfile, infile, used_cmdline);
        }

        //--------------------
        else {
            writeln("                                   ------ Unknown command: ", used_cmdline);
            exit(1);
        }

        if (g_CACHEDIR !is null) {
            cmd.set_cachedir(g_CACHEDIR);
        }
    }

    int is_option(string arg) {
        return arg.length > 1 && arg[0] == '-';
    }

    ulong nargs() {
        return mArgs.length;
    }

    bool is_cmd(string name) {
        return mProg == name || mProg.endsWith(name) && mProg[$ - name.length - 1] == '-';
    }

    bool opt_c() {
        return find(mArgs, "-c").length != 0;
    }

    bool opt_o(ref string file) {
        auto p = find(mArgs, "-o");
        if (p.length < 2) return false;
        file = p[1];
        return true;
    }

    int ar_option(string arg) {
        return arg == "rc" || arg == "cq" || arg == "qc";
    }

    bool src(ref string file) {
        int nfound = 0;
        foreach (arg; mArgs) {
            string ext = extension(arg);
            if (ext == ".c" || ext == ".cpp" || ext == ".cc") {
                file = arg;
                nfound += 1;
            }
        }
        return nfound == 1;
    }

    bool objs(ref string[] files) {
        files = null;
        foreach (arg; mArgs) {
            if (extension(arg) == ".o") {
                files ~= arg;
            }
        }
        return files.length >= 1;
    }

    bool archives(ref string[] files, bool clear = false) {
        if (clear) files = null;
        foreach (arg; mArgs) {
            if (extension(arg) == ".a") {
                files ~= arg;
            }
        }
        return files.length >= 1;
    }

   bool has_input(ref string file) {
       int count = 0;
       foreach (i, arg; mArgs) {
           if (arg == "<" && i < mArgs.length - 1) {
               file = mArgs[i+1];
               count += 1;
            }
        }
       return count == 1;
    }

    bool has_output(ref string file) {
       int count = 0;
       foreach (i, arg; mArgs) {
           if (arg == ">" && i < mArgs.length - 1) {
               file = mArgs[i+1];
               count += 1;
            }
        }
       return count == 1;
    }

 }

//----------------------------------------------------------------------

void split_line(string line, ref string arg0, ref string[] args) {
    split_string(line, args);
    arg0 = args[0];
    args = args[1..$];
}

void split_string(string str, ref string[] parts) {
    ulong j = str.length;
    while (j > 0 && str[j-1] == ' ') {
        j -= 1;
    }
    ulong i = 0;
    while (true) {
        while (i < j && str[i] == ' ') {
            i += 1;
        }
        if (i >= j) {
            break;
        }
        ulong k = i;
        while (k < j && str[k] != ' ') {
            k += 1;
        }
        parts ~= str[i..k];
        i = k;
    }
}

//----------------------------------------------------------------------

void read_commands(ref File f, ref ConsCache[string] cons_cache_by_dir, string subdir) {
    push_dir(subdir);
    scope (exit) pop_dir();

    string cwd = ".";
    foreach (line_buff; f.byLine()) {
        string line = line_buff.idup;
        //writeln("line=", [line]);
        auto line2 = strip(line);
        if (line2.length == 0) continue;

        string arg0;
        string[] args;
        split_line(line, arg0, args);

        if (arg0 == "#" && args.length == 2 && args[0] == "CHDIR:") {
            g_CHDIR = args[1];
            continue;
        }
        if (line.startsWith("# INPUT: ")) {
            g_INPUT ~= line[9..$];
            continue;
        }
        else if (line.startsWith("# OUTPUT: ")) {
            g_OUTPUT ~= line[10..$];
            continue;
        }
        else if (line.startsWith("# DEPEND: ")) {
            g_DEPEND ~= line[10..$];
            continue;
        }
        else if (line.startsWith("# EXE_DEPEND: ")) {
            g_EXE_DEPEND ~= line[14..$];
            continue;
        }
        else if (line.startsWith("# CACHEDIR: ")) {
            g_CACHEDIR = line[12..$];
            continue;
        }

        if (line[0] == '#') continue;

        if (arg0 == "cd" && args.length > 2 && args[1] == "&&") {
            cwd = args[0];
            arg0 = args[2];
            args = args[3..$];
        }

        auto cmd_reader = CmdReader(arg0, args);

        if (cwd !in cons_cache_by_dir) {
            cons_cache_by_dir[cwd] = new ConsCache();
        }

        if (cwd != ".") {
            push_dir(cwd);
            cmd_reader.process(line, cons_cache_by_dir[cwd], subdir);
            pop_dir();
        }
        else if (g_CHDIR.length == 0) {
            cmd_reader.process(line, cons_cache_by_dir[cwd], subdir);
        }
        else {
            push_dir(g_CHDIR);
            string line_with_chdir = "cd " ~ g_CHDIR ~ " && " ~ line;
            cmd_reader.process(line_with_chdir, cons_cache_by_dir[cwd], subdir);
            pop_dir();
        }

        cwd = ".";
        g_CHDIR = null;
        g_INPUT = null;
        g_OUTPUT = null;
        g_DEPEND = null;
        g_EXE_DEPEND = null;
        g_CACHEDIR = null;
    }
}

//----------------------------------------------------------------------

void parse_arguments(string[] argv) {
    auto p = new ArgumentParserConcrete("jcons_cmds");
    p.add("-q", "--quiet",       "be more quiet",      &opt_quiet);
    p.add("-j", "--parallel",    "build in parallel",  &Engine.njobs, 1)
        .metavar("N")
        ;
    p.add("-k", "--keep-going",  "continue after errors",  &opt_keep_going);
    p.add("-B", "--always-make", "always build targets", &opt_build_all);
    p.add("-r", "--remove",      "remove targets", &opt_remove);
    p.add("",   "--accept-existing-target", "make updating an existing target a nop",
          &opt_accept_existing_target);

    p.add("", "--dont-trust-mtime", "always consult files for content digest",
          &opt_trust_mtime, true);

    p.add("-f", "--file",        "name of *.cmds file", &cmds_files);
    p.add("-v", "--verbose",     "be more verbose",     &opt_verbose);
    p.add("",   "--version",     "show version",       &opt_version);

    p.add("",   "--cache-dir",   "name of cache directory", &opt_cachedir);
    p.add("",   "--cache-force", "copy existing files into cache", &opt_cache_force);

    p.add("-p", "--list-targets", "list known targets", &opt_list_targets);
    p.add("",   "--list-commands","list known commands", &opt_list_commands);
    p.add("",   "--log-states",   "log state machine",  &opt_log_states);


    p.addpos("arg", "target", 0, -1, &opt_args);
    p.parse_args(argv);

    if (opt_version) {
        writeln("jcons-cmds version 0.20");
        writeln();
        writeln("Copyright 2002-2017 Johan Holmberg <holmberg556@gmail.com>.");
        writeln("This is non-free software. There is NO warranty; not even");
        writeln("for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.");
        exit(0);
    }
}

//----------------------------------------------------------------------

int lookup_targets(string[] targets, ref Rentry entries[], ref Rfile[] tgts) {
    if (targets.length == 0) {
        targets ~= ".";
    }
    int errors = 0;
    foreach (target; targets) {
        Rentry tgt = filesystem.cwd.lookup_entry(target);
        if (tgt is null) {
            writeln("jcons: error: don't know how to build '", target, "'");
            errors += 1;
            continue;
        }
        if (auto f = cast(Rfile)tgt) {
            tgts ~= f;
            f.m_top_target = true;
            entries ~= f;
        }
        else  if (auto d = cast(Rdir)tgt) {
            d.append_files_under(tgts);
            entries ~= d;
        }
        else {
            writeln("jcons: error: internal error");
            exit(1);
        }
    }
    return errors;
}

//----------------------------------------------------------------------

int
main(string[] argv)
{
    parse_arguments(argv);
    cons.initialize();
    setup_signals();

    if (opt_cachedir != "") {
        set_cachedir(opt_cachedir);
    }

    alias ConsCache_map_t = ConsCache[string];
    ConsCache_map_t[string] cons_cache_by_dir2;

    if (cmds_files.length == 0) {
        if ("." !in cons_cache_by_dir2) {
            cons_cache_by_dir2["."] = null;
        }
        read_commands(stdin, cons_cache_by_dir2["."], ".");
    }
    else {
        foreach (fname; cmds_files) {
            auto f = File(fname, "r");
            if (opt_verbose) {
                writeln("+++ reading command file '", fname, "'");
            }
            string dir = dirName(fname);
            if (dir !in cons_cache_by_dir2) {
                cons_cache_by_dir2[dir] = null;
            }
            read_commands(f, cons_cache_by_dir2[dir], dir);
        }
    }

    Rentry entries[];
    Rfile[] tgts;
    auto errors = lookup_targets(opt_args, entries, tgts);
    if (errors > 0) {
        return 1;
    }

    if (opt_list_targets) {
        foreach(tgt; tgts) {
            writeln(tgt.path);
        }
        return 0;
    }
    if (opt_list_commands) {
        writeln("tgts ============ ", tgts);
        show_cmds();
        return 0;
    }
    if (opt_remove) {
        foreach (tgt; tgts) {
            if (tgt.file_exist_FORCED()) {
                os.rm_f(tgt.path());
                writeln("Removed ", tgt.path());
                tgt.st_invalid_error();
            }
        }
        foreach (entry; entries) {
            if (auto d = cast(Rdir)entry) {
                if (! entry.dirty) {
                    writeln("jcons: already removed: ", entry.path);
                }
            }
        }
        return 0;
    }

    auto engine = new Engine(tgts);
    engine.run();
    scope(exit) filesystem.terminate();

    if (Engine.got_sigint) {
        writeln("jcons: *** got interrupt, terminating");
        return 1;
    }

    string[] paths;
    foreach (fun; builder.s_active_funs.byKey()) {
        if (fun.m_caller !is null) {
            if (auto update_fun = cast(FUN_update_tgt) fun.m_caller) {
                paths ~= update_fun.tgt.path();
            }
        }
    }
    if (paths.length > 0) {
        sort(paths);
        writeln("jcons: error: circular dependency for '", paths.join(",") , "'");
        return 1;
    }

    foreach (entry; entries) {
        if (auto d = cast(Rdir)entry) {
            if (! entry.dirty) {
                writeln("jcons: up-to-date: ", entry.path);
            }
        }
    }

    return g_nerrors == 0 ? 0 : 1;
}


extern (C) nothrow @nogc @system void handle_sigint_signal(int sig) {
    Engine.got_sigint = true;
}

void setup_signals() {
    auto f = signal(SIGINT, &handle_sigint_signal);
}

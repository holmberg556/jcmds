//----------------------------------------------------------------------
// argumentparser.d
//----------------------------------------------------------------------
// Copyright 2015-2017 Johan Holmberg.
//----------------------------------------------------------------------
// This file is part of "jcmds".
//
// "jcmds" is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// "jcmds" is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with "jcmds".  If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------

import std.array;
import std.conv;
import core.stdc.stdlib;
import std.stdio;
import std.string;
import std.regex;

alias setflag_fun_t  = void delegate(bool flag);
alias setint_fun_t   = void delegate(int value);
alias callback_fun_t = void delegate();

interface Option {
    Option metavar(string name);
}

interface ArgumentParser {
    static ArgumentParser create(string program);

    Option add(string sname, string name,
               string help,
               setflag_fun_t fun);
    Option add(string sname, string name,
               string help,
               bool * value, bool invert = false);
    Option add(string sname, string name,
               string help,
               setint_fun_t fun);
    Option add(string sname, string name,
               string help,
               int * value, int default_value = 0);
    Option add(string sname, string name,
               string help,
               string * value);
    Option add(string sname, string name,
               string help,
               string[] * value);

    void addpos(string name,
                string help, int min, int max,
                string[] * value);

    void parse_args(string[] argv);
}


class OptionBase : Option {

    string sname;
    string name;
    string help;
    string meta;

    override Option metavar(string name) {
        meta = name;
        return this;
    }

    Option action(callback_fun_t /*callback*/) {
        return this;
    }

    string _arg_extra() {
        return (_with_arg() ? " " ~ _arg_name() : "");
    }

    string names() {
        string extra = (_with_arg() ? " " ~ _arg_name() : "");
        if (sname.length == 0) {
            return name ~ extra;
        }
        else {
            return sname ~ extra ~ ", " ~ name ~ extra;
        }
    }

    void init() {}
    bool _with_arg() { return false; }
    void _set(string value) {}

    string show() {
        if (sname.length != 0 && name.length != 0) {
            return sname ~ "/" ~ name;
        }
        else {
            return sname.length == 0 ? name : sname;
        }
    }

    string _arg_name() {
        if (meta.length != 0) return meta;

        string res = name;
        size_t j = 0;
        while (j<res.length && res[j] == '-') ++j;
        res = res[j..$];
        res = res.replace("-", "_");
        res = res.toUpper();
        return res;
    }

    string usage_text() {
        string res = (sname.length == 0 ? name : sname);
        res ~= _arg_extra();
        return "[" ~ res ~ "]";
    }
}

//----------------------------------------------------------------------

class BoolOption : OptionBase {
    bool * mPtr;
    bool invert;

    this(bool * ptr, bool invert) {
        this.mPtr = ptr;
        this.invert = invert;
    }

    override void init() { *mPtr = invert; }
    override bool _with_arg() { return false; }

    override void _set(string /*value*/) {
        *mPtr = !invert;
    }
}

class BoolFunOption : BoolOption {
    setflag_fun_t mFun;

    this(setflag_fun_t fun) {
        super(null, false);
        mFun = fun;
    }

    override void init() {}

    override void _set(string /*value*/) {
        mFun(true);
    }
}

class IntOption : OptionBase {
    int * mPtr;
    int default_value;

    this(int * ptr, int default_value) {
        this.mPtr = ptr;
        this.default_value = default_value;
    }

    override void init() { *mPtr = default_value; }
    override bool _with_arg() { return true; }

    override void _set(string value) {
        *mPtr = to!int(value);
    }
}

class IntFunOption : IntOption {
    setint_fun_t mFun;

    this(setint_fun_t fun) {
        super(null, 0);
        mFun = fun;
    }

    override void init() {}

    override void _set(string value) {
        mFun(to!int(value));
    }
}

class StringOption : OptionBase {
    string * mPtr;

    this(string * ptr) {
        mPtr = ptr;
    }

    override void init() { *mPtr = ""; }
    override bool _with_arg() { return true; }

    override void _set(string value) {
        *mPtr = value;
    }
}

class StringArrOption : OptionBase {
    string[] * mPtr;

    this(string[] * ptr) {
        mPtr = ptr;
    }

    override void init() { *mPtr = null; }
    override bool _with_arg() { return true; }

    override void _set(string value) {
        *mPtr ~= value;
    }
}


//----------------------------------------------------------------------

class ArgumentParserConcrete : ArgumentParser {
    string mProgram;
    OptionBase[] mOptions;
    bool mHelp;

    string mArgsName;
    string mArgsHelp;
    int mArgsMin;
    int mArgsMax;
    string[]* mArgsPtr;

    this(string program) {
        mProgram = program;
        add("-h", "--help", "show this help message and exit", &mHelp);
    }

    override Option add(string sname, string name, string help, setflag_fun_t fun) {
        OptionBase option = new BoolFunOption(fun);
        option.sname      = sname;
        option.name       = name;
        option.help       = help;
        mOptions ~= option;

        return option;
    }

    override Option add(string sname, string name, string help, bool * value, bool invert = false) {
        OptionBase option = new BoolOption(value, invert);
        option.sname      = sname;
        option.name       = name;
        option.help       = help;
        mOptions ~= option;

        return option;
    }

    override Option add(string sname, string name, string help, setint_fun_t fun) {
        OptionBase option = new IntFunOption(fun);
        option.sname      = sname;
        option.name       = name;
        option.help       = help;
        mOptions ~= option;

        return option;
    }

    override Option add(string sname, string name, string help, int * value, int default_value = 0) {
        OptionBase option = new IntOption(value, default_value);
        option.sname      = sname;
        option.name       = name;
        option.help       = help;
        mOptions ~= option;

        return option;
    }

    override Option add(string sname, string name, string help, string * value) {
        OptionBase option = new StringOption(value);
        option.sname      = sname;
        option.name       = name;
        option.help       = help;
        mOptions ~= option;

        return option;
    }

    override Option add(string sname, string name, string help, string[] * value) {
        OptionBase option = new StringArrOption(value);
        option.sname      = sname;
        option.name       = name;
        option.help       = help;
        mOptions ~= option;

        return option;
    }

    override void addpos(string name, string help, int min, int max, string[] * value) {
        mArgsName = name;
        mArgsHelp = help;
        mArgsMin = min;
        mArgsMax = max;
        mArgsPtr = value;
    }

    void print_usage_synopsis(ref File cxxx) {
        string leading = "usage: " ~ mProgram;
        cxxx.write(leading);
        size_t off = leading.length;

        foreach (option; mOptions) {
            string str = option.usage_text();
            if (off + 1 + str.length > 79) {
                cxxx.writeln();
                cxxx.write(" ".replicate(leading.length));
                off = leading.length;
            }
            cxxx.write(" ", str);
            off += 1 + str.length;
        }
        cxxx.writeln();

        if (mArgsName.length > 0) {
            cxxx.write(" ".replicate(leading.length));
            cxxx.write(" [", mArgsName, " [", mArgsName, " ...]]");
            cxxx.writeln();
        }
    }

    void print_usage() {
        print_usage_synopsis(stdout);

        if (mArgsName.length > 0) {
            writeln();
            writeln("positional arguments:");
            writeln("  ", mArgsName, "           ", mArgsHelp);
            writeln();
        }
        writeln("optional arguments:");
        writeln("  -h, --help            show this help message and exit");
        foreach (option; mOptions) {
            string str = "  " ~ option.names();
            size_t wanted = 22;
            if (str.length < wanted) {
                str ~= " ".replicate(wanted - str.length);
            }
            if (str.length > wanted) {
                writeln(str);
                writeln(" ".replicate(wanted), "  ", option.help);
            }
            else {
                writeln(str, "  ", option.help);
            }
        }
    }

    string[] mArgv;
    size_t mOptind;

    override void parse_args(string[] argv) {
        mArgv = argv[1..$];

        // initialize options
        foreach (option; mOptions) {
            option.init();
        }

        mOptind = 0;
        while (_more_args()) {
            if (_arg() == "-h") {
                print_usage();
                exit(0);
            }
            if (_arg() == "") break;
            if (_arg()[0] != '-') break;
            if (_arg() == "-") break;
            if (_arg() == "--") break;

            // -f
            if (auto m = matchFirst(_arg(), `^-\w$`)) {
                OptionBase option = _find_option(m[0]);
                if (option._with_arg()) {
                    if (! _more_args(1)) {
                        print_usage_synopsis(stderr);
                        stderr.writeln(mProgram, ": error: argument ",
                                       option.show(), ": expected one argument");
                        exit(2);
                    }
                    option._set(_arg(1));
                    mOptind += 2;
                }
                else {
                    option._set("");
                    mOptind += 1;
                }
            }

            // -f...
            else if (auto m = matchFirst(_arg(), `^(-\w)(.+)`)) {
                OptionBase option = _find_option(m[1]);
                if (option._with_arg()) {
                    option._set(m[2]);
                    mOptind += 1;
                }
                else {
                    option._set("");
                    _arg(0) = "-" ~ m[2];
                }
            }

            // --foo
            else if (auto m = matchFirst(_arg(), `^--\w[-_\w_]*`)) {
                OptionBase option = _find_option(m[0]);
                if (option._with_arg()) {
                    option._set(_arg(1));
                    mOptind += 2;
                }
                else {
                    option._set("");
                    mOptind += 1;
                }
            }

            // --foo=...
            else if (auto m = matchFirst(_arg(), `^--\w[-_\w]*)=(.*)`)) {
                OptionBase option = _find_option(m[1]);
                if (option._with_arg()) {
                    option._set(m[2]);
                    mOptind += 1;
                }
                else {
                    print_usage_synopsis(stderr);
                    stderr.writeln(mProgram, ": error: argument: ",
                                   option.show(), ": ignored explicit argument '",
                                   m[2], "'");
                    exit(2);
                }
            }

            else {
                stderr.writeln("INTERNAL ERROR: arg = ", _arg());
                throw new Exception("unexpected else ...");
            }
        }
        if (mArgsName.length > 0) {
            *mArgsPtr = mArgv[mOptind..$];
        }
        else if (mOptind < mArgv.length) {
            throw new Exception("too many options");
        }
    }

    OptionBase  _find_option(string name) {
        foreach (option; mOptions) {
            if (option.name == name) return option;
            if (option.sname == name) return option;
        }
        print_usage_synopsis(stderr);
        stderr.writeln(mProgram, ": error: unrecognized arguments: ", name);
        exit(2);
        assert(0);
    }

    ref string _arg(int i=0) {
        if (mOptind + i < mArgv.length) {
            return mArgv[mOptind + i];
        }
        else {
            throw new Exception("index out of range");
        }
    }

    bool _more_args(int i=0) {
        return mOptind + i < mArgv.length;
    }

}

ArgumentParser ArgumentParser_create(string name) {
    return new ArgumentParserConcrete(name);
}

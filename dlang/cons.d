//----------------------------------------------------------------------
// cons.d
//----------------------------------------------------------------------
// Copyright 2008-2017 Johan Holmberg.
//----------------------------------------------------------------------
// This file is part of "jcmds".
//
// "jcmds" is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// "jcmds" is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with "jcmds".  If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------

import std.algorithm;
import std.conv;
import std.process;
import std.stdio;
import std.string;
import core.stdc.stdlib;

import cache;
import filesystem;
import cmd;
import global;
import md5calc;

//----------------------------------------

Rdir s_conscript_dir = null;

void initialize() {
    filesystem.initialize();
    s_conscript_dir = filesystem.cwd;
}

//----------------------------------------

static Rdir[] conscript_dir_stack;

void push_dir(string path) {
    conscript_dir_stack ~=  s_conscript_dir;
    Rdir d  = s_conscript_dir.find_dir(path);
    s_conscript_dir  = d;
}

void pop_dir() {
    s_conscript_dir = conscript_dir_stack[$-1];
    conscript_dir_stack = conscript_dir_stack[0..$-1];
}

//----------------------------------------

class Env {
    string expand(string str) {
        return str;
    }
}

class LibPathMap
{
    Rdir[] m_libpath_dirs;
    string[] m_libs;

    Rfile[] m_fs_libs;
    bool m_fs_libs_set;

    this(Rdir[] libpath_dirs, string[] libs) {}

    static LibPathMap find(Rdir[] libpath_dirs, string[] libs) {
        return null;
    }

    Rfile[] fs_libs() {
        return m_fs_libs;
    }
}

//======================================================================

class ProgramPath {
    Rdir[] m_dir_arr;
    Rfile[string] m_file_by_cmdname;

    static ProgramPath[string] s_obj_by_path;

    this(string program_path) {
        foreach (dir; program_path.split(':')) {
            Rdir d = s_curr_dir.lookup_or_fs_dir(dir);
            if (d !is null) {
                m_dir_arr ~=  d;
            }
        }
    }

    void teardown() {
        s_obj_by_path = null;
    }

    static ProgramPath find(string path) {
        auto p = path in s_obj_by_path;
        if (p is null) {
            auto res = s_obj_by_path[path] = new ProgramPath(path);
            return res;
        }
        else {
            return *p;
        }
    }


    Rfile find_program(string cmdname) {
        auto p = cmdname in m_file_by_cmdname;
        if (p is null) {
            if (! cmdname.find('/')) {
                // simple name
                foreach (dir; m_dir_arr) {
                    Rfile f = dir.lookup_or_fs_file(cmdname);
                    if (f !is null) {
                        m_file_by_cmdname[cmdname] = f;
                        return f;
                    }
                }
            }
            else {
                // relative path
                Rfile f = s_curr_dir.lookup_or_fs_file(cmdname);
                if (f !is null) {
                    m_file_by_cmdname[cmdname] = f;
                    return f;
                }
            }
            m_file_by_cmdname[cmdname] = null;
            return null;
        }
        else {
            return *p;
        }
    }

}


//======================================================================

class CppPathMap {

    static CppPathMap[string] s_arr_to_obj;

    Rdir[] m_cpp_path_unique;
    Rdir[] m_cpp_path;
    string m_as_str;

    Rfile[string] m_file_to_rfile;
    Rdigest[Rfile] m_file_to_result;

    Rfile[Include][Rdir] bydir;

    int m_count;
    static int s_count;

    this(Rdir[] cpp_path) {
        m_count = s_count++;
        m_cpp_path = cpp_path;

        m_as_str = "";
        foreach (it; m_cpp_path) {
            m_as_str ~= ":";
            m_as_str ~= it.path();
        }

        bool[Rdir] seen;
        foreach (d; m_cpp_path) {
            auto p = d in seen;
            if (p is null) {
                m_cpp_path_unique ~= d;
                seen[d] = true;
            }
        }
    }

    void toString(scope void delegate(const(char)[]) sink) {
        sink("CppPathMap[");
        sink(to!string(m_cpp_path));
        sink(",");
        sink(to!string(m_count));
        sink("]");
    }

    //------------------------------

    static void teardown() {
        s_arr_to_obj = null;
    }

    //------------------------------

    static CppPathMap find(Rdir[] cpp_path) {
        string idx = join(map!(x => x.path())(cpp_path), " ");
        auto p = (idx in s_arr_to_obj);
        if (p !is null) {
            return *p;
        }
        else {
            CppPathMap obj = new CppPathMap(cpp_path);
            s_arr_to_obj[idx] = obj;
            return obj;
        }
    }

    //------------------------------

    Rdir[] cpp_path_dirs() {
        return m_cpp_path;
    }

    //------------------------------

    Rfile[] find_node_includes(Rfile node) {
        Rdir d = node.parent();
        auto p1 = d in bydir;
        Rfile[Include] bydir1;
        if (p1 is null) {
            Include x; bydir1[x] = null; bydir1.remove(x); // empty non-null 'bydir1'
            bydir[d] = bydir1;
        }
        else {
            bydir1 = *p1;
        }
        Rfile[] incs;
        foreach (include; node.db_includes()) {
            Rfile inc;
            auto p = include in bydir1;
            if (p is null) {
                if (include.quotes) {
                    inc = d.lookup_or_fs_file(include.file);
                }
                if (inc is null) {
                    inc = find_include(include.file);
                }
                bydir1[include] = inc;
            }
            else {
                inc = *p;
            }
            if (inc !is null && inc != node) {
                debug (logging) {
                    writeln("INCFOUND: ", node.path(), " - ", inc.path());
                }
                incs ~= inc;
            }
        }
        return incs;
    }

    Rfile find_include(string file) {
        {
            // try to find cached value
            auto p = (file in m_file_to_rfile);
            if (p !is null) {
                Rfile f = *p;
                return f;
            }
        }
        {
            // calculate new value and cache it
            Rfile f = null;
            foreach (d; m_cpp_path_unique) {
                f = d.lookup_or_fs_file(file);
                if (f !is null) break;
            }
            m_file_to_rfile[file] = f;
            return f;
        }
    }

    //------------------------------

    void put_result(Rfile file, in Rdigest result) {
        m_file_to_result[file] = result;
    }

    Rdigest get_result(Rfile file) {
        auto p = (file in m_file_to_result);
        if (p is null) {
            writeln("INTERNAL ERROR - node_calc_md5", file.path());
            exit(1);
        }

        return *p;
    }

    bool has_result(Rfile file, ref Rdigest result) {
        auto p = (file in m_file_to_result);
        if (p is null) {
            return  false;
        }
        else {
            result = *p;
            return true;
        }
    }

    bool has_result(Rfile file) {
        auto p = (file in m_file_to_result);
        return (p !is null);
    }
}




//======================================================================

class Cons {

    Env m_env;
    Rdir m_conscript_dir;

    bool m_obj_ext_set;
    string m_obj_ext;

    bool m_exe_ext_set;
    string m_exe_ext;

    bool m_lib_ext_set;
    string m_lib_ext;

    string m_libs_arg_string;
    string[] m_libs_arg_array;

    string[] m_libs;
    bool m_libs_set;

    string[] m_libpath;
    bool m_libpath_set;

    Rdir[] m_libpath_dirs;
    bool m_libpath_dirs_set;

    LibPathMap m_libpath_map;

    Rdir[] m_cpp_path_arg;
    CppPathMap m_cpp_path;
    Cache m_build_cache;

    //----------------------------------------------------------------------

    this() {
        m_env = new Env();

        //     Settings::init_once();

        //     m_conscript_dir = s_conscript_dir;
        //     m_env = new VarEnv();

        //     m_cpp_path = null;
        //     m_build_cache = null;
        //     m_obj_ext_set = false;
        //     m_exe_ext_set = false;
        //     m_lib_ext_set = false;

        m_libs_set = false;
        m_libpath_set = false;
        m_libpath_dirs_set = false;
        m_libpath_map = null;

        //     if (Ros::is_windows() || Ros::fake_windows()) {
        //       Settings::apply("windows", m_env);
        //     }
        //     else {
        //       Settings::apply("linux", m_env);
        //     }

        //     if (Rglobal::scons) {
        //       // rcons-python
        //       _scons_defaults(m_env);
        //       m_env.set( "CC_CMD",     "$CCCOM" );
        //       m_env.set( "CXX_CMD",    "$CXXCOM" );

        //       m_env.set( "SOURCES",    "%INPUT" );
        //       m_env.set( "TARGET",     "%OUTPUT" );

        //     }

        //     s_all_rcons.push_back(this);
    }

    //----------------------------------------------------------------------

    ProgramPath m_program_path;

    ProgramPath program_path() {
        if (m_program_path is null) {
            string path = environment.get("PATH");
            if (path is null) {
                writeln("ERROR: PATH not set");
                exit(1);
            }
            m_program_path = ProgramPath.find(path);
        }
        return m_program_path;
    }

    //----------------------------------------------------------------------

    string _expand(string str) {
        return m_env.expand(str);
    }

    //----------------------------------------------------------------------

//   Rcons::Rcons(Rcons & obj)
//     : RconsBase()
//   {
//     m_obj_ext_set = false;
//     m_exe_ext_set = false;
//     m_lib_ext_set = false;

//     m_libs_set = false;
//     m_libpath_set = false;
//     m_libpath_dirs_set = false;
//     m_libpath_map = null;

//     m_cpp_path = obj.m_cpp_path;
//     m_cpp_path_arg = obj.m_cpp_path_arg;

//     m_build_cache = obj.m_build_cache;
//     m_env = new VarEnv(*obj.m_env);

//     m_conscript_dir = obj.m_conscript_dir;

//     s_all_rcons.push_back(this);
//   }

//   //----------------------------------------------------------------------

//   Rcons::~Rcons()
//   {
//     delete m_env;
//     delete m_libpath_map;       // do we own this here ???
//   }

//   //----------------------------------------------------------------------

//   void Rcons::teardown()
//   {
//     for (auto & it : s_all_rcons) {
//       delete (it);
//     }
//     s_all_rcons.clear();
//     Settings::teardown();
//   }

//   //----------------------------------------------------------------------

//   Rcons * Rcons::clone()
//   {
//     return new Rcons(*this);
//   }

//   //----------------------------------------------------------------------

//   void Rcons::get_env_doc_strings(string_map_t & doc_strings)
//   {
//     Settings::get_env_doc_strings(doc_strings);
//   }

    //----------------------------------------------------------------------

    CppPathMap cpp_path() {
        if (m_cpp_path is null) {
            m_cpp_path = CppPathMap.find(m_cpp_path_arg);
        }
        return m_cpp_path;
    }

    //----------------------------------------------------------------------

    void set_cpp_path(string[] dirs) {
        m_cpp_path_arg = null;
        foreach (dir; dirs) {
            Rdir d = s_conscript_dir.find_dir(dir);
            if (d.tgtdir !is null) {
                m_cpp_path_arg ~= d.tgtdir;
            }
            m_cpp_path_arg ~= d;
        }
        //this.env_update_cpp_path();
    }

  //----------------------------------------------------------------------

//   void Rcons::env_update_cpp_path()
//   {
//     string _cpp_inc_opts_str;
//     for (auto & it : m_cpp_path_arg) {
//       if (! _cpp_inc_opts_str.empty()) _cpp_inc_opts_str += " ";
//       if (Rglobal::scons)
//         _cpp_inc_opts_str += "-I";
//       else
//         _cpp_inc_opts_str += "-I ";

//       _cpp_inc_opts_str += it.path();
//     }
//     m_env.set("_CPP_INC_OPTS", _cpp_inc_opts_str);
//   }

//   //------------------------------

//   string Rcons::_calc_libname(string name)
//   {
//     return _maybe_add_ext("lib" + name, lib_ext());
//   }

//   //----------------------------------------------------------------------

//   void Rcons::set_libs(string libs)
//   {
//     m_libs_arg_string = libs;
//   }

//   //----------------------------------------------------------------------

//   void Rcons::set_libs(string_arr_t & libs)
//   {
//     m_libs_arg_array = libs;
//   }

//   //----------------------------------------------------------------------

//   void Rcons::_calc_libs(string_arr_t & libs_arr)
//   {
//     string lib_opts;
//     m_libs.clear();

//     // TODO: delay this update, and take care of Windows syntax too
//     for (auto & it : libs_arr) {
//       if (! lib_opts.empty()) lib_opts += " ";
//       if (it.size() > 2 && it.substr(0,2) == "-l") {
//         // -lFOO
//         lib_opts += it;
//         m_libs.push_back( _calc_libname(it.substr(2)) );
//       }
//       else {
//         // FOO
//         lib_opts += "-l" + it;
//         m_libs.push_back( _calc_libname(it) );
//       }
//     }
//     m_env.set("_LIB_OPTS", lib_opts);
//   }

    //----------------------------------------------------------------------

    string[] libs() {
        if (! m_libs_set) {
            m_libs_set = true;

            if (m_libs_arg_array.length > 0) {
                string[] expanded_libs_arr;
                foreach (it; m_libs_arg_array) {
                    expanded_libs_arr ~= _expand(it);
                }
                // TODO: _calc_libs(expanded_libs_arr);
            }
            else if (m_libs_arg_string.length > 0) {
                string[] expanded_libs_arr;
                //TODO: split_on_spaces(_expand(m_libs_arg_string), expanded_libs_arr);

                //TODO: _calc_libs(expanded_libs_arr);
            }
        }
        return m_libs;
    }

//   //----------------------------------------------------------------------

//   void Rcons::set_libpath(string_arr_t & libpath)
//   {
//     m_libpath = libpath;
//     m_libpath_set = true;
//     this.env_update_libpath_opts();
//   }

//   //----------------------------------------------------------------------
//   // TODO: delay this update, and take care of Windows syntax too

//   void Rcons::env_update_libpath_opts()
//   {
//     string _libpath_opts_str;
//     for (auto & it : m_libpath) {
//       if (! _libpath_opts_str.empty()) _libpath_opts_str += " ";

//       _libpath_opts_str += "-L";
//       _libpath_opts_str += it;
//     }
//     m_env.set("_LIBPATH_OPTS", _libpath_opts_str);
//   }

    //----------------------------------------------------------------------

    Rdir[] libpath_dirs() {
        if (! m_libpath_dirs_set) {
            m_libpath_dirs = [];
            foreach (it; m_libpath) {
                Rdir d = m_conscript_dir.lookup_or_fs_dir(it);
                if (d !is null) {
                    m_libpath_dirs ~= d;
                }
            }
            m_libpath_dirs_set = true;
        }
        return m_libpath_dirs;
    }

    //----------------------------------------------------------------------

    LibPathMap libpath_map() {
        if (m_libpath_map is null) {
            m_libpath_map = LibPathMap.find(libpath_dirs(), libs());
        }
        return m_libpath_map;
    }

    //----------------------------------------------------------------------

    void connect_tgt(Rfile tgt, Cmd cmd) {
        Cmd existing_cmd = tgt.cmd;
        if (existing_cmd !is null) {
            writeln("jcons: error: same target twice: ",
                    "'", tgt.path(), "'");
            exit(1);
        }

        cmd.tgts ~= tgt;
        tgt.cmd = cmd;
        if (tgt.extra_deps !is null) {
            if (cmd.extra_deps !is null) {
                // merge sets
                cmd.extra_deps ~= tgt.extra_deps;
                tgt.extra_deps = null;
            }
            else {
                // move set
                cmd.extra_deps = tgt.extra_deps;
                tgt.extra_deps = null;
            }
        }
    }

    //----------------------------------------------------------------------

    void connect_src(Rfile src, Cmd cmd) {
        cmd.srcs ~= src;
    }

  //----------------------------------------------------------------------

    Cmd Command(string tgt, string src, string command, string digest_command, bool cpp_scanner = false) {
        Rfile t = s_conscript_dir.find_file(_expand(tgt));
        Rfile s = s_conscript_dir.find_file(_expand(src));
        Cmd cmd = new Cmd(this);
        connect_src(s, cmd);
        connect_tgt(t, cmd);
        cmd.m_digest_cmdline = digest_command;
        cmd.m_cmdline = command;
        cmd.uses_cpp = cpp_scanner;
        return cmd;
    }

  //----------------------------------------------------------------------

    Cmd Command(string tgt, string src, string command, bool cpp_scanner = false) {
        Rfile t = s_conscript_dir.find_file(_expand(tgt));
        Rfile s = s_conscript_dir.find_file(_expand(src));
        Cmd cmd = new Cmd(this);
        connect_src(s, cmd);
        connect_tgt(t, cmd);
        cmd.m_cmdline = command;
        cmd.uses_cpp = cpp_scanner;
        return cmd;
    }

    //----------------------------------------------------------------------

    Cmd Command(string[] tgts, string[] srcs, string command, bool cpp_scanner = false) {
        Cmd cmd = new Cmd(this);
        foreach (tgt; tgts) {
            Rfile t = s_conscript_dir.find_file(_expand(tgt));
            connect_tgt(t, cmd);
        }
        foreach (src; srcs) {
            Rfile s = s_conscript_dir.find_file(_expand(src));
            connect_src(s, cmd);
        }
        cmd.m_cmdline = command;
        cmd.uses_cpp = cpp_scanner;
        return cmd;
    }

    //----------------------------------------------------------------------

    Cmd Command(string tgt, string[] srcs, string cmd) {
        string[] tgts;
        tgts ~= tgt;
        return Command(tgts, srcs, cmd);
    }

    //----------------------------------------------------------------------

    Cmd Command(string[] tgts, string src, string cmd) {
        string[] srcs;
        srcs ~= src;
        return Command(tgts, srcs, cmd);
    }

//   //----------------------------------------------------------------------

//   bool
//   is_obj_ext(string file)
//   {
//     string ext = Ros::file_ext(file);
//     return (ext == obj_ext());
//   }

//   //----------------------------------------------------------------------

//   bool
//   is_exe_ext(string file)
//   {
//     string ext = Ros::file_ext(file);
//     return (ext == exe_ext());
//   }

//   //----------------------------------------------------------------------

//   bool
//   is_lib_ext(string file)
//   {
//     string ext = Ros::file_ext(file);
//     return (ext == lib_ext());
//   }

//   //----------------------------------------------------------------------

//   string
//   lang_by_ext(string file)
//   {
//     string ext = Ros::file_ext(file);
//     if (ext == ".c") {
//       return "C";
//     }
//     else if (ext == ".cpp") {
//       return "C++";
//     }
//     else if (ext == ".cc") {
//       return "C++";
//     }
//     else if (ext == ".cxx") {
//       return "C++";
//     }
//     else {
//       cout << "Error: unknown extension for: '" << file << "'" << endl;
//       exit(1);
//     }
//   }

//   //----------------------------------------------------------------------

//   string
//   compile_symbol_by_lang(string lang)
//   {
//     if (lang == "C") {
//       return "%CC_CMD";
//     }
//     else if (lang == "C++") {
//       return "%CXX_CMD";
//     }
//     else {
//       cout << "Error: unknown language: " << lang << endl;
//       exit(1);
//     }
//   }

//   //----------------------------------------------------------------------

//   string
//   link_symbol_by_lang(string lang)
//   {
//     if (lang == "C") {
//       return "%CC_LINK";
//     }
//     else if (lang == "C++") {
//       return "%CXX_LINK";
//     }
//     else {
//       cout << "Error: unknown language: " << lang << endl;
//       exit(1);
//     }
//   }

//   //----------------------------------------------------------------------
//   // Used in rcons.

//   void
//   Objects(string_arr_t & objs, string_arr_t & srcs)
//   {
//     for (auto & it : srcs) {
//       string obj = Object(it);
//       objs.push_back(obj);
//     }
//   }

//   //----------------------------------------------------------------------
//   // Used in rcons.

//   void
//   Objects(string_arr_t & objs, rcons_arr_t & rconses, string_arr_t & srcs)
//   {
//     rcons_arr_t::iterator rcons_it = rconses.begin();
//     for (auto & it : srcs) {
//       string obj = (*rcons_it).Object(it);
//       objs.push_back(obj);
//       ++rcons_it;
//     }
//   }

//   //----------------------------------------------------------------------
//   // Used in rcons.

//   string
//   Object(string obj,
//                 string src)
//   {
//     PROBE3("Object", obj, src);
//     Rfile t = s_conscript_dir.find_file(_expand(obj));
//     Rfile s = s_conscript_dir.find_file(_expand(src));

//     string x_cmd = compile_symbol_by_lang(lang_by_ext(src));

//     Cmd existing_cmd = t.cmd;
//     if (existing_cmd !is null) {
//       if (x_cmd  == existing_cmd.m_cmdline &&
//           this == existing_cmd.m_cons &&
//           existing_cmd.uses_cpp)
//       {
//         // same command again ==> OK, and nothing new to do
//         return t.path();
//       }
//       else {
//         cout << "jcons: error: same target twice, different command: " <<
//           "'" << t.path() << "'" << endl;
//         exit(1);
//       }
//     }

//     Cmd cmd = new Cmd(this);
//     connect_src(s,cmd);
//     connect_tgt(t,cmd);
//     cmd.uses_cpp = true;

//     cmd.m_cmdline = x_cmd;
//     return t.path();
//   }

//   //----------------------------------------------------------------------
//   // Replace ".." with "__" in paths.

//   void
//   _replace_dot_dot(string & path)
//   {
//     size_t off = 0;
//     for (;;) {
//       size_t i = path.find("..", off);
//         if (i == string::npos) break;
//         if ((i == 0 || path[i-1] == '/') &&
//             (i == path.size() - 2 || path[i+2] == '/'))
//         {
//           path.replace(i, 2, "__");
//         }
//         off = i + 2;
//       }
//   }

//   //----------------------------------------------------------------------

//   string
//   _prepend_build_top(string path)
//   {
//     string mapped_path = path;

//     string build_top;
//     m_env.get("BUILD_TOP", build_top);
//     if (build_top != "") {
//       _replace_dot_dot(mapped_path);
//       mapped_path = build_top + "/" + Rcons::s_conscript_dir.path() + "/" + mapped_path;
//     }

//     string build_subdir;
//     m_env.get("BUILD_SUBDIR", build_subdir);
//     if (build_subdir != "") {
//       size_t slash_i = mapped_path.rfind('/');
//       if (slash_i == string::npos) {
//         mapped_path.insert(0, build_subdir + "/");
//       }
//       else {
//         mapped_path.insert(slash_i + 1, build_subdir + "/");
//       }
//     }

//     string build_suffix;
//     m_env.get("BUILD_SUFFIX", build_suffix);
//     if (build_suffix != "") {
//       size_t slash_i = mapped_path.rfind('/');
//       size_t dot_i   = mapped_path.rfind('.');
//       if (dot_i == string::npos) {
//         mapped_path += "-" + build_suffix;
//       }
//       else if (slash_i == string::npos || dot_i > slash_i) {
//         mapped_path.insert(dot_i, "-" + build_suffix);
//       }
//       else {
//         mapped_path += "-" + build_suffix;
//       }
//     }

//     return mapped_path;
//   }

//   //----------------------------------------------------------------------

//   string
//   _get_mapped_exe(string path)
//   {
//     string exe_dir;
//     m_env.get("EXE_DIR", exe_dir);
//     if (exe_dir != "") {
//       string mapped_path = exe_dir + "/" + path;
//       return mapped_path;
//     }

//     return _prepend_build_top(path);
//   }

//   //----------------------------------------------------------------------

//   string
//   _get_mapped_lib(string path)
//   {
//     string lib_dir;
//     m_env.get("LIB_DIR", lib_dir);
//     if (lib_dir != "") {
//       string mapped_path = lib_dir + "/" + path;
//       return mapped_path;
//     }

//     return _prepend_build_top(path);
//   }

//   //----------------------------------------------------------------------

//   string
//   _get_mapped_obj(string path)
//   {
//     return _prepend_build_top(path);
//   }

//   //----------------------------------------------------------------------

//   string
//   _maybe_add_ext(string path, string ext)
//   {
//     if (Ros::file_ext(path) == "") {
//       return path + ext;
//     }
//     else {
//       return path;
//     }
//   }

//   //----------------------------------------------------------------------

//   string obj_ext()
//   {
//     if (! m_obj_ext_set) {
//       m_obj_ext_set = true;
//       m_env.get("OBJ_EXT", m_obj_ext, true);
//     }
//     return m_obj_ext;
//   }

//   //----------------------------------------------------------------------

//   string exe_ext()
//   {
//     if (! m_exe_ext_set) {
//       m_exe_ext_set = true;
//       m_env.get("EXE_EXT", m_exe_ext, true);
//     }
//     return m_exe_ext;
//   }

//   //----------------------------------------------------------------------

//   string lib_ext()
//   {
//     if (! m_lib_ext_set) {
//       m_lib_ext_set = true;
//       m_env.get("LIB_EXT", m_lib_ext, true);
//     }
//     return m_lib_ext;
//   }

//   //----------------------------------------------------------------------

//   string
//   Object(string src)
//   {
//     string obj = Ros::file_remove_ext(_expand(src)) + obj_ext();
//     string mapped_obj = _get_mapped_obj(obj);

//     string returned_obj = Object(mapped_obj, src);
//     return mapped_obj;
//   }

//   //----------------------------------------------------------------------

//   void _update_lang(string & link_lang, string lang)
//   {
//     if (link_lang == "") {
//       link_lang = lang;
//     }
//     else if (link_lang == "C" && lang == "C++") {
//       link_lang = lang;
//     }
//     else {
//       // already set
//     }
//   }

//   //----------------------------------------------------------------------

//   void
//   Program(string prog,
//                  string_arr_t & srcs)
//   {
//     string link_lang = "";
//     string_arr_t objs;
//     for (auto & it : srcs) {
//       string expanded_it = _expand(it);
//       if (is_obj_ext(expanded_it)) {
//         objs.push_back(expanded_it);
//       }
//       else if (is_lib_ext(expanded_it)) {
//         objs.push_back(expanded_it);
//       }
//       else {
//         string obj = Object(expanded_it);
//         objs.push_back(obj);
//         _update_lang(link_lang, lang_by_ext(expanded_it));
//       }
//     }
//     if (link_lang == "") link_lang = "C++"; // best guess

// #if 1
//     Cmd link_cmd = new Cmd(this);
//     for (auto & it : objs) {
//       Rfile o = s_conscript_dir.find_file(it);
//       connect_src(o,link_cmd);
//     }

//     string mapped_prog = _get_mapped_exe(_maybe_add_ext(prog, exe_ext()));

//     Rfile t = s_conscript_dir.find_file(mapped_prog);
//     connect_tgt(t,link_cmd);

//     string x_cmd = link_symbol_by_lang(link_lang);
//     link_cmd.m_cmdline = x_cmd;
// #endif
//   }

//   //----------------------------------------------------------------------
//   // Used in rcons.

//   void
//   Program(string prog,
//                  rcons_arr_t & rconses, string_arr_t & srcs)
//   {
//     string link_lang = "";
//     string_arr_t objs;
//     rcons_arr_t::iterator rcons_it = rconses.begin();
//     for (auto & it : srcs) {
//       Rcons * rcons = dynamic_cast<Rcons*>(*rcons_it);
//       string expanded_it = rcons._expand(it);
//       if (is_obj_ext(expanded_it)) {
//         objs.push_back(expanded_it);
//       }
//       else if (is_lib_ext(expanded_it)) {
//         objs.push_back(_get_mapped_lib(expanded_it));
//       }
//       else {
//         string obj = (*rcons_it).Object(expanded_it);
//         objs.push_back(obj);
//         _update_lang(link_lang, lang_by_ext(expanded_it));
//       }
//       ++rcons_it;
//     }
//     if (link_lang == "") link_lang = "C++"; // best guess

// #if 1
//     Cmd link_cmd = new Cmd(this);
//     for (auto & it : objs) {
//       Rfile o = s_conscript_dir.find_file(it);
//       connect_src(o,link_cmd);
//     }

//     string mapped_prog = _get_mapped_exe(_maybe_add_ext(_expand(prog), exe_ext()));

//     Rfile t = s_conscript_dir.find_file(mapped_prog);
//     connect_tgt(t,link_cmd);

//     string x_cmd = link_symbol_by_lang(link_lang);
//     link_cmd.m_cmdline = x_cmd;
//     link_cmd.m_uses_libpath = true;
// #endif
//   }

//   //----------------------------------------------------------------------

//   void
//   StaticLibrary(string & mapped_lib,
//                        string lib,
//                        string_arr_t & srcs)
//   {
//     string_arr_t objs;
//     for (auto & it : srcs) {
//       string expanded_src = _expand(it);
//       if (is_obj_ext(expanded_src)) {
//         objs.push_back(expanded_src);
//       }
//       else {
//         string obj = Object(expanded_src);
//         objs.push_back(obj);
//       }
//     }

//     Cmd lib_cmd = new Cmd(this);
//     for (auto & it : objs) {
//       Rfile o = s_conscript_dir.find_file(it);
//       connect_src(o,lib_cmd);
//     }

//     mapped_lib = _get_mapped_lib(_maybe_add_ext(_expand(lib), lib_ext()));

//     Rfile t = s_conscript_dir.find_file(mapped_lib);
//     connect_tgt(t, lib_cmd);

//     lib_cmd.m_cmdline = "%AR_CMD";
//   }

//   //----------------------------------------------------------------------
//   // Used in rcons.

//   void
//   StaticLibrary(string & mapped_lib,
//                        string lib,
//                        rcons_arr_t & rconses, string_arr_t & srcs)
//   {
//     string_arr_t objs;
//     rcons_arr_t::iterator rcons_it = rconses.begin();
//     for (auto & it : srcs) {
//       Rcons * rcons = dynamic_cast<Rcons*>(*rcons_it);
//       string expanded_it = rcons._expand(it);
//       if (is_obj_ext(expanded_it)) {
//         objs.push_back(expanded_it);
//       }
//       else {
//         string obj = (*rcons_it).Object(expanded_it);
//         objs.push_back(obj);
//       }
//       ++rcons_it;
//     }

//     Cmd lib_cmd = new Cmd(this);
//     for (auto & it : objs) {
//       Rfile o = s_conscript_dir.find_file(it);
//       connect_src(o,lib_cmd);
//     }

//     mapped_lib = _get_mapped_lib(_maybe_add_ext(_expand(lib), lib_ext()));

//     Rfile t = s_conscript_dir.find_file(mapped_lib);
//     connect_tgt(t, lib_cmd);

//     lib_cmd.m_cmdline = "%AR_CMD";
//   }

  //----------------------------------------------------------------------

  void Depends(string tgt, string src) {
      Rfile t = s_conscript_dir.find_file(_expand(tgt));
      Rfile s = s_conscript_dir.find_file(_expand(src));

      if (t.cmd !is null) {
          t.cmd.extra_deps ~= s;
      }
      else {
          t.extra_deps ~= s;
      }
  }

  //----------------------------------------------------------------------

    void ExeDepends(string tgt, string src) {
        Rfile t = s_conscript_dir.find_file(_expand(tgt));
        Rfile s = s_conscript_dir.find_file(_expand(src));
        t.m_exe_deps ~= s;
    }

//   //----------------------------------------------------------------------

//   void
//   Install(string tgtdir,
//                  string src)
//   {
//     Rfile s = s_conscript_dir.find_file(_expand(src));
//     Rdir * td = s_conscript_dir.find_dir(_expand(tgtdir));
//     Rfile t = td.find_file(s.name());

//     Cmd cmd = new Cmd(this);
//     connect_src(s,cmd);
//     connect_tgt(t,cmd);
//     cmd.m_cmdline = "cp %INPUT %OUTPUT";
//   }

//   //----------------------------------------------------------------------

//   void
//   InstallAs(string tgt,
//                    string src)
//   {
//     Rfile s = s_conscript_dir.find_file(_expand(src));
//     Rfile t = s_conscript_dir.find_file(_expand(tgt));

//     Cmd cmd = new Cmd(this);
//     connect_src(s,cmd);
//     connect_tgt(t,cmd);
//     cmd.m_cmdline = "cp %INPUT %OUTPUT";
//   }

//   //----------------------------------------------------------------------

//   void setenv(string key, string val)
//   {
//     m_env.set(key, val);
//   }

//   void getenv(string key, string & val, bool expand_all)
//   {
//     m_env.get(key, val, expand_all);
//   }

    void UNUSED_set_build_cache(string dir) {
        m_build_cache = new Cache(s_conscript_dir, dir);
    }

}

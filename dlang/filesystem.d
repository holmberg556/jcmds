//----------------------------------------------------------------------
// filesystem.d
//----------------------------------------------------------------------
// Copyright 2008-2017 Johan Holmberg.
//----------------------------------------------------------------------
// This file is part of "jcmds".
//
// "jcmds" is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// "jcmds" is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with "jcmds".  If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------

import std.algorithm;
import std.array;
import core.stdc.stdlib;
import std.conv;
import std.datetime;
import std.file;
import std.path;
import std.regex;
import std.stdio;
import std.string;

import builder;
import cmd;
import global;
import os;
import md5calc;

bool file_exist_and_mtime(string path, out SysTime mtime) {
    try {
        SysTime unused_atime;
        getTimes(path, unused_atime, mtime);
        return true;
    }
    catch (FileException) {
        return false;
    }
}

Rdigest get_file_digest(string path) {
    Rmd5 md5;
    auto f = File(path, "rb");
    ubyte[16384] buffer;
    while (true)  {
        ubyte[] got = f.rawRead(buffer);
        if (got.length == 0) break;
        md5.append(got);
    }
    return md5.digest();
}

SysTime get_file_mtime(string path) {
    SysTime unused_atime;
    SysTime mtime;
    getTimes(path, unused_atime, mtime);
    return mtime;
}

enum Status { UNKNOWN, OK, ERROR };

enum Color { WHITE, GREY, BLACK };

struct Include {
    bool quotes;
    string file;
}

//======================================================================
// Information saved for each file

class ConsignEntry {

    SysTime mtime;
    Rdigest content_sig;
    Rdigest dep_sig;
    Include[] includes;

    this() {
        this.mtime = SysTime(0, UTC());
    }

    const void toString(scope void delegate(const(char)[]) sink) {
        sink("ConsignEntry(");
        sink(to!string(mtime));
        sink(", ");
        sink(to!string(content_sig));
        sink(", ");
        sink(to!string(dep_sig));
        if (includes.length > 0) {
            sink(", [[");
            foreach (include; includes) {
                sink(to!string(include.quotes));
                sink(" ");
                sink(include.file);
                sink(", ");
            }
            sink("]] ");
        }
        sink(")");
    }
}

//======================================================================
// All ConsignEntry's for a directory

class Consign {

    ConsignEntry[string] entries;
    bool dirty;

    this() {
    }

    void remove(string name) {
        entries.remove(name);
    }

    ConsignEntry get(string name) {
        auto p = (name in entries);
        if (p !is null) {
            return *p;
        }
        else {
            return entries[name] = new ConsignEntry;
        }
    }

    static Consign read_from_file(string consign_file) {
        if (is_file(consign_file)) {
            auto obj = pickle_load(consign_file);
            obj.dirty = false;
            return obj;
        }
        else {
            return new Consign;
        }
    }

    void write_to_file(SysTime curr_mtime, string consign_file) {
        foreach (k,v; entries) {
            if (v.mtime == curr_mtime) {
                // don't trust recent 'mtime' (within current "delta")
                v.mtime = SysTime(0, UTC());
            }
        }
        pickle_dump(consign_file);
    }

    static Consign pickle_load(string consign_file) {
        auto consign = new Consign;
        auto f = File(consign_file, "r");
        foreach (line; f.byLine) {
            auto fields = line.split("\t");

            auto entry = new ConsignEntry;
            entry.mtime       = SysTime(to!long(fields[1]));
            entry.content_sig = Rdigest.fromString(fields[2]);
            entry.dep_sig     = Rdigest.fromString(fields[3]);
            if (fields.length > 4) {
                foreach (include; fields[4..$]) {
                    entry.includes ~= Include(include[0] == '"',
                                              include[1..$].idup);
                }
            }
            string name = fields[0].idup;
            consign.entries[name] = entry;
        }
        return consign;
    }

    void pickle_dump(string consign_file) {
        auto f = File(consign_file, "w");
        foreach (name, entry; entries) {
            f.write(name,
                    "\t", entry.mtime.stdTime(),
                    "\t", entry.content_sig,
                    "\t", entry.dep_sig);
            foreach (include; entry.includes) {
                f.write(include.quotes ? "\t\"" : "\t<");
                f.write(include.file);
            }
            f.writeln();
        }
    }

}

//======================================================================
// Rentry

class Rentry {

    string name;
    string cached_path;
    Rdir raw_parent;
    bool dirty;

    this(string name, Rdir parent) {
        this.name = name;
        this.cached_path = null;
        this.raw_parent = parent;
        this.dirty = false;
    }

    // Constructor for "root" directory
    this(string name) {
        this.name = name;
        this.cached_path = name;
        this.raw_parent = null;
        this.dirty = false;
    }

    void toString(scope void delegate(const(char)[]) sink) {
        sink("Rentry[");
        sink(this.path);
        sink("]");
    }

    // Return the path of a file/directory.
    // The path may be absolute/relative depending on where in the filesystem
    // the file/directory is located.
    string path() {
        if (cached_path is null) {
            cached_path = raw_parent.path;
            if (cached_path == ".") {
                cached_path = name;
            }
            else {
                if (cached_path[$-1] != '/') {
                    cached_path ~= '/';
                }
                cached_path ~= name;
            }
        }
        return cached_path;
    }

    void print_tree(int level) {}

    // Return the absolute path of a file/directory.
    string abspath() {
        if (raw_parent is null) {
            return name;
        }
        else {
            string path = raw_parent.abspath;
            if (path[$-1] != '/') {
                path ~= '/';
            }
            path ~= name;
            return path;
        }
    }

    string path_prefix() {
        string parent_path = raw_parent.path;
        if (parent_path == ".") {
            return "";
        }
        else if (parent_path[$-1] == '/') {
            return parent_path;
        }
        else {
            return parent_path ~ "/";
        }
    }

    // Set dirty flag and propagate upwards in tree.
    void set_dirty() {
        if (dirty) return;
        dirty = true;
        for (Rdir dir = raw_parent; dir !is null; dir = dir.raw_parent) {
            if (dir.dirty) break;
            dir.dirty = true;
        }
    }
}

//======================================================================

Rdir s_curr_dir;
Rdir[] s_dir_arr;
Rdir[string] s_tops;

immutable string s_dot_jcons_dir = ".jcons/dlang";

@property Rdir cwd() { return s_curr_dir; }

// Initialize filesystem moduile.
// The function can be called several times, and will then "reset"
// the state. This can be useful for testing the code.
void initialize() {
    s_curr_dir = null;
    foreach (k; s_tops.keys) s_tops.remove(k);

    s_dir_arr = [];

    Rdir top = new Rdir("/");
    s_tops["/"] = top;
    s_curr_dir = top.find_dir(getcwd());
    s_curr_dir.cached_path = ".";
    s_curr_dir.m_local_dir_p = true;

    if (! (exists(s_dot_jcons_dir) && isDir(s_dot_jcons_dir))) {
        mkdirRecurse(s_dot_jcons_dir);
    }
}

// Flush information to .consign files.
// Should be called before the process is terminated.
void terminate() {
    SysTime curr_mtime = curr_time_filesystem();
    foreach (d; s_dir_arr) {
        if (d.cached_consign.dirty) {
            d.cached_consign.write_to_file(curr_mtime, d._consign_path());
        }
    }
}

SysTime curr_time_filesystem() {
    string timestamp_file = s_dot_jcons_dir ~ "/.timestamp";
    auto f = File(timestamp_file, "w");
    f.writeln("Current time now is ", Clock.currStdTime());
    f.writeln("but filesystem may have less granularity");
    f.close();
    return get_file_mtime(timestamp_file);
}

//======================================================================
// Rdir

class Rdir : Rentry {

    bool m_cache_dir_p;
    bool m_local_dir_p;
    Consign cached_consign;
    Rentry[string] entries;
    bool[string] m_fs_entries;
    Rdir tgtdir;

    this(string name, Rdir parent) {
        super(name,parent);
        m_fs_entries = null;
        cached_consign = null;
        tgtdir = null;

        // TODO: verify that parent !is null always is true,
        // and if so remove the tests below.
        m_cache_dir_p = (parent !is null && parent.m_cache_dir_p);
        m_local_dir_p = (parent !is null && parent.m_local_dir_p);

        entries["."]  = this;
        entries[".."] = (parent !is null ? parent : this);
    }

    this (string name) {
        super(name);
        m_fs_entries = null;
        cached_consign = null;
        tgtdir = null;
        m_cache_dir_p = false;     // TODO: how is this used?
        m_local_dir_p = false;
    }

    //----------------------------------------------------------------------

    Rdir parent() {
        return raw_parent is null ? this : raw_parent;
    }

    //----------------------------------------------------------------------

    void teardown() {
        s_dir_arr = [];
        foreach (k; s_tops.keys) s_tops.remove(k);
    }

    //----------------------------------------------------------------------

    void init_out_of_source(string relpath, Rdir tgtdir) {
        this.cached_path = relpath;
        this.tgtdir = tgtdir;
    }

    //----------------------------------------------------------------------

    void top_print_tree() {
        foreach (k,v; s_tops) {
            v.print_tree(0);
        }
    }

    //----------------------------------------------------------------------

    string _consign_path() {
        if (m_cache_dir_p || (opt_perl_cons && m_local_dir_p)) {
            string consign_path = this.path ~ "/.rconsign";
            return consign_path;
        }
        else {
            string name = this.path;
            //
            // Transform a path with directory delimiters ('/') to a filename:
            //
            //   - use '=' as quote character
            //   - use '+' instead of '/'
            //
            name = name.replace("=", "==");
            name = name.replace("+", "=+");
            name = name.replace("/", "+");

            string consign_path = s_dot_jcons_dir ~ "/__" ~ name;
            return consign_path;
        }
    }

    //----------------------------------------------------------------------

    Consign consign() {
        if (cached_consign is null) {
            cached_consign = Consign.read_from_file(_consign_path());
            s_dir_arr ~= this;
        }
        return cached_consign;
    }

    //----------------------------------------------------------------------

    override void print_tree(int level) {
        for (int i=0; i<level; ++i) { write("    "); }
        writeln("'", name, "'");
        foreach (k,v; entries) {
            if (k == ".") continue;
            if (k == "..") continue;
            v.print_tree(level + 1);
        }
    }

    //----------------------------------------------------------------------

    static pure string[] _split_path(string path) {
        if (path == "") {
            throw new Exception("empty path");
        }
        if (path == "/") {
            return [""];
        }
        string[] parts;
        string rest = path;
        while (true) {
            auto i = rest.indexOf('/');
            if (i == -1) {
                parts ~= rest;
                break;
            }
            else if (i == 0 && parts.length == 0) {
                parts ~= "";
            }
            else if (i == 0) {
                // skip double /
            }
            else {
                parts ~= rest[0..i];
            }
            rest = rest[i+1..$];
        }
        return parts;
    }

    unittest {
        assert( _split_path("/aaa") == ["", "aaa"] );
        assert( _split_path("/aaa/bbb") == ["", "aaa", "bbb"] );

        assert( _split_path("aaa") == ["aaa"] );
        assert( _split_path("aaa/bbb") == ["aaa", "bbb"] );
        assert( _split_path("aaa/bbb/ccc") == ["aaa", "bbb", "ccc"] );
    }

    //----------------------------------------------------------------------

    Rfile find_file(string path) {
        string[] parts =_split_path(path);
        string name = parts[$-1];
        Rdir d = (parts.length == 1) ? this : _find_dir(parts[0..$-1]);

        Rfile f;
        auto p = (name in d.entries);
        if (p is null) {
            d.entries[name] = f = new Rfile(name, d);
        }
        else {
            f = cast(Rfile) *p;
            if (f is null) {
                throw new Exception("not file");
            }
        }
        return f;
    }

    //----------------------------------------------------------------------

    Rdir find_dir(string path) {
        string[] parts = _split_path(path);
        return _find_dir(parts);
    }

    Rdir _find_dir(string[] path) {
        Rdir d = this;
        foreach (name; path) {
            if (name == "#") {
                d = s_curr_dir;
            }
            else if (name == "") {
                d = s_tops["/"];
            }
            else {
                Rdir d2;
                auto p = (name in d.entries);
                if (p is null) {
                    d.entries[name] = d2 = new Rdir(name, d);
                }
                else {
                    d2 = cast(Rdir) *p;
                    if (d2 is null) {
                        throw new Exception("not dir");
                    }
                }
                d = d2;
            }
        }
        return d;
    }

    unittest {
        initialize();
        assert(true);
        Rdir d1  = s_curr_dir.find_dir("d1");
        Rdir d1x = s_curr_dir.find_dir("d1");
        Rdir d2  = s_curr_dir.find_dir("d2");
        Rdir e1  = s_curr_dir.find_dir("d1/e1");
        Rdir e2  = s_curr_dir.find_dir("d1/e2");

        assert( d1.path() == "d1" );
        assert( d1x.path() == "d1" );
        assert( d1 is d1x );
        assert( d1 !is d2 );

        assert( d1 !is e1 );
        assert( d1 is e1.parent() );

        assert( d2.path() == "d2" );

        assert( e1.path() == "d1/e1" );
        assert( e2.path() == "d1/e2" );

        assert( d1.parent().path() == "." );

        assert( d1.parent().parent().path() == dirName(getcwd()) );
    }

    //----------------------------------------------------------------------

    Rdir lookup_or_fs_dir(string path) {
        string parts[] = _split_path(path);
        return _lookup_or_fs_dir(parts);
    }

    Rdir _lookup_or_fs_dir(string[] parts) {
        Rdir d = this;
        foreach (name; parts) {
            if (name == "") {
                d = s_tops["/"];
            }
            else {
                auto p = (name in d.entries);
                if (p is null) {
                    string fspath = d.path() ~ "/" ~ name;
                    try {
                        auto entry = DirEntry(fspath);
                        if (entry.isDir) {
                            Rdir d2 = new Rdir(name, d);
                            d.entries[name] = d2;
                            d = d2;
                        }
                        else {
                            Rfile f = new Rfile(name, d);
                            d.entries[name] = f;
                            f.m_mtime = entry.timeLastModified;
                            f.m_mtime_set = true;
                            throw new Exception("not a dir");
                        }
                    }
                    catch (FileException) {
                        d.entries[name] = null;
                        return null;
                    }
                }
                else if (*p is null) {
                    return null;
                }
                else {
                    d = cast(Rdir) *p;
                    if (d is null) {
                        throw new Exception("not a dir");
                    }
                }
            }
        }
        return d;
    }


    //----------------------------------------------------------------------
    // TODO: what to do if 'path' is a full filepath?

    Rfile lookup_or_fs_file(string path) {
        debug (logging) {
            writeln("LOOKUP_OR_FS_FILE: ", path);
        }
        string[] parts = _split_path(path);
        Rdir d = _lookup_or_fs_dir(parts[0..$-1]);
        if (d is null) {
            return null;
        }
        else {
            Rfile f;
            string name = parts[$-1];
            auto p = (name in d.entries);
            if (p is null) {
                string fspath = d.path() ~ "/" ~ name;
                try {
                    auto entry = DirEntry(fspath);
                    if (entry.isFile) {
                        f = new Rfile(name, d);
                        d.entries[name] = f;
                    }
                    else {
                        Rdir d2 = new Rdir(name, d);
                        d.entries[name] = d2;
                        throw new Exception("not a file");
                    }
                }
                catch (FileException) {
                    d.entries[name] = null;
                    return null;
                }
            }
            else if (*p is null) {
                return null;
            }
            else {
                f = cast(Rfile) *p;
                if (f is null) {
                        throw new Exception("not a file");
                }
            }
            return f;
        }
    }

    // //----------------------------------------------------------------------

    Rentry lookup_entry(string path) {
        string[] parts = _split_path(path);
        Rentry e = this;
        foreach (name; parts) {
            Rdir d = cast(Rdir) e;
            if (d is null) {
                throw new Exception("not dir");
            }
            if (name == "") {
                d = s_tops["/"];
            }
            else {
                auto p = (name in d.entries);
                if (p is null) {
                    return null;
                }
                else {
                    e = *p;
                }
            }
        }
        return e;
    }

    //----------------------------------------------------------------------

    bool mkdir_p() {
        try {
            mkdirRecurse(this.path());
            return true;
        }
        catch (FileException) {
            return false;
        }
    }

    //----------------------------------------------------------------------

    void append_files_under(ref Rfile[] tgts) {
        auto ks = entries.keys();
        sort(ks);
        foreach (k; ks) {
            auto v = entries[k];
            if (k == ".") continue;
            if (k == "..") continue;
            if (auto d = cast(Rdir)v) {
                d.append_files_under(tgts);
            }
            else if (auto f = cast(Rfile)v) {
                if (f.cmd !is null) {
                    tgts ~= f;
                }
            }
            else {
                writeln("jcons: error: internal error");
                exit(1);
            }
        }
    }

}

//======================================================================

class Rfile : Rentry {

    Status build_ok;
    SysTime m_mtime = SysTime(0, UTC());
    bool m_mtime_set = false;
    bool m_file_exist = false;
    bool m_file_exist_set = false;
    Rdir m_tgtfile = null;
    Rfile[] extra_deps = null;
    Rfile[] m_exe_deps = null;
    bool m_top_target = false;
    Cmd cmd = null;
    BaseFun m_waiting_sem = null;

    Color color;
    Rdigest wanted_digest;

    this(string name, Rdir parent) {
        super(name,parent);
    }

    Rdir parent() {
        return raw_parent;
    }

    //----------------------------------------------------------------------

    ConsignEntry _consign_get_entry() {
        return raw_parent.consign.get(name);
    }

    void _consign_remove_entry() {
        raw_parent.consign().remove(name);
    }

    void _consign_set_dirty() {
        raw_parent.consign().dirty = true;
    }

    //--------------------------------------------------
    //--------------------------------------------------
    // Methods for managing the .consign information ...

    //----------------------------------------------------------------------
    // Return the "raw" content_sig value.
    // It will always exist when this function is called.

    Rdigest db_content_sig() {
        ConsignEntry consign_entry = _consign_get_entry();
        return consign_entry.content_sig;
    }


    //----------------------------------------------------------------------
    // Return dep_sig if it is valid.
    // Called when we consider rebuilding the file.

    Rdigest db_dep_sig() {
        ConsignEntry consign_entry = _consign_get_entry();
        if (consign_entry.dep_sig.invalid_p()) {
            return Rdigest.invalid;
        }

        SysTime mtime = file_mtime();
        if (mtime == consign_entry.mtime && opt_trust_mtime) {
            return consign_entry.dep_sig;
        }
        Rdigest content_sig = file_md5();
        if (content_sig == consign_entry.content_sig) {
            consign_entry.mtime = mtime;
            _consign_set_dirty();
            return consign_entry.dep_sig;
        }
        consign_entry.includes = null;
        _consign_set_dirty();
        return Rdigest.undefined;
    }

    //----------------------------------------------------------------------
    // The file exists and is a "source".
    // Make sure it has an accurate .consign entry.

    void st_source() {
        ConsignEntry consign_entry = _consign_get_entry();
        SysTime mtime = file_mtime();
        if (! (mtime == consign_entry.mtime && opt_trust_mtime)) {
            consign_entry.mtime = mtime;
            consign_entry.content_sig = file_md5();
            consign_entry.includes = null;
            _consign_set_dirty();
        }
        if (consign_entry.dep_sig != consign_entry.content_sig) {
            consign_entry.dep_sig = consign_entry.content_sig;
            _consign_set_dirty();
        }
        this.build_ok = Status.OK;
    }

    //----------------------------------------------------------------------
    // The file is invalid: the file does not exist, or a build step failed.
    // Forget about the file.

    void st_invalid_error() {
        set_dirty();
        _consign_remove_entry();
        _consign_set_dirty();
        this.build_ok = Status.ERROR;
    }

    //----------------------------------------------------------------------
    // Propagate the fact that there has been an error "upstream".
    // No change of .consign info.

    void st_propagate_error() {
        set_dirty();
        this.build_ok = Status.ERROR;
    }

    //----------------------------------------------------------------------
    // The file is already up-to-date.
    // No change of .consign info.

    void st_propagate_ok() {
        this.build_ok = Status.OK;
    }

    //----------------------------------------------------------------------
    // The file has been updated by running a command.
    // The new 'dep_sig' is stored, and 'mtime' and 'content_sig' are updated.

    void st_updated(Rdigest dep_sig) {
        set_dirty();
        ConsignEntry consign_entry = _consign_get_entry();
        _consign_set_dirty();
        consign_entry.mtime       = file_mtime_FORCED();
        consign_entry.content_sig = file_md5();
        consign_entry.dep_sig     = dep_sig;

        consign_entry.includes = null;

        this.build_ok = Status.OK;
    }

    //----------------------------------------------------------------------

    bool st_ok_p() {
        return this.build_ok == Status.OK;
    }

    //----------------------------------------------------------------------

    bool file_exist() {
        if (! m_file_exist_set) {
            m_file_exist = file_exist_and_mtime(path(), m_mtime);
            m_file_exist_set = true;
            m_mtime_set = m_file_exist;
        }
        return m_file_exist;
    }

    //----------------------------------------------------------------------

    bool file_exist_FORCED() {
        // like 'file_exist', but with forced call
        m_file_exist = file_exist_and_mtime(path(), m_mtime);
        m_file_exist_set = true;
        m_mtime_set = m_file_exist;

        return m_file_exist;
    }

    //----------------------------------------------------------------------
    // TODO: use this one ???

    Rdigest file_md5() {
        string path = this.path();
        // TODO: avoid extra copying of Rdigest
        Rdigest digest = get_file_digest(path);
        return digest;
    }

    //----------------------------------------------------------------------

    SysTime file_mtime() {
        if (! m_mtime_set) {
            m_mtime = get_file_mtime(path());
            m_mtime_set = true;
        }
        return m_mtime;
    }

    //----------------------------------------------------------------------

    SysTime file_mtime_FORCED() {
        // like 'file_mtime', but with forced call
        m_mtime = get_file_mtime(path());
        m_mtime_set = true;

        return m_mtime;
    }

    //----------------------------------------------------------------------
    // Called when file exists and has current info in .consign.
    // We may either get value from .consign or compute it now.

    Include[] db_includes() {
        ConsignEntry consign_entry = _consign_get_entry();
        if (consign_entry.includes is null) {
            consign_entry.includes = [];
            calc_includes(consign_entry.includes);
            _consign_set_dirty();
        }
        return consign_entry.includes;
    }

    //----------------------------------------------------------------------

    void calc_includes(ref Include[] includes) {
        static include_re = regex(`^\s*#\s*include\s+([<"])(.*?)[>"]`);
        static end_re = regex(`^#end\b`);

        string path = this.path();
        auto f = File(path, "r");
        foreach (line; f.byLine) {
            if (line.find('#').length == 0) continue;
            if (auto m = matchFirst(line, include_re)) {
                bool quotes = (m[1] == "\"");
                string filename = m[2].idup;
                includes ~= Include(quotes, filename);
            }
            else if (matchFirst(line, end_re)) {
                break;
            }
        }
    }

    bool is_source() {
        return cmd is null;
    }

    //----------------------------------------------------------------------

    override void print_tree(int level) {
        for (int i=0; i<level; ++i) { write("    "); }
        writeln("\"", name, "\"");
    }

}

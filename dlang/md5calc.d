//----------------------------------------------------------------------
// md5calc.d
//----------------------------------------------------------------------
// Copyright 2008-2017 Johan Holmberg.
//----------------------------------------------------------------------
// This file is part of "jcmds".
//
// "jcmds" is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// "jcmds" is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with "jcmds".  If not, see <http://www.gnu.org/licenses/>.
//----------------------------------------------------------------------

import std.digest.md;
import std.conv;
import std.stdio;
import std.string;
import std.format;
import std.bitmanip;

struct Rdigest {
    ubyte[16] data;

    static Rdigest invalid;
    static Rdigest undefined;
    bool invalid_p() {
        bool flag = this == invalid;
        return flag;
    }

    int opCmp(ref const Rdigest other) const {
        for (int i=0; i<16; i++) {
            int cmp = data[i] - other.data[i];
            if (cmp != 0) return cmp;
        }
        return 0;
    }

    const void toString(scope void delegate(const(char)[]) sink) {
        foreach (i; data) {
            sink(format("%02x", i));
        }
    }

    static Rdigest fromString(char[] str) {
        assert(str.length == 32);
        Rdigest res;
        foreach (i; 0..16) {
            res.data[i] = str[2*i..2*i+2].to!ubyte(16);
        }
        return res;
    }

    // string toString() {
    //     string res;
    //     foreach (c; data) {
    //         res ~=
    //     }
    //     return res;
    // }
}

Rdigest calc_digest(ref const Rdigest[] digests) {
    Rmd5 md5;
    foreach (digest; digests) {
        md5.append(digest);
    }
    return md5.digest();
}


struct Rmd5 {
    MD5 m_md5_state;

    void append(int value) {
        m_md5_state.put(nativeToLittleEndian(value));
    }
    void append(string str) {
        m_md5_state.put(cast(ubyte[]) str);
    }

    void append(ubyte[] bytes) {
        m_md5_state.put(bytes);
    }

    void append(Rdigest digest) {
        m_md5_state.put(digest.data);
    }

    Rdigest digest() {
        Rdigest digest;
        digest.data = m_md5_state.finish();
        return digest;
    }

    static Rdigest digest(string str) {
        Rmd5 md5;
        md5.append(str);
        return md5.digest();
    }

    string hexdigest() {
        return m_md5_state.finish().toHexString().idup;
    }

}

unittest {
    Rmd5 m0;
    assert( m0.digest().data == [212, 29, 140, 217, 143, 0, 178, 4, 233, 128, 9, 152, 236, 248, 66, 126] );

    Rmd5 m1;
    assert( m1.hexdigest() == "D41D8CD98F00B204E9800998ECF8427E" );

    Rmd5 m2;
    m2.append("a");
    assert( m2.hexdigest() == "0CC175B9C0F1B6A831C399E269772661" );

    Rmd5 m3;
    m2.append(0x01020304);
    assert( m2.hexdigest() == "C73CABEB6558ABA030BBA9CA49DCDD75" );
}
